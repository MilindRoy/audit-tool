﻿$(document).ready(function () {

	let auditTypeSelectBox = document.querySelector("#audit-type-select-update");

	auditTypeSelectBox.addEventListener("change", function (e) {
		if (this.value == "-1") {
			resetAllElements();
			return;
		}
		fillRequirementChecklistTableToUpdate();
	});

	getUserName();

	$('#update-checklist-btn').tooltip();
	$('#reset-checklist-btn').tooltip();
});

function getUserName() {

	let sUserNameURL = "/Account/GetUsername";
	//Make Ajax request to get user name
	$.ajax({
		url: sUserNameURL,
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		success: populateUsername,
		error: function () {
			var userLabel = document.getElementById("userLabel");
			userLabel.innerHTML = "Username failure";
		}
	});
}

function populateUsername(username) {
	let userLabel = document.getElementById("userLabel");
	let userIcon = document.getElementById("userLabelDiv");
	userLabel.innerHTML = "Hello, " + username;
	userIcon.setAttribute("data-toggle", "tooltip");
	userIcon.setAttribute("data-placement", "bottom");
	userIcon.setAttribute("title", userLabel.innerHTML);
	$('#userLabelDiv').tooltip();
	userName = username;
}

function resetAllElements() {
	let tableContainer = document.querySelector("#requirement-checklist-update");
	tableContainer.innerHTML = "";
	document.getElementById("buttonRow").classList.add("hidden");
}

function fillRequirementChecklistTableToUpdate() {
	let loader = document.getElementById("loader-container");

	let _auditType = document.getElementById("audit-type-select-update").value;

	let ajax = new XMLHttpRequest();
	ajax.open("POST", "/Action/GetCheckList");
	ajax.setRequestHeader('Content-Type', 'application/json');

	ajax.addEventListener("load", function (e) {
		loader.classList.add("hidden");
		let json = e.currentTarget.responseText;
		if (json == '0' || json == '' || json == null) {
			let tableContainer = document.querySelector("#requirement-checklist-update");
			tableContainer.innerHTML = "<h6>No Data</h6>";
			return;
		}
		renderCheckListNewTableUpdateChecklist(JSON.parse(json));
	});
	ajax.addEventListener("error", function (e) {
	    alertMessages("0", "Error");
	});

	ajax.send(JSON.stringify({
		auditType: _auditType
	}));
	loader.classList.remove("hidden");
}

function renderCheckListNewTableUpdateChecklist(checklist) {
	document.getElementById("buttonRow").classList.remove("hidden");
	let tr, td, textarea, fieldset, tableheader;
	let counter = 0;
	let tableContainer = document.querySelector("#requirement-checklist-update");
	tableContainer.innerHTML = "";

	if (checklist == '' || checklist == undefined) {
		table.innerHTML = "<tr><td>No checklist available</td><tr/>";
	}


	$.each(checklist, function (key, value) {

		fieldset = document.createElement('FIELDSET');
		legend = document.createElement('legend');
		legend.classList.add("table-legend");
		legend.appendChild(document.createTextNode(key + " "));

		tableheader = document.createElement("div");
		tableheader.classList.add("table-header");
		tableheader.innerHTML = "Checklist";

		var image = document.createElement('img');
		image.setAttribute("src", "/images/plus.svg");

		var pluslink = document.createElement('div');
		pluslink.addEventListener('click', function () {
			addChecklist(key, counter, $(key).length - 1)
		});
		pluslink.style.cursor = "pointer";
		pluslink.style.cssFloat = "right";
		span = document.createElement('span');
		span.style.color = "#0078D4";
		span.style.fontWeight = "bold";
		span.innerHTML = " Add New Item";
		pluslink.appendChild(image);
		pluslink.appendChild(span);
		tableheader.appendChild(pluslink);
		legend.appendChild(tableheader);

		legend.classList.add("text-center");
		fieldset.appendChild(legend);

		table = document.createElement('table');
		table.classList.add("table", "custom-table");

		table.setAttribute("id", key);
		var temp = key;

		tbody = document.createElement('tbody');

		value.forEach(function (element, index) {

			tr = document.createElement("tr");
			td = document.createElement("td");
			td.classList.add("updateChecklistClass");
			td.style.width = "1150px";
			td.setAttribute("name", `checklist-${counter}-${index}`);
			td.setAttribute("contenteditable", "false");
			hint = element.replace(/(?:\r\n|\r|\n)/g, '<div class="hint">');
			td.innerHTML = hint + "</div>";


			cross = document.createElement('div');
			cross.classList.add("iconLink");
			cross.classList.add("fa");
			cross.classList.add("fa-trash-o");
			$(cross).prop('title', 'Click to delete this row');
			cross.addEventListener('click', function () {
				deleteRow(this)
			});
			cross.style.cssFloat = "right";
			cross.style.cursor = "pointer";
			cross.style.color = "#E16166";
			cross.setAttribute("contenteditable", "false");
			td.appendChild(cross);


			edit = document.createElement('div');
			edit.classList.add("iconlink");
			edit.classList.add("fa");
			edit.classList.add("fa-edit");
			$(edit).prop('title', 'Click to edit this row');
			td.appendChild(edit);
			edit.style.cssFloat = "right";
			edit.style.marginRight = "10px";
			edit.style.marginTop = "2px";
			edit.style.cursor = "pointer";
			edit.contentEditable = "false";
			edit.style.color = "#2EA566";
			var temp1 = td.getAttribute('name');
			edit.addEventListener('click', function () {
				editRow(temp1, temp)
			});
			//edit.setAttribute("style", "margin-right: 2px;");
			//edit.setAttribute("style", "padding-left: 1040px;");
			//edit.setAttribute("style", "padding-bottom: 2px;");
			td.appendChild(edit);

			tr.appendChild(td);

			tbody.appendChild(tr);
		});
		counter++;
		table.appendChild(tbody);
		fieldset.appendChild(table);
		tableContainer.appendChild(fieldset);

	});

	$('.fa-trash-o').tooltip();
	$('.fa-edit').tooltip();
}

function updateChecklist() {
	let iLengthOfLegends = document.getElementsByClassName("table-legend").length;
	let oUpdatedJSONForTheAudit = {};
	for (let iIteratorToFindOffsetName = 0; iIteratorToFindOffsetName < iLengthOfLegends; iIteratorToFindOffsetName++) {
		let sOffsetName = document.getElementsByClassName("table-legend")[iIteratorToFindOffsetName].innerText;
		sOffsetName = sOffsetName.replace(/Add New Item/gi, '');
		sOffsetName = sOffsetName.replace(/Checklist/gi, '');
		sOffsetName = sOffsetName.trim();
		oUpdatedJSONForTheAudit[sOffsetName] = [];
		let iLengthOfData = document.getElementsByClassName("table")[iIteratorToFindOffsetName].childNodes[0].childElementCount;
		for (let iIteratorToAddData = 0; iIteratorToAddData < iLengthOfData; iIteratorToAddData++) {
			let sData = document.getElementsByClassName("table")[iIteratorToFindOffsetName].childNodes[0].childNodes[iIteratorToAddData].childNodes[0].innerText;
			oUpdatedJSONForTheAudit[sOffsetName].push(sData);
		}
	}
	let sAauditType = document.getElementById("audit-type-select-update").value;

	let sUpdateChecklist = "/UpdateChecklist/modifyChecklist";
	let sModifiedChecklist = JSON.stringify(oUpdatedJSONForTheAudit);
	$.ajax({
		url: sUpdateChecklist,
		method: "post",
		contentType: "application/json; charset=utf-8",
		data: JSON.stringify({
			auditType: sAauditType,
			updatedString: sModifiedChecklist
		}),
		dataType: "json",
		success: function (data) {
			if (data === 'Error') {
			    alertMessages("0", "Some error occurred. Please try again");
			} else {
			    alertMessages("1", "Checklist for the audit type '" + sAauditType + "' has been successfully updated.");
			}
			resetAllElements();

			let auditTypeSelectBox = document.querySelector("#audit-type-select-update");
			auditTypeSelectBox.value = "-1";

		},
		error: function (data) {
			//console.log(data);
		}
	});

}

function resetChecklist() {
	resetAllElements();
	fillRequirementChecklistTableToUpdate();
}

function addChecklist(key, index, counter) {
	tableRef = document.getElementById(key);
	newRow = tableRef.insertRow();
	newCell = newRow.insertCell(0);
	newCell.setAttribute("contenteditable", "true");
	newCell.style.width = "1150px";
	newCell.style.border = "2px solid #dee2e6";
	newCell.setAttribute("name", `key-${counter}-${index}`);

	//text = document.createElement('div');
	////text.setAttribute("contenteditable", "true");
	//text.setAttribute("style", "float: left;");
	//newCell.appendChild(text);

	cross = document.createElement('div');
	cross.classList.add("iconLink");
	cross.classList.add("fa");
	cross.classList.add("fa-trash-o");
	newCell.appendChild(cross);
	cross.addEventListener('click', function () {
		deleteRow(this)
	});
	cross.setAttribute("style", "float: right;");
	cross.style.cursor = "pointer";
	cross.style.color = "#E16166";
	cross.setAttribute("contenteditable", "false");

	text = document.createElement('div');
	//text.setAttribute("contenteditable", "true");
	text.setAttribute("style", "float: left;");
	newCell.appendChild(text);
}

function deleteRow(arg) {
	$(arg).closest("tr").remove();
}

function editRow(arg, key) {

	tableRef = document.getElementById(key);
	var tds = tableRef.getElementsByTagName("td");

	for (i = 0; i < tds.length; i++) {

		if (tds[i].getAttribute('name') != arg) {
			tds[i].contentEditable = 'false';
			tds[i].classList.add("updateChecklistClass");

		}

		if (tds[i].getAttribute('name') == arg) {
			tds[i].contentEditable = 'true';
			tds[i].classList.remove("updateChecklistClass");

		}

	}
}

function alertMessages(messageType, messageText) {
    $("#modal-message").text(messageText);
    if (messageType == "0") {
        $("#modal-title").text("Error");
        $("#closeButton").removeClass('btn-success');
        $("#closeButton").removeClass('btn-info');
        $("#closeButton").addClass('btn-danger');
    }
    else if (messageType == "1") {
        $("#modal-title").text("Success");
        $("#closeButton").removeClass('btn-danger');
        $("#closeButton").removeClass('btn-info');
        $("#closeButton").addClass('btn-success');
    }
    else {
        $("#modal-title").text("Info");
        $("#closeButton").removeClass('btn-danger');
        $("#closeButton").removeClass('btn-success');
        $("#closeButton").addClass('btn-info');
    }
    $('#myModal').css('display', 'block');
    $('#myModal').css('background-color', 'RGBA(0,0,0,0.4)');
    $('#myModal').css('opacity', '1');
}

function closePopUp() {
    $('#myModal').css('display', 'none');
    $('#myModal').css('opacity', '0');
}