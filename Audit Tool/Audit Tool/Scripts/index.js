﻿$(document).ready(function () {
	getUserName();

	renderProjectManagers();
	renderProjects();

	let auditTypeSelectBox = document.querySelector("#audit-type-select");
	let auditDateSelectBox = document.querySelector("#audit-select-box");

	document.querySelector("#project-manager-name").addEventListener("change", function (e) {
		let PMname = this.options[this.selectedIndex].value;

		if (this.selectedIndex != 0) {
			loadAllProjectsForTrimming();
			let projectSelect = document.querySelectorAll("#project-name-select option");
			for (let index = 0; index < projectSelect.length; index++) {
				if (PMname != projectSelect[index].getAttribute('for')) {
					$(projectSelect[index]).remove();
				}
			}
		} else {
			removeAllProjects();
		}

		auditTypeSelectBox.selectedIndex = 0;
		auditDateSelectBox.innerHTML = "<option value='-1'>New Audit</option>";

		resetResultTable();
	});

	//$('#project-name-select').change(function(){
	//    let projectName = this.options[this.selectedIndex].value;

	//    if (this.selectedIndex != 0) {
	//        renderReports();
	//    }
	//    else {
	//        removeAllReports();
	//    }
	//});

	document.querySelector("#project-name-select").addEventListener("change", function (e) {
		auditTypeSelectBox.selectedIndex = 0;
		auditDateSelectBox.innerHTML = "<option value='-1'>New Audit</option>";
		resetResultTable();
	});

	auditTypeSelectBox.addEventListener("change", function (e) {
		if (this.value == "-1") {
			resetResultTable();
			return;
		}
		renderAuditDateSelectBox();
		fillRequirementChecklistTable();
	});

	document.querySelector("#submit-audit-btn").addEventListener("click", function (e) {
		submitAudit($(this).val());
	});

	document.querySelector("#save-audit-btn").addEventListener("click", function (e) {
	    submitAudit($(this).val());
	    //autoSaveAudit("Save");
	});

	auditDateSelectBox.addEventListener("change", function (e) {
		if (this.value == "-1") {
			fillRequirementChecklistTable();
		} else {
			let date = this.value.trim();
			getPreviousAudit(date);
		}
	});

    setInterval(function () {
        //Autosave function that will be run every 10 minutes.    
        autoSaveAudit("Save");
    }, (10 * 60 * 1000));

	$('input[name="radio-review"]').click(function () {
	    showHide();
	});

    //var i = 1;
    //$('#add-btn').click(function () {
    //    i++;
    //    $('#dynamic_field').append('<tr id="row' + i + '"><td><input type="text" placeholder="Enter your Name" class="form-control" /></td><td><button type="button" name="remove" id="' + i + '" class="btn btn-danger btn_remove">X</button></td></tr>');
    //});
    //$(document).on('click', '.btn_remove', function () {
    //    var button_id = $(this).attr("id");
    //    $('#row' + button_id + '').remove();
    //});

	$('#add-observations-btn').click(function () {
	    var res = confirm("Are you sure you want to add new row?");
	    if (res == true) {
	        dynamicObservations();
	    }
    });

    $(document).on('click', '.btn_remove', function () {
        var res = confirm("Are you sure you want to delete this row?");
        if (res == true) {
            var button_id = $(this).attr("id");
            $('#row' + button_id + '').remove();
        }
    });

    $('.navbar-icon').tooltip();
    $('#save-audit-btn').tooltip();
    $('#submit-audit-btn').tooltip();
});

let oProjectsJSON;
var rownumber = 1;
function dynamicObservations() {
    rownumber++;
    let observ = `<div class="row cancellable-row" id="row` + rownumber + `">
            <div class="col-md-7">
                <div class="form-group">
                    <input type="text" class ="form-control" placeholder="Leave the section blank instead of 'None', 'Nil', 'NA', etc"></input>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <select class ="form-control observation-severity">
                        <option value="-1">Select</option>
                        <option value="P0">P0</option>
                        <option value="P1">P1</option>
                    </select>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <select class ="form-control observation-area">
                        <option value="-1">Select</option>
                        <option value="Security">Security</option>
                        <option value="Performance">Performance</option>
                        <option value="Availability">Availability</option>
                        <option value="Scalability">Scalability</option>
                        <option value="Others">Others</option>
                    </select>
                </div>
            </div>
            <div class="col-md-1">
                <div class="form-group">
                    <button type="button" name="remove" id="`+ rownumber + `" class ="btn btn-danger btn_remove">X</button>
                </div>
            </div></div>`;
    $('#dynamic_observations').append(observ);
}

function showHide() {
    var inputValue = "";
    inputValue = document.querySelector('input[name="radio-review"]:checked').value;
    if (inputValue == 'no-reason') {
        document.getElementById("reason-row").style.display = "block";
        document.getElementById("form-comment-row").style.display = "block";
        $('#requirement-checklist').hide();
        $('#dynamic_observations').hide();
    }
    else {
        document.getElementById("reason-row").style.display = "none";
        document.getElementById("form-comment-row").style.display = "none";
        $('#requirement-checklist').show();
        $('#dynamic_observations').show();
    }
}

function getUserName() {

	let sUserNameURL = "/Account/GetUsername";
	//Make Ajax request to get user name
	$.ajax({
		url: sUserNameURL,
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		success: populateUsername,
		error: function () {
			var userLabel = document.getElementById("userLabel");
			userLabel.innerHTML = "Username failure";
		}
	});
}

function populateUsername(username) {
	let userLabel = document.getElementById("userLabel");
	let userIcon = document.getElementById("userLabelDiv");
	userLabel.innerHTML = "Hello, " + username;
	userIcon.setAttribute("data-toggle", "tooltip");
	userIcon.setAttribute("data-placement", "bottom");
	userIcon.setAttribute("title", userLabel.innerHTML);
	$('#userLabelDiv').tooltip();
	userName = username;
}

function renderChecklist() {
    let sUpdateChecklist1 = "/Account/CheckUserinSG";

	$.ajax({
		url: sUpdateChecklist1,
		dataType: "json",
		success: function (data) {

			if (data === 'Error') {
			    alertMessages("0", "Error while rendering checklist. Please try again");
			} else {

				finduser(data);
			}
		},
		error: function (data) {

		}
	});


	function finduser(flag) {

		
		if (flag) {
		    document.getElementById("checklist-div").classList.remove("hidden");

		}

	}
}

function renderAuditDateSelectBox() {

	let _pMId = document.getElementById("project-manager-name").value;
	let _projectId = document.getElementById("project-name-select").value;
	let _auditType = document.getElementById("audit-type-select").value;
	let select = document.querySelector("#audit-select-box");

	let params = {
		'projectId': _projectId,
		'auditType': _auditType,
		'projectManagerId': _pMId,
	}
	params = JSON.stringify(params);
	let ajax = new XMLHttpRequest();
	ajax.open("POST", "/Action/GetPreviousAuditDates");
	ajax.setRequestHeader('Content-Type', 'application/json');
	ajax.addEventListener("load", function (e) {
		let json = e.currentTarget.responseText;


		let html = `<option value="-1" selected>New Audit</option>`;
		if (json == '' || json == '0' || json == null || json == undefined) {
			html += "<option value='0' disabled>No previous audit available</option>"
			select.innerHTML = html;
			return;
		}
		json = JSON.parse(json);
		json.forEach(function (element, index) {
			let datetime = element.DateTime.substring(0, element.DateTime.indexOf('.')).replace("T", "  ");
			html += `<option value="${element.DateTime}">${datetime}</option>`
		});
		select.innerHTML = html;
	});
	ajax.addEventListener("error", function (e) {
	    alertMessages("0", "Server error");
	});
	ajax.send(params);
	select.innerHTML = `<option value="-1" selected>Loading...</option>`;
}

function getPreviousAudit(date) {
	debugger;
	let loader = document.getElementById("loader-container");
	let _pMId = document.getElementById("project-manager-name").value;
	let _projectId = document.getElementById("project-name-select").value;
	let _auditType = document.getElementById("audit-type-select").value;
	let sProjectName = $("#project-name-select option:selected").text();

	let params = {
		'projectId': _projectId,
		'auditType': _auditType,
		'projectManagerId': _pMId,
		'date': date
	}

	params = JSON.stringify(params);

	var sGetPrevData = "/Action/GetPrevAuditData";
	$.ajax({
		url: sGetPrevData,
		method: "post",
		contentType: "application/json; charset=utf-8",
		data: JSON.stringify({
			projectName: sProjectName,
			PMName: _pMId,
			AuditType: _auditType,
			Date: date
		}),
		dataType: "json",
		success: function (data) {
			if (data === 'Error') {
				var result = $.parseJSON(data);
				$.each(result, function (key, val) {
					document.getElementById(key).value = val;
				});
			} else {
				var result = $.parseJSON(data);
				$.each(result, function (key, val) {
					if (val) { //check if the values are not empty or null
						document.getElementById(key).value = val;
					}
					//else
					//{
					//	document.getElementById(key).value = "";
					//}
				});
				$('#requirement-checklist').show();
			}
		},
		error: function (ex) {
		    alertMessages("0", ex);
		}
	});

	var sGetPreviousObservations = "/Action/GetPreviousObservations";
	$.ajax({
		url: sGetPreviousObservations,
		method: "post",
		contentType: "application/json; charset=utf-8",
		data: JSON.stringify({
			projectId: _projectId,
			projectManagerId: _pMId,
			auditType: _auditType,
			Date: date
		}),
		dataType: "json",
		success: function (data) {
			if (data === 'Error') {
				document.getElementById("all-observations").value = "Some error occurred. Please try again."
			} else {
				if (data === "")
				{
					document.getElementById("all-observations").innerHTML = "<tr><td>None</td><td>None</td></tr>";
				}
				else
				{
				    if (_auditType == "Architecture") {
				        $("#dynamic_observations").append(data);
				    }
				    else {
				        document.getElementById("all-observations").innerHTML = data;
				    }
				    
				}
			}
		},
		error: function (data) {
			//console.log(data);
		}
	});

	var sGetBugs = "/Action/GetBugs";
	$.ajax({
		url: sGetBugs,
		method: "post",
		contentType: "application/json; charset=utf-8",
		data: JSON.stringify({
			projectName: sProjectName,
			PMName: _pMId,
			AuditType: _auditType,
			Date: date
		}),
		dataType: "json",
		success: function (data) {
			if (data === 'Error') {
				document.getElementById("p0-bug").value = "Some error occurred";
				document.getElementById("p1-bug").value = "Some error occurred";

            }
            else {
                document.getElementById("p0-bug").value = data[0];
                document.getElementById("P0BugRedirect").setAttribute("href", data[1]);
                document.getElementById("p1-bug").value = data[2];
                document.getElementById("P1BugRedirect").setAttribute("href", data[3]);
            }
        },
        error: function (data) {
            //console.log(data);
        }
    });

    var sGetSubmitStatus = "/Action/GetSubmitStatus";
    $.ajax({
        url: sGetSubmitStatus,
        method: "post",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({ projectName: sProjectName, PMName: _pMId, AuditType: _auditType, Date: date }),
        dataType: "json",
        success: function (data) {
            if (data) {
                document.querySelector("#submit-audit-btn").innerHTML = "Update";
            }
            else {
                document.querySelector("#submit-audit-btn").innerHTML = "Submit";
            }
        },
        error: function (data) {
            alertMessages("0", "Some error occured in Submit status");
        }
    });

    let ajax = new XMLHttpRequest();
    ajax.open("POST", "/Action/GetPrevChecklist");
    ajax.setRequestHeader('Content-Type', 'application/json');
    ajax.addEventListener("load", function (e) {
        loader.classList.add("hidden");
        let json = e.currentTarget.responseText;
        if (json == '0' || json == '' || json == null) {
            resetResultTable();
            alertMessages("0", "Error while retrieving the previous checklist");
            return;
        }
        json = JSON.parse(json);
        let isApproved = json[0].approve;
        let checklistJSon = JSON.parse(json[0].checklist.replace(/&quot;/g, "\""));


        renderCheckListTable(checklistJSon, isApproved, _auditType);
        if (_auditType == "Architecture") {
            document.getElementById("dynamic_observations").classList.remove("hidden");
        }
        else {
            document.querySelector("#form-btn-container").classList.remove("hidden");
        }
        document.getElementById("buttonRow").classList.remove("hidden");
        document.getElementById("Bug-id-container").classList.remove("hidden");

    });

    ajax.addEventListener("error", function (e) {
        resetResultTable();
        alertMessages("0", "Server error");
    });
    ajax.send(params);

    const sleep = milliseconds => {
        return new Promise(resolve => setTimeout(resolve, milliseconds));
    };

    // sleep for 5000 miliiseconds and then execute function 
    if (_auditType == "Architecture") {
        sleep(3000).then(() => {
            var sGetPrevData = "/Action/GetPrevAuditData2";
            $.ajax({
                url: sGetPrevData,
                method: "post",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({
                    projectName: sProjectName,
                    PMName: _pMId,
                    AuditType: _auditType,
                    Date: date
                }),
                dataType: "json",
                success: function (data) {
                    if (data === 'Error') {
                        var result = $.parseJSON(data);
                        $.each(result, function (key, val) {
                            document.getElementById(key).value = val;
                        });
                    } else {
                        var result = $.parseJSON(data);
                        $.each(result, function (key, val) {
                            if (val) { //check if the values are not empty or null
                                document.getElementById(key).value = val;
                            }
                        });
                    }
                    if (document.getElementById("reason-select").value != "-1") {
                        $('input:radio[value="no-reason"]').prop('checked', true);
                        $('#requirement-checklist').hide();
                        $('#dynamic_observations').hide();
                        $('#parent-sign-off').hide();
                        $('#Bug-id-container').hide();
                        $('#reason-row').show();
                        $('#form-comment-row').show();
                    }
                    else {
                        $('input:radio[value="yes-reason"]').prop('checked', true);
                        $('#requirement-checklist').show();
                        $('#dynamic_observations').show();
                        $('#reason-row').hide();
                        $('#form-comment-row').hide();
                        $('#parent-sign-off').show();
                        $('#Bug-id-container').show();
                    }
                },
                error: function (ex) {
                    alertMessages("0", ex);
                }
            });
        });
    }

	
	$(".cancellable-row").remove();
	$('#addition-row').find('input').val("");
	$('.observation-severity').prop('selectedIndex', 0);
	$('.observation-area').prop('selectedIndex', 0);
	$('#frequency-select :nth-child(1)').prop('selected', true);
	$('#reason-select :nth-child(1)').prop('selected', true);
	$('#poc-bag').val("");
	$('#comments-bag').val("");
	document.getElementById("active-reviewer-row").classList.remove("hidden");
	document.getElementById("creator-row").classList.remove("hidden");
	document.getElementById("updaters-row").classList.remove("hidden");
	document.getElementById("prev-observations-table").classList.remove("hidden");
	document.getElementById("parent-sign-off").classList.remove("hidden");
	document.getElementById("Bug-id-container").classList.remove("hidden");
	document.getElementById("reviewers-bag").value = "";
	document.getElementById("url-bag").value = "";
	document.getElementById("all-observations").style.display = "";
	document.getElementById("url-bag").readOnly = true;
	loader.classList.remove("hidden");

}

let ajaxCount = 0;
let idEnableSelect;

function renderProjectManagers() {

	idEnableSelect = setInterval(enableSelect, 300);

	let loader = document.getElementById("loader-container");
	let PMNameSelect = document.getElementById("project-manager-name");

	let ajax1 = new XMLHttpRequest();
	ajax1.open("POST", "/Action/GetProjectManager");
	ajax1.addEventListener("load", function (e) {
		loader.classList.add("hidden");
		let json = e.currentTarget.responseText;
		if (json == '0' || json == '' || json == null) {
		    alertMessages("0", "No Manager");
			return;
		}
		json = JSON.parse(json);

		PMNameSelect.innerHTML = "";
		let html = `<option value="-1">Select</option>`;
		json.forEach(function (element, index) {
			html += `<option  value="${element.PMOwner}">${element.PMOwner}</option>`;

		});
		PMNameSelect.innerHTML = html;
		ajaxCount++;
	});
	ajax1.addEventListener("error", function (e) {
		ajaxCount++;
		alertMessages("0", "Server error");
	});
	ajax1.send();
	loader.classList.remove("hidden");
}

function renderProjects() {
	idEnableSelect = setInterval(enableSelect, 300);

	let loader = document.getElementById("loader-container");
	let ajax = new XMLHttpRequest();
	let projectNameselect = document.getElementById("project-name-select");

	projectNameselect.classList.add("no-pointer");

	ajax.open("POST", "/Action/GetProjects");
	ajax.addEventListener("load", function (e) {
		loader.classList.add("hidden");
		let json = e.currentTarget.responseText;
		if (json == '0' || json == '' || json == null) {
		    alertMessages("0", "No Projects available");
			return;
		}
		json = JSON.parse(json);
		oProjectsJSON = json;

		ajaxCount++;
	});
	ajax.addEventListener("error", function (e) {
		ajaxCount++;
		alertMessages("0", "Server error");
	});
	ajax.send();
}

//function renderReports() {
//    let _pMName = document.getElementById("project-manager-name").value;
//    let _projectId = document.getElementById("project-name-select").value;
//    let _reportName = document.getElementById("report-name-select");

//    params = {
//        'projectId': _projectId
//    }

//    $.ajax({
//        url: "/Action/GetReports",
//        method: "post",
//        contentType: "application/json; charset=utf-8",
//        data: JSON.stringify(params),
//        dataType: "json",
//        success: function (data) {
//            let html = `<option value="-1" selected>Select</option>`;
//            if (data == '' || data == '0' || data == null || data == undefined) {
//                html += "<option value='0' disabled>No reports available</option>"
//                _reportName.innerHTML = html;
//                return;
//            }
//            var jsonObject = $.parseJSON(data);
//            $.each(jsonObject, function (i, obj) {
//                html += `<option>${obj.ReportName}</option>`;
//            });
//            _reportName.innerHTML = html;
//            document.getElementById("report-name-select").selectedIndex = 0;
//        },
//        error: function (data) {
//            alert("0", "error in RenderProjects");
//        }
//    });
//}

function loadAllProjectsForTrimming() {
	idEnableSelect = setInterval(enableSelect, 300);
	let loader = document.getElementById("loader-container");
	let projectNameselect = document.getElementById("project-name-select");
	projectNameselect.classList.add("no-pointer");
	projectNameselect.innerHTML = '';
	let html = `<option value="-1">Select</option>`;
	oProjectsJSON.forEach(function (element, index) {
		html += `<option for="${element.PMOwner}" value="${element.ProjectID}">${element.ProjectName}</option>`;
	});
	projectNameselect.innerHTML += html;
}

function removeAllProjects() {
	let projectNameselect = document.getElementById("project-name-select");
	projectNameselect.innerHTML = '';
	let html = `<option>Please select a PM to proceed</option>`;
	projectNameselect.innerHTML += html;
}

//function removeAllReports() {
//    let reportNameSelect = document.getElementById("report-name-select");
//    reportNameSelect.innerHTML = '';
//    let html = `<option>Please select a Project to proceed</option>`;
//    reportNameSelect.innerHTML += html;
//}

function enableSelect() {
	if (ajaxCount == 2) {
		clearInterval(idEnableSelect);
		let projectNameselect = document.getElementById("project-name-select");
		projectNameselect.classList.remove("no-pointer");

		let PMNameSelect = document.getElementById("project-manager-name");
		PMNameSelect.classList.remove("no-pointer");
	}
}

function fillRequirementChecklistTable() {
	let loader = document.getElementById("loader-container");
	let tableContainer = document.querySelector("#requirement-checklist");
	tableContainer.innerHTML = "";

	let _auditType = document.getElementById("audit-type-select").value;

	document.getElementById("active-reviewer-row").classList.add("hidden");
	document.getElementById("creator-row").classList.add("hidden");
	document.getElementById("updaters-row").classList.add("hidden");
	document.getElementById("Bug-id-container").classList.add("hidden");
	document.getElementById("parent-sign-off").classList.add("hidden");
	document.getElementById("prev-observations-table").classList.add("hidden");
	document.getElementById("reviewers-bag").value = "";
	document.getElementById("url-bag").value = "";
	//$('#requirement-checklist').show();
	//$('#sign-off-container').show();
	//$('#Bug-id-container').show();
	//$('#dynamic_observations').show();

	let ajax = new XMLHttpRequest();
	ajax.open("POST", "/Action/GetCheckList");
	ajax.setRequestHeader('Content-Type', 'application/json');

	ajax.addEventListener("load", function (e) {
		loader.classList.add("hidden");
		let json = e.currentTarget.responseText;
		if (json == '0' || json == '' || json == null) {
			resetResultTable();
			let tableContainer = document.querySelector("#requirement-checklist");
			tableContainer.innerHTML = "<h6>No Data</h6>";
			//if (_auditType == "Architecture") {
	        //    document.getElementById("dynamic_observations").classList.add("hidden");
			//}
			//else {
	        //    document.querySelector("#form-btn-container").classList.add("hidden");
	        //}
			//document.getElementById("buttonRow").classList.add("hidden");

            return;
        }
        renderCheckListNewTable(JSON.parse(json), _auditType);
    });
    ajax.addEventListener("error", function (e) {
        resetResultTable();
        alertMessages("0", "Error GetCheckList");
    });

	ajax.send(JSON.stringify({
		auditType: _auditType
	}));
	loader.classList.remove("hidden");
	document.querySelector("#submit-audit-btn").innerText = "Submit";
	if (_auditType == "Architecture") {
	    document.getElementById("dynamic_observations").classList.remove("hidden");
	    document.querySelector("#form-btn-container").classList.add("hidden");
	    $('#audit-reasons').show();
	}
	else {
	    document.querySelector("#form-btn-container").classList.remove("hidden");
	    document.getElementById("dynamic_observations").classList.add("hidden");
	    $('#audit-reasons').hide();
	}
	document.getElementById("buttonRow").classList.remove("hidden");

}

function renderCheckListNewTable(checklist, auditType) {
    document.querySelector("#sign-off-container").innerHTML = "";
    let thead = `<thead>
            <tr>
                <th>Checklist</th>
                <th colspan="3" class="text-center">Yes/No/NA</th>
                <th>Comment</th>
            </tr>
    </thead>`;

	let tableContainer = document.querySelector("#requirement-checklist");
	tableContainer.innerHTML = "";

	let checklist_info = document.createElement('div');
	checklist_info.innerHTML = `<span id="checklist_info"><img src="/images/info.svg"> Please fill the comment box if you select N.A</span>`;
	tableContainer.appendChild(checklist_info);

	if (checklist == '' || checklist == undefined) {
		table.innerHTML = "<tr><td>No checklist available</td><tr/>";
	}

	let tr, td, textarea, fieldset, legend;
	let counter = 0;
	$.each(checklist, function (key, value) {


        fieldset = document.createElement('FIELDSET');
        legend = document.createElement('legend');
        legend.classList.add("table-legend");
        legend.appendChild(document.createTextNode(key));
        fieldset.appendChild(legend);

        table = document.createElement('table');
        table.classList.add("table", "table-bordered");
        if (auditType != 'PLT') {
            table.innerHTML = thead;
        }
        tbody = document.createElement('tbody');

		value.forEach(function (element, index) {

            tr = document.createElement("tr");
            td = document.createElement("td");
            td.style.width = "75%";
            td.setAttribute("name", `checklist-${counter}-${index}`);
            str = element.replace(/(?:\r\n|\r|\n)/g, '<span class="hint"><br>');
            td.innerHTML = str+"</span>";
            tr.appendChild(td);

            td = document.createElement("td");
            td.innerHTML = `<label class="radio"><input type="radio"  for="yes" name="status-radio-${counter}-${index}">
                                <span>Yes</span>
                        </label>`;
            if (auditType == 'PLT') {
                td.classList.add("hidden");
            }
            tr.appendChild(td);

            td = document.createElement("td");
            td.innerHTML = `<label class="radio"><input type="radio" for="no" checked="checked" name="status-radio-${counter}-${index}">
                                <span>No</span>
                        </label>`;
            if (auditType == 'PLT') {
                td.classList.add("hidden");
            }
            tr.appendChild(td);

            td = document.createElement("td");
            td.innerHTML = `<label class="radio"><input type="radio" for="na" name="status-radio-${counter}-${index}">
                                <span>NA</span>
                        </label>`;
            if (auditType == 'PLT') {
                td.classList.add("hidden");
            }
            tr.appendChild(td);

            

            td = document.createElement("td");
            textarea = document.createElement("textarea");
            textarea.setAttribute("name", `comment-${counter}-${index}`);
            if (auditType == 'PLT') {
                textarea.setAttribute("id", 'plt-text');
            }
            td.appendChild(textarea);
            tr.appendChild(td);

			tbody.appendChild(tr);
		});
		counter++;
		table.appendChild(tbody);
		fieldset.appendChild(table);

		tableContainer.appendChild(fieldset);
	});

	//if (auditType == "Architecture") {
	//    $('input:radio[value="yes-reason"]').prop('checked', true);
	//    $('#reason-row').hide();
	//    $('#form-comment-row').hide();
	//    $('dynamic_observations').show();
    //}
	$('#requirement-checklist').show()
	$(".cancellable-row").remove();
	$('#addition-row').find('input').val("");
	$('#frequency-select :nth-child(1)').prop('selected', true);
	$('#reason-select :nth-child(1)').prop('selected', true);
	$('#poc-bag').val("");
	$('#comments-bag').val("");
	$('.observation-severity').prop('selectedIndex', 0);
	$('.observation-area').prop('selectedIndex', 0);
	document.getElementById("p0-observations").value = "";
	document.getElementById("p1-observations").value = "";
	document.getElementById("url-bag").readOnly = false;
	document.getElementById("all-observations").style.display = "none";
}

function decodeHTMLEntities(htmlString) {
	let html = $('<textarea />').html(htmlString);
	let sConvertedString = html.text();
	sConvertedString = unescape(sConvertedString);
	return sConvertedString;
}


function renderCheckListTable(checklist, isApproved, auditType) {
    signOffRender(isApproved);

	let thead = `<thead>
            <tr>
                <th>Checklist</th>
                <th colspan="3" class="text-center">Yes/No/NA</th>
                <th>Comment</th>
            </tr>
    </thead>`;

	let tableContainer = document.querySelector("#requirement-checklist");
	tableContainer.innerHTML = "";

	if (checklist == '' || checklist == undefined) {
		table.innerHTML = "<tr><td>No checklist available</td><tr/>";
	}

	let tr, td, textarea, fieldset, legend;
	let counter = 0;

	checklist.forEach(function (element, index) {
		fieldset = document.createElement('FIELDSET');
		legend = document.createElement('legend');
		legend.appendChild(document.createTextNode(element[0]));
		legend.classList.add("table-legend");
		//legend.classList.add("text-center");
		fieldset.appendChild(legend);

        table = document.createElement('table');
        table.classList.add("table", "table-bordered");
        if (auditType != 'PLT') {
            table.innerHTML = thead;
        }
        tbody = document.createElement('tbody');

		element[1].forEach(function (ele, ind) {

			tr = document.createElement("tr");
			td = document.createElement("td");
			td.style.width = "750px";
			td.setAttribute("name", `checklist-${counter}-${ind}`);
			td.innerHTML = decodeHTMLEntities(ele["observations"]);
			tr.appendChild(td);

			let yes, no, na;
			if (ele["yesno"] == "Yes") {
				yes = "checked";
				no = "";
				na = "";
			} else if (ele["yesno"] == "No") {
				yes = "";
				no = "checked";
				na = "";
			} else {
				yes = "";
				no = "";
				na = "checked";
			}

            td = document.createElement("td");
            td.innerHTML = `<label class="radio"><input type="radio" for="yes" name="status-radio-${counter}-${ind}" ${yes}>
                                <span>Yes</span>
                            </label>`;
            if (auditType == 'PLT') {
                td.classList.add("hidden");
            }
            tr.appendChild(td);

            td = document.createElement("td");
            td.innerHTML = `<label class="radio"><input type="radio" for="no"  name="status-radio-${counter}-${ind}" ${no}>
                                <span>No</span>
                        </label>`;
            if (auditType == 'PLT') {
                td.classList.add("hidden");
            }
            tr.appendChild(td);

            td = document.createElement("td");
            td.innerHTML = `<label class="radio"><input type="radio" for="na"  name="status-radio-${counter}-${ind}" ${na}>
                                <span>NA</span>
                        </label>`;
            if (auditType == 'PLT') {
                td.classList.add("hidden");
            }
            tr.appendChild(td);

            td = document.createElement("td");
            textarea = document.createElement("textarea");
            textarea.setAttribute("name", `comment-${counter}-${ind}`);
            if (auditType == 'PLT') {
                textarea.setAttribute("id", 'plt-text')
            }
            textarea.innerText = decodeHTMLEntities(ele["comment"]);
            td.appendChild(textarea);
            tr.appendChild(td);

			tbody.appendChild(tr);
		});
		counter++;
		table.appendChild(tbody);
		fieldset.appendChild(table);

		tableContainer.appendChild(fieldset);
	});

}
let container = document.querySelector("#sign-off-container");
let appr = `<div class ="alert alert-success" role="alert" style="width:100%">
                <img src="/images/approveIcon.svg" alt="Approved" class="sign-off-img"><span class ="sign-off-header"> Audit request approved.</span></div>`;

let rej = `<div class ="alert alert-danger" role="alert" style="width:100%">
               <img src="/images/rejectIcon.svg" alt="Rejected" class ="sign-off-img"><span class ="sign-off-header"> Audit request rejected.</span></div>`;

let html = `<div class="col-md-6">
	    <div class="row">
		    <img src="/images/exclaimationIcon.svg" alt="Exclaimation Icon" class ="sign-off-img">
            <span class="sign-off-header">Audit Request: </span>
	    </div>
    </div>
    <div class="col-md-6">
	    <div class="row" id="btn-sign-off">
		    <div class="col-md-6">
			    <button id="approve-btn" data-toggle="tooltip" data-placement="bottom" title="Approves the audit by changing status in database" onclick="approveRejectAudit(1)"><span>Approve</span></button>
		    </div>
		    <div class="col-md-6">
			    <button id="reject-btn" data-toggle="tooltip" data-placement="bottom" title="Rejects the audit by changing status in database" onclick="approveRejectAudit(0)"><span>Reject</span></button>
		    </div>
	    </div>
    </div>`;

function signOffRender(isApproved) {



	if (isApproved == 1) {
		container.classList.remove("alert", "alert-secondary");
		container.removeAttribute("role", "alert");
		container.innerHTML = appr;

	} else if (isApproved == -1) {
		container.classList.remove("alert", "alert-secondary");
		container.removeAttribute("role", "alert");
		container.innerHTML = rej;
	} else {
		container.innerHTML = html;
		container.classList.add("alert", "alert-secondary");
		container.setAttribute("role", "alert");
	}

	$('#approve-btn').tooltip();
	$('#reject-btn').tooltip();

}


function approveRejectAudit(flag) {

	let _pMId = document.getElementById("project-manager-name").value;
	let _projectId = document.getElementById("project-name-select").value;
	let _auditType = document.getElementById("audit-type-select").value;
	let auditDateTime = document.querySelector("#audit-select-box").value;
	let _projectName = $("#project-name-select option:selected").text();

	let params = {
		'projectId': _projectId,
		'auditType': _auditType,
		'projectManagerId': _pMId,
		'dateTime': auditDateTime,
		'projectName': _projectName,
		'flag': flag,
		'username': sEmailId
	}
	params = JSON.stringify(params);

	let approveBtn = document.getElementById("approve-btn");
	let rejectBtn = document.getElementById("reject-btn");
	let ajax = new XMLHttpRequest();
	ajax.open("POST", "/action/ApproveRejectAudit");
	ajax.setRequestHeader('Content-Type', 'application/json');
	ajax.addEventListener("load", function (e) {
	    let data = e.currentTarget.responseText;
	    if (data == "success") {
	        if (flag == 1) {
	            container.classList.remove("alert", "alert-secondary");
	            container.removeAttribute("role", "alert");
	            container.innerHTML = appr;
	        } else if (flag == 0) {
	            container.classList.remove("alert", "alert-secondary");
	            container.removeAttribute("role", "alert");
	            container.innerHTML = rej;
	        }
	    }
	    else if(data == "restricted") {
	        alertMessages("2", "Only reviewers can approve or reject audit");
	        container.innerHTML = html;
	        container.classList.add("alert", "alert-secondary");
	        container.setAttribute("role", "alert");
	    }
		else {
	        alertMessages("0", "Error approveRejectAudit");
		}
	});
	ajax.upload.addEventListener("progress", function (e) {
		approveBtn.classList.add("no-pointer");
		rejectBtn.classList.add("no-pointer");
		if (flag == 1) {
			approveBtn.firstChild.innerText = "Waiting ...";
		} else {
			rejectBtn.firstChild.innerText = "Waiting ...";
		}
	});
	ajax.addEventListener("error", function (e) {
		approveBtn.classList.remove("no-pointer");
		rejectBtn.classList.remove("no-pointer");
		approveBtn.firstChild.innerText = "Approve";
		rejectBtn.firstChild.innerText = "Reject";
		alertMessages("0", "Error approveRejectAudit");
	});
	ajax.send(params);

}

var sGetEmailid = "/Account/GetEmailId";
var sEmailId = "";
$.ajax({
	url: sGetEmailid,
	contentType: "application/json; charset=utf-8",
	dataType: "json",
	success: populateEmail,
	error: function (a, b, error) { }
});

function populateEmail(email) {
	sEmailId = email;
	renderChecklist();
}

//function submitMultipleObservations() {
//    let comments = [];
//    let _severity = [];
//    let _area = [];
//    let _pMName = document.getElementById("project-manager-name").value;
//    let _projectId = document.getElementById("project-name-select").value;
//    let _auditType = document.getElementById("audit-type-select").value;

//    var areaField = document.getElementsByClassName("observation-area");
//    for (var i = 0; i < areaField.length; i++) {
//        _area[i] = areaField[i].options[areaField[i].selectedIndex].value;
//    }

//    var severityField = document.getElementsByClassName("observation-severity");
//    for (var i = 0; i < severityField.length; i++) {
//        _severity[i] = severityField[i].options[severityField[i].selectedIndex].value;
//    }

//    var field = document.getElementById("dynamic_observations");
//    var tags = field.getElementsByTagName("input");
//    for (var i = 0; i < tags.length; i++) {
//        comments[i] = tags[i].value;
//    }



//    let params = {
//        'projectId': _projectId,
//        'projectManagerId': _pMName,
//        'auditDate': "",
//        'auditType': _auditType,
//        'comments': comments,
//        'severity': _severity,
//        'area': _area
//    }
//    let _date = document.getElementById("audit-select-box").value;
//    if (_date == "-1") {
//        params.auditDate = "0";
//    }
//    else {
//        params.auditDate = _date;
//    }

//    params = JSON.stringify(params)

//    let ajax = new XMLHttpRequest();
//    ajax.open("POST", "/Action/SubmitMultipleObservations");
//    ajax.setRequestHeader('Content-Type', 'application/json');
//    ajax.addEventListener("load", function (e) {
//        let json = e.currentTarget.responseText;
//        if (json == '0' || json == null) {
//            alertMessages("0", "Error in inserting comments");
//            return;
//        }
//    });
//    ajax.addEventListener("error", function (e) {
//        resetResultTable();
//        alertMessages("0", "Server error");
//    });
//    ajax.send(params)
//}

function submitAudit(sSaveOrSubmit) {


    let sReviewers = document.getElementById("reviewers-bag").value;
    let sUrl = document.getElementById("url-bag").value;
    sReviewers = sReviewers.replace(/;/g, ",");
    let sReviewersSplitted = sReviewers.split(",");

	if (sReviewersSplitted == "") {
	    alertMessages("2", "Please enter reviewer name(s)");
		return;
	}

	for (let iIteratorToValidateReviewers = 0; iIteratorToValidateReviewers < sReviewersSplitted.length; iIteratorToValidateReviewers++) {
		if (!sReviewersSplitted[iIteratorToValidateReviewers].includes("maqsoftware.com") && sReviewersSplitted[iIteratorToValidateReviewers] != "") {
		    alertMessages("2", "Please enter valid email aliases of reviewers (only maqsoftware.com)");
			return;
		}
	}

	let loader = document.getElementById("loader-container");

	let _pMId = document.getElementById("project-manager-name").value;

	let _p0Observations = escape(document.getElementById("p0-observations").value);
	let _p1Observations = escape(document.getElementById("p1-observations").value);

	let _projectId = document.getElementById("project-name-select").value;
	let _auditType = document.getElementById("audit-type-select").value;

	let _mulObservations = [];
	let _severity = [];
	let _area = [];
	let _frequency = $('#frequency-select').val();
	let _poc = $('#poc-bag').val();
	let _reason = $('#reason-select').val();
	let _comments = $('#comments-bag').val();


	if (_auditType == "-1") {
	    alertMessages("2", "Choose audit type");
		return;
	}
	if (_projectId == "-1") {
	    alertMessages("2", "Choose Project");
		return;
	}
	if (_pMId == "-1") {
	    alertMessages("2", "Choose Project Manager");
		return;
	}

	if (_auditType == "Architecture") {

	    if (!_poc.includes("maqsoftware.com") && _poc != "") {
	        alertMessages("2", "Autosave : Please enter valid email aliases of poc (only maqsoftware.com)");
	        return;
	    }

	    if (_frequency == "-1") {
	        alertMessages("2", "Choose frequency of review");
	        return;
	    }

	    if (!$("input:radio[value='no-reason']").is(":checked")) {
	        var field = document.getElementById("dynamic_observations");
	        var tags = field.getElementsByTagName("input");

	        var severityField = document.getElementsByClassName("observation-severity");
	        for (var i = 0; i < severityField.length; i++) {
	            if ((tags[i].value != "") && (severityField[i].selectedIndex == 0)) {
	                alertMessages("2", "Select a value from the Severity dropdown");
	                return;
	            }
	        }

	        var areaField = document.getElementsByClassName("observation-area");
	        for (var i = 0; i < areaField.length; i++) {
	            if ((tags[i].value != "") && (areaField[i].selectedIndex == 0)) {
	                alertMessages("2", "Select a value from the Area dropdown");
	                return;
	            }
	        }
	    }


	    var areaField = document.getElementsByClassName("observation-area");
	    for (var i = 0; i < areaField.length; i++) {
	        _area[i] = areaField[i].options[areaField[i].selectedIndex].value;
	    }

	    var severityField = document.getElementsByClassName("observation-severity");
	    for (var i = 0; i < severityField.length; i++) {
	        _severity[i] = severityField[i].options[severityField[i].selectedIndex].value;
	    }

	    var field = document.getElementById("dynamic_observations");
	    var tags = field.getElementsByTagName("input");
	    for (var i = 0; i < tags.length; i++) {
            _mulObservations[i] = tags[i].value;
	    }
	}

	let fieldset = document.querySelectorAll("#requirement-checklist fieldset");
	let counter = 0;
	let map = new Map();
	let flagForCommentsMandate = 0;
	let iYesCount = 0;
	let iNoCount = 0;
	let iAuditScore = 0;

	fieldset.forEach(function (element, ind) {
		let tableTitle = element.firstChild.innerText;
		let table = element.childNodes[1].querySelectorAll("tbody tr");

        let list = [];
        for (let index = 0; index < table.length; index++) {
            let rec = document.getElementsByName(`checklist-${counter}-${index}`)[0].innerHTML;

			let yesno = document.querySelector(`input[name='status-radio-${counter}-${index}']:checked`);
			yesno = yesno.getAttribute('for');

			let com = escape(document.getElementsByName(`comment-${counter}-${index}`)[0].value);

			if (rec == '' || rec == undefined) {
				rec = "NA";
			}
			if (yesno == '' || yesno == undefined) {
				yesno = "no";
			}
			if (com == '' || com == undefined) {
				com = "";
			}

			if (yesno === "na" && com === "") {
			    alertMessages("2", "Comment section is mandatory when NA is selected");
				flagForCommentsMandate = 1;
				return;
			}

			if (yesno === "yes") {
				iYesCount++;
			}
			if (yesno === "no") {
				iNoCount++;
			}

			let obj = {};
			obj.observations = rec;
			obj.yesno = (yesno == "yes") ? "Yes" : (yesno == "no") ? "No" : "NA";
			obj.comment = com;
			list.push(obj);
		}

		counter++;
		map.set(tableTitle, list);
	});

	if (flagForCommentsMandate === 1) {
		return;
	}

    iAuditScore = iYesCount / (iYesCount + iNoCount);
    iAuditScore = Math.round(iAuditScore * 100) / 100;
    
    var submitData = {
        projectId: _projectId,
        auditType: _auditType,
        projectManagerId: _pMId,
        p0Observations: _p0Observations,
        p1Observations: _p1Observations,
        updatedBy: sEmailId,
        list: JSON.stringify([...map]),
        flag: "",
        saveOrSubmit: sSaveOrSubmit,
        reviewers: sReviewers,
        auditScore: iAuditScore,
        projectURL: sUrl,
        submitStatus: 0,
        frequency: _frequency,
        poc: _poc,
        reason: _reason,
        comments: _comments
    };

    let auditDate = document.getElementById("audit-select-box").value;
    if (auditDate == "-1") {
        submitData.flag = "0";
    }
    else {
        submitData.flag = auditDate;
    }

    if (sSaveOrSubmit == "Submit") {
        submitData.submitStatus = 1;
    }
    else {
        submitData.submitStatus = 0;
    }

    submitData = JSON.stringify({ details: submitData, callAutoSave: false, mulObservations: _mulObservations, severity: _severity, area: _area });

    let url = "/Action/SubmitAudit";
    let ajax = new XMLHttpRequest();
    ajax.open("POST", url);
    ajax.setRequestHeader('Content-Type', 'application/json');

	ajax.addEventListener("load", function (e) {
		loader.classList.add("hidden");
		let data = e.currentTarget.responseText;
		if (data == '0') {
		    alertMessages("0", "Error");
		} else {
			resetResultTable();
			if (sSaveOrSubmit === "Submit") {
			    alertMessages("1", "Successfully submitted the audit and mail sent");
			} else if (sSaveOrSubmit === "Save") {
			    alertMessages("1", "Successfully saved the audit. Click on Submit/Update button to send mail.");
			}
		}
		document.querySelector("#sign-off-container").innerHTML = "";
	});
	ajax.addEventListener("error", function (e) {
	    alertMessages("0", "Server error");
		document.querySelector("#sign-off-container").innerHTML = "";
	});
	ajax.send(submitData);
	//if (_auditType == "Architecture") {
	//    submitMultipleObservations();
    //}
	loader.classList.remove("hidden");
}

function autoSaveAudit(sSaveOrSubmit) {

    let sReviewers = document.getElementById("reviewers-bag").value;
    let sUrl = document.getElementById("url-bag").value;
    sReviewers = sReviewers.replace(/;/g, ",");
    let sReviewersSplitted = sReviewers.split(",");

    if (sReviewersSplitted == "") {
        alertMessages("2", "Autosave : Please enter reviewer name(s)");
        return;
    }

    for (let iIteratorToValidateReviewers = 0; iIteratorToValidateReviewers < sReviewersSplitted.length; iIteratorToValidateReviewers++) {
        if (!sReviewersSplitted[iIteratorToValidateReviewers].includes("maqsoftware.com") && sReviewersSplitted[iIteratorToValidateReviewers] != "") {
            alertMessages("2", "Autosave : Please enter valid email aliases of reviewers (only maqsoftware.com)");
            return;
        }
    }

    let loaderAutoSync = document.getElementById("navbar-autosave-sync");
    let loader = document.getElementById("navbar-autosave");
    loaderAutoSync.classList.remove("hidden");
    loader.classList.add("hidden");

    let _pMId = document.getElementById("project-manager-name").value;

    let _p0Observations = escape(document.getElementById("p0-observations").value);
    let _p1Observations = escape(document.getElementById("p1-observations").value);

    let _projectId = document.getElementById("project-name-select").value;
    let _auditType = document.getElementById("audit-type-select").value;

    let _mulObservations = [];
    let _severity = [];
    let _area = [];
    let _frequency = $('#frequency-select').val();
    let _poc = $('#poc-bag').val();
    let _reason = $('#reason-select').val();
    let _comments = $('#comments-bag').val();


    if (_auditType == "-1") {
        alertMessages("2", "Choose audit type");
        return;
    }
    if (_projectId == "-1") {
        alertMessages("2", "Choose Project");
        return;
    }
    if (_pMId == "-1") {
        alertMessages("2", "Choose Project Manager");
        return;
    }

    if (_auditType == "Architecture") {

        if (!_poc.includes("maqsoftware.com") && _poc != "") {
            alertMessages("2", "Autosave : Please enter valid email aliases of poc (only maqsoftware.com)");
            return;
        }

        if (_frequency == "-1") {
            alertMessages("2", "Choose frequency of review");
            return;
        }

        if (!$("input:radio[value='no-reason']").is(":checked")) {
            var field = document.getElementById("dynamic_observations");
            var tags = field.getElementsByTagName("input");

            var severityField = document.getElementsByClassName("observation-severity");
            for (var i = 0; i < severityField.length; i++) {
                if ((tags[i].value != "") && (severityField[i].selectedIndex == 0)) {
                    alertMessages("2", "Select a value from the Severity dropdown");
                    return;
                }
            }

            var areaField = document.getElementsByClassName("observation-area");
            for (var i = 0; i < areaField.length; i++) {
                if ((tags[i].value != "") && (areaField[i].selectedIndex == 0)) {
                    alertMessages("2", "Select a value from the Area dropdown");
                    return;
                }
            }
        }


        var areaField = document.getElementsByClassName("observation-area");
        for (var i = 0; i < areaField.length; i++) {
            _area[i] = areaField[i].options[areaField[i].selectedIndex].value;
        }

        var severityField = document.getElementsByClassName("observation-severity");
        for (var i = 0; i < severityField.length; i++) {
            _severity[i] = severityField[i].options[severityField[i].selectedIndex].value;
        }

        var field = document.getElementById("dynamic_observations");
        var tags = field.getElementsByTagName("input");
        for (var i = 0; i < tags.length; i++) {
            _mulObservations[i] = tags[i].value;
        }
    }

    let fieldset = document.querySelectorAll("#requirement-checklist fieldset");
    let counter = 0;
    let map = new Map();
    let flagForCommentsMandate = 0;
    let iYesCount = 0;
    let iNoCount = 0;
    let iAuditScore = 0;

    fieldset.forEach(function (element, ind) {
        let tableTitle = element.firstChild.innerText;
        let table = element.childNodes[1].querySelectorAll("tbody tr");

        let list = [];
        for (let index = 0; index < table.length; index++) {
            let rec = document.getElementsByName(`checklist-${counter}-${index}`)[0].innerHTML;

            let yesno = document.querySelector(`input[name='status-radio-${counter}-${index}']:checked`);
            yesno = yesno.getAttribute('for');

            let com = escape(document.getElementsByName(`comment-${counter}-${index}`)[0].value);

            if (rec == '' || rec == undefined) {
                rec = "NA";
            }
            if (yesno == '' || yesno == undefined) {
                yesno = "no";
            }
            if (com == '' || com == undefined) {
                com = "";
            }

            if (yesno === "na" && com === "") {
                alertMessages("2", "Autosave : Comment section is mandatory when NA is selected");
                flagForCommentsMandate = 1;
                return;
            }

            if (yesno === "yes") {
                iYesCount++;
            }
            if (yesno === "no") {
                iNoCount++;
            }

            let obj = {};
            obj.observations = rec;
            obj.yesno = (yesno == "yes") ? "Yes" : (yesno == "no") ? "No" : "NA";
            obj.comment = com;
            list.push(obj);
        }

        counter++;
        map.set(tableTitle, list);
    });

    if (flagForCommentsMandate === 1) {
        return;
    }

    iAuditScore = iYesCount / (iYesCount + iNoCount);
    iAuditScore = Math.round(iAuditScore * 100) / 100;

    var submitData = {
        projectId: _projectId,
        auditType: _auditType,
        projectManagerId: _pMId,
        p0Observations: _p0Observations,
        p1Observations: _p1Observations,
        updatedBy: sEmailId,
        list: JSON.stringify([...map]),
        flag: "",
        saveOrSubmit: sSaveOrSubmit,
        reviewers: sReviewers,
        auditScore: iAuditScore,
        projectURL: sUrl,
        submitStatus: 0,
        frequency: _frequency,
        poc: _poc,
        reason: _reason,
        comments: _comments
    };

    let auditDate = document.getElementById("audit-select-box").value;
    if (auditDate == "-1") {
        submitData.flag = "0";
    }
    else {
        submitData.flag = auditDate;
    }

    submitData = JSON.stringify({ details: submitData, callAutoSave: true, mulObservations: _mulObservations, severity: _severity, area: _area });


    let url = "/Action/SubmitAudit";
    let ajax = new XMLHttpRequest();
    ajax.open("POST", url);
    ajax.setRequestHeader('Content-Type', 'application/json'); 

    ajax.addEventListener("load", function (e) {
        let data = e.currentTarget.responseText;
        if (data == '0') {
            alertMessages("0", "Error");
        }
        else {
            if (sSaveOrSubmit === "Save") {
                loaderAutoSync.classList.add("hidden");
                loader.classList.remove("hidden");
            }
        }
    });
    ajax.addEventListener("error", function (e) {
        alertMessages("0", "Server error");
        loaderAutoSync.classList.add("hidden");
        loader.classList.remove("hidden");
    });
    ajax.send(submitData);

    if (auditDate == "-1") {
        let select = document.querySelector("#audit-select-box");

        let params = {
            'projectId': _projectId,
            'auditType': _auditType,
            'projectManagerId': _pMId,
        }

        $.ajax({
            url: "/Action/GetPreviousAuditDates",
            method: "post",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify( params ),
            dataType: "json",
            success: function (data) {
                let json = data;
                let html = `<option value="-1" selected>New Audit</option>`;
                if (json == '' || json == '0' || json == null || json == undefined) {
                    html += "<option value='0' disabled>No previous audit available</option>"
                    select.innerHTML = html;
                    return;
                }
                json.forEach(function (element, index) {
                    let datetime = element.DateTime.substring(0, element.DateTime.indexOf('.')).replace("T", "  ");
                    html += `<option value="${element.DateTime}">${datetime}</option>`
                });
                select.innerHTML = html;
                document.getElementById("audit-select-box").selectedIndex = 1;
            },
            error: function (data) {
                //console.log(data);
            }
        });
        
    }
    //if (_auditType == "Architecture") {
    //    submitMultipleObservations();
    //}
}


function resetResultTable() {
	let table = document.querySelector("#requirement-checklist");
	table.innerHTML = "";
	$(".cancellable-row").remove();
	$('#addition-row').find('input').val("");
	$('.observation-severity').prop('selectedIndex', 0);
	$('.observation-area').prop('selectedIndex', 0);
	$('#frequency-select :nth-child(1)').prop('selected', true);
	$('#reason-select :nth-child(1)').prop('selected', true);
	$('#poc-bag').val("");
	$('#comments-bag').val("");
	$('#audit-reasons').hide();
	$('input:radio[value="yes-reason"]').prop('checked', true);
	document.getElementById("p0-observations").value = "";
	document.getElementById("p1-observations").value = ""
	document.getElementById("all-observations").value = "";
	document.getElementById("active-reviewer-row").classList.add("hidden");
	document.getElementById("creator-row").classList.add("hidden");
	document.getElementById("updaters-row").classList.add("hidden");
	document.getElementById("parent-sign-off").classList.add("hidden");
	document.getElementById("url-bag").readOnly = false;
	document.getElementById("url-bag").value = "";
	document.getElementById("reviewers-bag").value = "";
	document.getElementById("dynamic_observations").classList.add("hidden");
	document.getElementById("form-btn-container").classList.add("hidden");
	document.getElementById("buttonRow").classList.add("hidden");
	document.getElementById("Bug-id-container").classList.add("hidden");

	let auditTypeSelectBox = document.querySelector("#audit-type-select");
	auditTypeSelectBox.selectedIndex = 0;

	let auditDateSelectBox = document.querySelector("#audit-select-box")
	auditDateSelectBox.innerHTML = "<option value='-1'>New Audit</option>";
	auditDateSelectBox.selectedIndex = 0;
}

function alertMessages(messageType, messageText) {
    $("#modal-message").text(messageText);
    if (messageType == "0") {
        $("#modal-title").text("Error");
        $("#closeButton").removeClass('btn-success');
        $("#closeButton").removeClass('btn-info');
        $("#closeButton").addClass('btn-danger');
    }
    else if(messageType == "1"){
        $("#modal-title").text("Success");
        $("#closeButton").removeClass('btn-danger');
        $("#closeButton").removeClass('btn-info');
        $("#closeButton").addClass('btn-success');
    }
    else {
        $("#modal-title").text("Info");
        $("#closeButton").removeClass('btn-danger');
        $("#closeButton").removeClass('btn-success');
        $("#closeButton").addClass('btn-info');
    }
    $('#myModal').css('display', 'block');
    $('#myModal').css('background-color', 'RGBA(0,0,0,0.4)');
    $('#myModal').css('opacity', '1');
}

function closePopUp() {
    $('#myModal').css('display', 'none');
    $('#myModal').css('opacity', '0');
}

//session timeout pop up code in case of future use
//var funInterval, sessionInterval;
//function SessionExpireAlert(timeout) {
//	var seconds = timeout ;
//	var flag = 0;
//	funInterval = function () {
//		seconds--;
//		if (seconds<=5) {
//			if (0 >= seconds) {
//				$("#sessionTime").text('Session Expired!!');
//				//$("#popUpHead").text('Session Expired!');
//				//$("#popUpText").text('Click "Reload" to reload the page');
//				clearInterval(sessionInterval);

//			} else {
//				$("#sessionTime").text(seconds.toString());
				

//			}
			
//			$('#myModal').css('display', 'block');
//			$('#myModal').css('background-color', 'RGBA(0,0,0,0.4)');
//			$('#myModal').css('opacity', '1');
//		}

//	};
//	sessionInterval = setInterval(funInterval, 1000);
//}
//function ResetSession() {
//	//Redirect to refresh Session.
//	if ((window.location.href.toString()).indexOf("Home/Index") != -1) {
//		window.location.search = ReportPages.sCurrentSelectedSubTab.toLowerCase().replace(" ", "");
//		document.location.reload();
//	} else {
//		document.location.reload();
//	}
//}
//function ClosePopUp()
//{
//	clearInterval(sessionInterval);
//	$('#myModal').css('display', 'none');
//	$('#myModal').css('opacity', '0');
//}
//function getSessionInterval() {
//	$.ajax({
//		type: 'POST',
//		url: '/Account/SessionInterval',
//		success: function (interval) {
//			SessionExpireAlert(interval);
//		}
//	});
//}
