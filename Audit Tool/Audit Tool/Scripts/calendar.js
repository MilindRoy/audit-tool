﻿$(document).ready(function () {
    $(window).resize(function () {
        calendar.setOption("height", getCalendarHeight());
        calendar.render();
    });

    getUserName();

    $('#navbar-calender').tooltip();
    $('#ulist').tooltip();
});

function getUserName() {

    let sUserNameURL = "/Account/GetUsername";
    //Make Ajax request to get user name
    $.ajax({
        url: sUserNameURL,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: populateUsername,
        error: function () {
            var userLabel = document.getElementById("userLabel");
            userLabel.innerHTML = "Username failure";
        }
    });
}

function populateUsername(username) {
    let userLabel = document.getElementById("userLabel");
    let userIcon = document.getElementById("userLabelDiv");
    userLabel.innerHTML = "Hello, " + username;
    userIcon.setAttribute("data-toggle", "tooltip");
    userIcon.setAttribute("data-placement", "bottom");
    userIcon.setAttribute("title", userLabel.innerHTML);
    $('#userLabelDiv').tooltip();
    userName = username;
}

/*jslint browser:true */
var rruleWeekDays = new Map();
rruleWeekDays.sunday = "su";
rruleWeekDays.monday = "mo";
rruleWeekDays.tuesday = "tu";
rruleWeekDays.wednesday = "we";
rruleWeekDays.thursday = "th";
rruleWeekDays.friday = "fr";
rruleWeekDays.saturday = "sa";
var weekOfMonth = new Map();
weekOfMonth.first = "1";
weekOfMonth.second = "2";
weekOfMonth.third = "3";
weekOfMonth.fourth = "4";
weekOfMonth.last = "-1";

//To display user list
function displayList() {
    var x = document.getElementById("userlist");
    var y = document.getElementById("desc");
    if (x.style.display == "none") {
        x.style.display = "inline-block";
        y.style.height = "30%";
        x.style.height = "68%";
    } else {
        x.style.display = "none";
        y.style.height = "100%";
    }
}
//To convert time from milliseconds to hrs:min:sec
function msToHMS(ms) {
    // 1- Convert to seconds:
    var seconds = ms / 1000;
    // 2- Extract hours:
    var hours = parseInt(seconds / 3600); // 3,600 seconds in 1 hour
    seconds = seconds % 3600; // seconds remaining after extracting hours
    // 3- Extract minutes:
    var minutes = parseInt(seconds / 60); // 60 seconds in 1 minute
    // 4- Keep only seconds not extracted to minutes:
    seconds = seconds % 60;
    return (hours + ":" + minutes + ":" + seconds);
}
var endDate;
var groupIds = [];
var calendarEl = document.getElementById("calendar");
var event;
var users;
var groupColorCodes = new Map();
var groupColorDeselect = new Map();
var groupColorSelect = new Map();
var select = 1;
var eventsArray = [];
var groupEvents = [];
var clickedIds = [];
var legends = document.getElementById("slideContainer");

// To change height of calendar everytime window size changes.
function getCalendarHeight() {
    return $("#calendar-body").height() * 0.99;
}


//set calendar params for our calendar
var calendar = new FullCalendar.Calendar(calendarEl, {
    lang: "en",
    eventLimit: true,
    views: {
        month: {
            eventLimit: 3
        },
        timeGridWeek: {
            eventLimit: 3
        },
        timeGridDay: {
            eventLimit: 3
        }
    },
    nowIndicator: true,
    slotEventOverlap: false,
    displayEventTime: true,
    height: getCalendarHeight,
    //aspectRatio:1,
    plugins: ["interaction", "dayGrid", "timeGrid", "list", "rrule"],
    defaultView: "timeGridWeek",
    defaultDate: new Date(),
    timeZone: "local",
    eventRender: function (info) {
        var tooltip;
        if (info.event.end == null) {
            tooltip = new Tooltip(info.el, {
                title: [info.event.extendedProps.extraParams
                    .grp + "\n" + info.event.title +
                    "\n" + "\n" + moment(info.event
                        .start).format(
                            "YYYY/MM/DD HH:mm") + "\n" +
                    /*ignore jslint start*/
                    moment.utc(info.event._instance
                        .range.end).format(
                            "YYYY/MM/DD HH:mm")
                ],
                /*ignore jslint end*/
                placement: "top",
                trigger: "hover",
                container: "body"
            });
        } else {
            tooltip = new Tooltip(info.el, {
                title: [info.event.extendedProps.extraParams
                    .grp + "\n" + info.event.title +
                    "\n" + moment(info.event.start)
                        .format("YYYY/MM/DD HH:mm") + "\n" +
                    moment(info.event.end).format(
                        "YYYY/MM/DD HH:mm")
                ],
                placement: "top",
                trigger: "hover",
                container: "body"
            });
        }
    },
    eventClick: function (info) {
        var reqAttendees = "<ul>";
        info.event.extendedProps.extraParams.Req_attendees.forEach(
            function (e) {
                reqAttendees += "<li>" + e + "</li>";
            });
        reqAttendees += "</ul>";
        var optAttendees = "<ul>";
        info.event.extendedProps.extraParams.Op_attendees.forEach(
            function (e) {
                optAttendees += "<li>" + e + "</li>";
            });
        optAttendees += "</ul>";
        document.getElementById("desc1").innerHTML =
            "<b>ORGANISER:</b><br>" + info.event.extendedProps
                .extraParams.Organiser + "<br><b>REQUIRED:</b><br>" +
            reqAttendees + "<b>OPTIONAL:</b><br>" + optAttendees;
    },
    header: {
        left: "prev,next today",
        center: "title",
        right: "dayGridMonth,timeGridWeek,timeGridDay,listMonth"
    },
    events: eventsArray
});
const $source = document.querySelector("#source");
const $result = document.querySelector("#result");
const typeHandler = function (e) {
    var options = {
        shouldSort: true,
        threshold: 0.6,
        location: 0,
        distance: 100,
        maxPatternLength: 32,
        minMatchCharLength: 1,
        keys: ["Name"]
    };
    $(".demo").remove();
    var fuse = new Fuse(users, options); // "list" is the item array
    var result = fuse.search(e.target.value);
    var userList;
    var user;
    var input;
    var label;
    if (e.target.value != "") {
        userList = $("#userinfo")[0];
        result.forEach(function (option) {
            user = option;
            input = document.createElement("input");
            input.type = "radio";
            input.name = "Users";
            input.value = user.ID;
            label = document.createElement("label");
            label.setAttribute("class", "demo");
            label.htmlFor = user.Name;
            label.appendChild(input);
            label.appendChild(document.createTextNode(user.Name));
            var br = document.createElement("br");
            br.setAttribute("class", "demo");
            userList.appendChild(label);
            userList.appendChild(br);
        });
        $("input[name=Users]").on("click", countChecked);
    } else {
        userList = $("#userinfo")[0];
        users.forEach(function (option) {
            user = option;
            input = document.createElement("input");
            input.type = "radio";
            input.name = "Users";
            input.value = user.ID;
            label = document.createElement("label");
            label.setAttribute("class", "demo");
            label.htmlFor = user.Name;
            label.appendChild(input);
            label.appendChild(document.createTextNode(user.Name));
            var br = document.createElement("br");
            br.setAttribute("class", "demo");
            userList.appendChild(label);
            userList.appendChild(br);
        });
        $("input[name=Users]").on("click", countChecked);
    }
};
$source.addEventListener("input", typeHandler); // register for oninput
$source.addEventListener("propertychange", typeHandler); // for IE8

//ajax call to fetch data of all tenant members
var getUserData = function () {
    $.ajax({
        url: "/Calendar/GetAllUsersData",
        method: "get",
        dataType: "json",
        success: function (data) {
            users = data;
            var userList = $("#userinfo")[0];
            var input;
            var user;
            var label;
            users.forEach(function (option) {
                user = option;
                input = document.createElement("input");
                input.type = "radio";
                input.name = "Users";
                input.value = user.ID;
                label = document.createElement("label");
                label.setAttribute("class", "demo");
                label.htmlFor = user.Name;
                label.appendChild(input);
                label.appendChild(document.createTextNode(user.Name));
                var br = document.createElement("br");
                br.setAttribute("class", "demo");
                userList.appendChild(label);
                userList.appendChild(br);
            });
            $("input[name=Users]").on("click", countChecked);
        }
    });
};

//ajax call to fetch the data of default calendar view i.e code review and architecture review groups
var getData = function () {
    $.ajax({
        url: "/Calendar/GetSignedInUserDataJD",
        method: "get",
        dataType: "json",
        success: function (data) {
            event = data;
            eventsArray = [];
            groupColorSelect.clear();
            groupColorDeselect.clear();
            groupColorSelect.set("18fe27c1-ff5f-48df-9279-990c673b9bb4",
                "#192F54");
            groupColorSelect.set("2444413c-14c0-4b6a-87be-e7520967ed92",
                "#408E45");
            groupColorDeselect.set("18fe27c1-ff5f-48df-9279-990c673b9bb4",
                "#f2f2f2");
            groupColorDeselect.set("2444413c-14c0-4b6a-87be-e7520967ed92",
                "#f2f2f2");
            var tiles = "";
            event.forEach(function (x) {
                if (x.EventList !== null) {
                    if (!groupColorCodes.has(x.GroupID)) {
                        groupColorCodes.set(x.GroupID, groupColorSelect.get(x
                            .GroupID));
                        tiles +=
                            `<div id="${x.GroupID}" clicked="false" onclick=divClick("${x.GroupID}") style="cursor:pointer; padding:1em; margin-bottom:5px; background-color:${groupColorCodes.get(x.GroupID)};">${x.GroupName}</div>`;
                        groupIds.push(x.GroupID);
                    }
                    CalendarEvents(x);
                }
            });
            legends.innerHTML = tiles;
            calendar.render();
            document.getElementById("loading").style
                .visibility = "hidden";
            document.getElementById("slide").style.visibility =
            "visible";
            document.getElementById("desc").style.visibility =
                "visible";
            document.getElementById("ulist").style.display =
                "inline-block";
        },
        error: function (jqXHR, textStatus, errorMessage) {
            console.log(textStatus);
        }
    });
};
getUserData();
getData();

//Adding events for specific groups to calendar
function CalendarEvents(x) {
    x.EventList.forEach(function (list_event) {
        var startdate = new Date(list_event.start.dateTime);
        var enddate = new Date(list_event.end.dateTime);
        var reqAttendeesList = [];
        var optAttendeesList = [];
        list_event.attendees.forEach(function (attendee) {
            if (attendee.type === "required") {
                reqAttendeesList.push(attendee.emailAddress.name);
            } else {
                optAttendeesList.push(attendee.emailAddress.name);
            }
        });
        if (list_event.recurrence === null) {
            startdate.setHours(startdate.getHours() + 5);
            startdate.setMinutes(startdate.getMinutes() + 30);
            enddate.setHours(enddate.getHours() + 5);
            enddate.setMinutes(enddate.getMinutes() + 30);
            eventsArray.push({
                "groupId": x.GroupID,
                "title": list_event.subject,
                "start": startdate,
                "end": enddate,
                "allDay": list_event.isAllDay,
                "backgroundColor": groupColorCodes.get(x.GroupID),
                "borderColor": groupColorCodes.get(x.GroupID),
                "extraParams": {
                    "Req_attendees": reqAttendeesList,
                    "Op_attendees": optAttendeesList,
                    "Organiser": list_event.organizer
                        .emailAddress.name,
                    "grp": x.GroupName
                }
            });
            calendar.addEvent({
                "groupId": x.GroupID,
                "title": list_event.subject,
                "start": startdate,
                "end": enddate,
                "allDay": list_event.isAllDay,
                "backgroundColor": groupColorCodes.get(x.GroupID),
                "borderColor": groupColorCodes.get(x.GroupID),
                "extraParams": {
                    "Req_attendees": reqAttendeesList,
                    "Op_attendees": optAttendeesList,
                    "Organiser": list_event.organizer
                        .emailAddress.name,
                    "grp": x.GroupName
                }
            });
        } else {
            startdate.setHours(startdate.getHours() + 11);
            enddate.setHours(enddate.getHours() + 11);
            if (list_event.recurrence.pattern.type === "daily") {
                if (list_event.recurrence.range.type === "endDate") {
                    endDate = new Date(list_event.recurrence.range
                        .endDate);
                    endDate.setDate(endDate.getDate() + 1);
                    eventsArray.push({
                        "groupId": x.GroupID,
                        "title": list_event.subject,
                        "rrule": {
                            "dtstart": startdate,
                            "freq": "DAILY",
                            "interval": list_event.recurrence
                                .pattern.interval,
                            "until": endDate
                        },
                        "duration": msToHMS(enddate.getTime() -
                            startdate.getTime()),
                        "allDay": list_event.isAllDay,
                        "backgroundColor": groupColorCodes.get(x.GroupID),
                        "borderColor": groupColorCodes.get(x.GroupID),
                        "extraParams": {
                            "Req_attendees": reqAttendeesList,
                            "Op_attendees": optAttendeesList,
                            "Organiser": list_event.organizer
                                .emailAddress.name,
                            "grp": x.GroupName
                        }
                    });
                    calendar.addEvent({
                        "groupId": x.GroupID,
                        "title": list_event.subject,
                        "rrule": {
                            "dtstart": startdate,
                            "freq": "DAILY",
                            "interval": list_event.recurrence
                                .pattern.interval,
                            "until": endDate
                        },
                        "duration": msToHMS(enddate.getTime() -
                            startdate.getTime()),
                        "allDay": list_event.isAllDay,
                        "backgroundColor": groupColorCodes.get(x.GroupID),
                        "borderColor": groupColorCodes.get(x.GroupID),
                        "extraParams": {
                            "Req_attendees": reqAttendeesList,
                            "Op_attendees": optAttendeesList,
                            "Organiser": list_event.organizer
                                .emailAddress.name,
                            "grp": x.GroupName
                        }
                    });
                } else if (list_event.recurrence.range.type ===
                    "noEnd") {
                    eventsArray.push({
                        "groupId": x.GroupID,
                        "title": list_event.subject,
                        "rrule": {
                            "dtstart": startdate,
                            "freq": "DAILY",
                            "interval": list_event.recurrence
                                .pattern.interval,
                            "until": "2099-12-31"
                        },
                        "duration": msToHMS(enddate.getTime() -
                            startdate.getTime()),
                        "allDay": list_event.isAllDay,
                        "backgroundColor": groupColorCodes.get(x.GroupID),
                        "borderColor": groupColorCodes.get(x.GroupID),
                        "extraParams": {
                            "Req_attendees": reqAttendeesList,
                            "Op_attendees": optAttendeesList,
                            "Organiser": list_event.organizer
                                .emailAddress.name,
                            "grp": x.GroupName
                        }
                    });
                    calendar.addEvent({
                        "groupId": x.GroupID,
                        "title": list_event.subject,
                        "rrule": {
                            "dtstart": startdate,
                            "freq": "DAILY",
                            "interval": list_event.recurrence
                                .pattern.interval,
                            "until": "2099-12-31"
                        },
                        "duration": msToHMS(enddate.getTime() -
                            startdate.getTime()),
                        "allDay": list_event.isAllDay,
                        "backgroundColor": groupColorCodes.get(x.GroupID),
                        "borderColor": groupColorCodes.get(x.GroupID),
                        "extraParams": {
                            "Req_attendees": reqAttendeesList,
                            "Op_attendees": optAttendeesList,
                            "Organiser": list_event.organizer
                                .emailAddress.name,
                            "grp": x.GroupName
                        }
                    });
                } else {
                    eventsArray.push({
                        "groupId": x.GroupID,
                        "title": list_event.subject,
                        "rrule": {
                            "dtstart": startdate,
                            "freq": "DAILY",
                            "interval": list_event.recurrence
                                .pattern.interval,
                            "count": list_event.recurrence.range
                                .numberOfOccurrences
                        },
                        "duration": msToHMS(enddate.getTime() -
                            startdate.getTime()),
                        "allDay": list_event.isAllDay,
                        "backgroundColor": groupColorCodes.get(x.GroupID),
                        "borderColor": groupColorCodes.get(x.GroupID),
                        "extraParams": {
                            "Req_attendees": reqAttendeesList,
                            "Op_attendees": optAttendeesList,
                            "Organiser": list_event.organizer
                                .emailAddress.name,
                            "grp": x.GroupName
                        }
                    });
                    calendar.addEvent({
                        "groupId": x.GroupID,
                        "title": list_event.subject,
                        "rrule": {
                            "dtstart": startdate,
                            "freq": "DAILY",
                            "interval": list_event.recurrence
                                .pattern.interval,
                            "count": list_event.recurrence.range
                                .numberOfOccurrences
                        },
                        "duration": msToHMS(enddate.getTime() -
                            startdate.getTime()),
                        "allDay": list_event.isAllDay,
                        "backgroundColor": groupColorCodes.get(x.GroupID),
                        "borderColor": groupColorCodes.get(x.GroupID),
                        "extraParams": {
                            "Req_attendees": reqAttendeesList,
                            "Op_attendees": optAttendeesList,
                            "Organiser": list_event.organizer
                                .emailAddress.name,
                            "grp": x.GroupName
                        }
                    });
                }
            } else if (list_event.recurrence.pattern.type ===
                "weekly") {
                var weekNum = [];
                list_event.recurrence.pattern.daysOfWeek.forEach(
                    function (y) {
                        weekNum.push(rruleWeekDays[y]);
                    });
                if (list_event.recurrence.range.type === "endDate") {
                    endDate = new Date(list_event.recurrence.range
                        .endDate);
                    endDate.setDate(endDate.getDate() + 1);
                    eventsArray.push({
                        "groupId": x.GroupID,
                        "title": list_event.subject,
                        "rrule": {
                            "dtstart": startdate,
                            "freq": "WEEKLY",
                            "interval": list_event.recurrence
                                .pattern.interval,
                            "until": endDate,
                            "byweekday": weekNum,
                            "wkst": list_event.recurrence
                                .pattern.firstDayOfWeek
                        },
                        "duration": msToHMS(enddate.getTime() -
                            startdate.getTime()),
                        "allDay": list_event.isAllDay,
                        "backgroundColor": groupColorCodes.get(x.GroupID),
                        "borderColor": groupColorCodes.get(x.GroupID),
                        "extraParams": {
                            "Req_attendees": reqAttendeesList,
                            "Op_attendees": optAttendeesList,
                            "Organiser": list_event.organizer
                                .emailAddress.name,
                            "grp": x.GroupName
                        }
                    });
                    calendar.addEvent({
                        "groupId": x.GroupID,
                        "title": list_event.subject,
                        "rrule": {
                            "dtstart": startdate,
                            "freq": "WEEKLY",
                            "interval": list_event.recurrence
                                .pattern.interval,
                            "until": endDate,
                            "byweekday": weekNum,
                            "wkst": list_event.recurrence
                                .pattern.firstDayOfWeek
                        },
                        "duration": msToHMS(enddate.getTime() -
                            startdate.getTime()),
                        "allDay": list_event.isAllDay,
                        "backgroundColor": groupColorCodes.get(x.GroupID),
                        "borderColor": groupColorCodes.get(x.GroupID),
                        "extraParams": {
                            "Req_attendees": reqAttendeesList,
                            "Op_attendees": optAttendeesList,
                            "Organiser": list_event.organizer
                                .emailAddress.name,
                            "grp": x.GroupName
                        }
                    });
                } else if (list_event.recurrence.range.type ===
                    "noEnd") {
                    eventsArray.push({
                        "groupId": x.GroupID,
                        "title": list_event.subject,
                        "rrule": {
                            "dtstart": startdate,
                            "freq": "WEEKLY",
                            "interval": list_event.recurrence
                                .pattern.interval,
                            "until": "2099-12-31",
                            "byweekday": weekNum,
                            "wkst": list_event.recurrence
                                .pattern.firstDayOfWeek
                        },
                        "duration": msToHMS(enddate.getTime() -
                            startdate.getTime()),
                        "allDay": list_event.isAllDay,
                        "backgroundColor": groupColorCodes.get(x.GroupID),
                        "borderColor": groupColorCodes.get(x.GroupID),
                        "extraParams": {
                            "Req_attendees": reqAttendeesList,
                            "Op_attendees": optAttendeesList,
                            "Organiser": list_event.organizer
                                .emailAddress.name,
                            "grp": x.GroupName
                        }
                    });
                    calendar.addEvent({
                        "groupId": x.GroupID,
                        "title": list_event.subject,
                        "rrule": {
                            "dtstart": startdate,
                            "freq": "WEEKLY",
                            "interval": list_event.recurrence
                                .pattern.interval,
                            "until": "2099-12-31",
                            "byweekday": weekNum,
                            "wkst": list_event.recurrence
                                .pattern.firstDayOfWeek
                        },
                        "duration": msToHMS(enddate.getTime() -
                            startdate.getTime()),
                        "allDay": list_event.isAllDay,
                        "backgroundColor": groupColorCodes.get(x.GroupID),
                        "borderColor": groupColorCodes.get(x.GroupID),
                        "extraParams": {
                            "Req_attendees": reqAttendeesList,
                            "Op_attendees": optAttendeesList,
                            "Organiser": list_event.organizer
                                .emailAddress.name,
                            "grp": x.GroupName
                        }
                    });
                } else {
                    eventsArray.push({
                        "groupId": x.GroupID,
                        "title": list_event.subject,
                        "rrule": {
                            "dtstart": startdate,
                            "freq": "WEEKLY",
                            "interval": list_event.recurrence
                                .pattern.interval,
                            "byweekday": weekNum,
                            "wkst": list_event.recurrence
                                .pattern.firstDayOfWeek,
                            "count": list_event.recurrence.range
                                .numberOfOccurrences
                        },
                        "duration": msToHMS(enddate.getTime() -
                            startdate.getTime()),
                        "allDay": list_event.isAllDay,
                        "backgroundColor": groupColorCodes.get(x.GroupID),
                        "borderColor": groupColorCodes.get(x.GroupID),
                        "extraParams": {
                            "Req_attendees": reqAttendeesList,
                            "Op_attendees": optAttendeesList,
                            "Organiser": list_event.organizer
                                .emailAddress.name,
                            "grp": x.GroupName
                        }
                    });
                    calendar.addEvent({
                        "groupId": x.GroupID,
                        "title": list_event.subject,
                        "rrule": {
                            "dtstart": startdate,
                            "freq": "WEEKLY",
                            "interval": list_event.recurrence
                                .pattern.interval,
                            "byweekday": weekNum,
                            "wkst": list_event.recurrence
                                .pattern.firstDayOfWeek,
                            "count": list_event.recurrence.range
                                .numberOfOccurrences
                        },
                        "duration": msToHMS(enddate.getTime() -
                            startdate.getTime()),
                        "allDay": list_event.isAllDay,
                        "backgroundColor": groupColorCodes.get(x.GroupID),
                        "borderColor": groupColorCodes.get(x.GroupID),
                        "extraParams": {
                            "Req_attendees": reqAttendeesList,
                            "Op_attendees": optAttendeesList,
                            "Organiser": list_event.organizer
                                .emailAddress.name,
                            "grp": x.GroupName
                        }
                    });
                }
            } else if (list_event.recurrence.pattern.type ===
                "absoluteMonthly") {
                if (list_event.recurrence.range.type === "endDate") {
                    endDate = new Date(list_event.recurrence.range
                        .endDate);
                    endDate.setDate(endDate.getDate() + 1);
                    eventsArray.push({
                        "groupId": x.GroupID,
                        "title": list_event.subject,
                        "rrule": {
                            "dtstart": startdate,
                            "freq": "MONTHLY",
                            "interval": list_event.recurrence
                                .pattern.interval,
                            "until": endDate,
                            "bymonthday": list_event.recurrence
                                .pattern.dayOfMonth
                        },
                        "duration": msToHMS(enddate.getTime() -
                            startdate.getTime()),
                        "allDay": list_event.isAllDay,
                        "backgroundColor": groupColorCodes.get(x.GroupID),
                        "borderColor": groupColorCodes.get(x.GroupID),
                        "extraParams": {
                            "Req_attendees": reqAttendeesList,
                            "Op_attendees": optAttendeesList,
                            "Organiser": list_event.organizer
                                .emailAddress.name,
                            "grp": x.GroupName
                        }
                    });
                    calendar.addEvent({
                        "groupId": x.GroupID,
                        "title": list_event.subject,
                        "rrule": {
                            "dtstart": startdate,
                            "freq": "MONTHLY",
                            "interval": list_event.recurrence
                                .pattern.interval,
                            "until": endDate,
                            "bymonthday": list_event.recurrence
                                .pattern.dayOfMonth
                        },
                        "duration": msToHMS(enddate.getTime() -
                            startdate.getTime()),
                        "allDay": list_event.isAllDay,
                        "backgroundColor": groupColorCodes.get(x.GroupID),
                        "borderColor": groupColorCodes.get(x.GroupID),
                        "extraParams": {
                            "Req_attendees": reqAttendeesList,
                            "Op_attendees": optAttendeesList,
                            "Organiser": list_event.organizer
                                .emailAddress.name,
                            "grp": x.GroupName
                        }
                    });
                } else if (list_event.recurrence.range.type ===
                    "noEnd") {
                    eventsArray.push({
                        "groupId": x.GroupID,
                        "title": list_event.subject,
                        "rrule": {
                            "dtstart": startdate,
                            "freq": "MONTHLY",
                            "interval": list_event.recurrence
                                .pattern.interval,
                            "until": "2099-12-31",
                            "bymonthday": list_event.recurrence
                                .pattern.dayOfMonth
                        },
                        "duration": msToHMS(enddate.getTime() -
                            startdate.getTime()),
                        "allDay": list_event.isAllDay,
                        "backgroundColor": groupColorCodes.get(x.GroupID),
                        "borderColor": groupColorCodes.get(x.GroupID),
                        "extraParams": {
                            "Req_attendees": reqAttendeesList,
                            "Op_attendees": optAttendeesList,
                            "Organiser": list_event.organizer
                                .emailAddress.name,
                            "grp": x.GroupName
                        }
                    });
                    calendar.addEvent({
                        "groupId": x.GroupID,
                        "title": list_event.subject,
                        "rrule": {
                            "dtstart": startdate,
                            "freq": "MONTHLY",
                            "interval": list_event.recurrence
                                .pattern.interval,
                            "until": "2099-12-31",
                            "bymonthday": list_event.recurrence
                                .pattern.dayOfMonth
                        },
                        "duration": msToHMS(enddate.getTime() -
                            startdate.getTime()),
                        "allDay": list_event.isAllDay,
                        "backgroundColor": groupColorCodes.get(x.GroupID),
                        "borderColor": groupColorCodes.get(x.GroupID),
                        "extraParams": {
                            "Req_attendees": reqAttendeesList,
                            "Op_attendees": optAttendeesList,
                            "Organiser": list_event.organizer
                                .emailAddress.name,
                            "grp": x.GroupName
                        }
                    });
                } else {
                    eventsArray.push({
                        "groupId": x.GroupID,
                        "title": list_event.subject,
                        "rrule": {
                            "dtstart": startdate,
                            "freq": "MONTHLY",
                            "interval": list_event.recurrence
                                .pattern.interval,
                            "bymonthday": list_event.recurrence
                                .pattern.dayOfMonth,
                            "count": list_event.recurrence.range
                                .numberOfOccurrences
                        },
                        "duration": msToHMS(enddate.getTime() -
                            startdate.getTime()),
                        "allDay": list_event.isAllDay,
                        "backgroundColor": groupColorCodes.get(x.GroupID),
                        "borderColor": groupColorCodes.get(x.GroupID),
                        "extraParams": {
                            "Req_attendees": reqAttendeesList,
                            "Op_attendees": optAttendeesList,
                            "Organiser": list_event.organizer
                                .emailAddress.name,
                            "grp": x.GroupName
                        }
                    });
                    calendar.addEvent({
                        "groupId": x.GroupID,
                        "title": list_event.subject,
                        "rrule": {
                            "dtstart": startdate,
                            "freq": "MONTHLY",
                            "interval": list_event.recurrence
                                .pattern.interval,
                            "bymonthday": list_event.recurrence
                                .pattern.dayOfMonth,
                            "count": list_event.recurrence.range
                                .numberOfOccurrences
                        },
                        "duration": msToHMS(enddate.getTime() -
                            startdate.getTime()),
                        "allDay": list_event.isAllDay,
                        "backgroundColor": groupColorCodes.get(x.GroupID),
                        "borderColor": groupColorCodes.get(x.GroupID),
                        "extraParams": {
                            "Req_attendees": reqAttendeesList,
                            "Op_attendees": optAttendeesList,
                            "Organiser": list_event.organizer
                                .emailAddress.name,
                            "grp": x.GroupName
                        }
                    });
                }
            } else if (list_event.recurrence.pattern.type ===
                "relativeMonthly") {
                var weekDay = [];
                list_event.recurrence.pattern.daysOfWeek.forEach(
                    function (p) {
                        weekDay.push(rruleWeekDays[p]);
                    });
                endDate = new Date(list_event.recurrence.range
                    .endDate);
                endDate.setDate(endDate.getDate() + 1);
                if (list_event.recurrence.range.type === "endDate") {
                    eventsArray.push({
                        "groupId": x.GroupID,
                        "title": list_event.subject,
                        "rrule": {
                            "dtstart": startdate,
                            "freq": "MONTHLY",
                            "interval": list_event.recurrence
                                .pattern.interval,
                            "until": endDate,
                            "byweekday": weekDay,
                            "bysetpos": weekOfMonth[list_event
                                .recurrence.pattern.index]
                        },
                        "duration": msToHMS(enddate.getTime() -
                            startdate.getTime()),
                        "allDay": list_event.isAllDay,
                        "backgroundColor": groupColorCodes.get(x.GroupID),
                        "borderColor": groupColorCodes.get(x.GroupID),
                        "extraParams": {
                            "Req_attendees": reqAttendeesList,
                            "Op_attendees": optAttendeesList,
                            "Organiser": list_event.organizer
                                .emailAddress.name,
                            "grp": x.GroupName
                        }
                    });
                    calendar.addEvent({
                        "groupId": x.GroupID,
                        "title": list_event.subject,
                        "rrule": {
                            "dtstart": startdate,
                            "freq": "MONTHLY",
                            "interval": list_event.recurrence
                                .pattern.interval,
                            "until": endDate,
                            "byweekday": weekDay,
                            "bysetpos": weekOfMonth[list_event
                                .recurrence.pattern.index]
                        },
                        "duration": msToHMS(enddate.getTime() -
                            startdate.getTime()),
                        "allDay": list_event.isAllDay,
                        "backgroundColor": groupColorCodes.get(x.GroupID),
                        "borderColor": groupColorCodes.get(x.GroupID),
                        "extraParams": {
                            "Req_attendees": reqAttendeesList,
                            "Op_attendees": optAttendeesList,
                            "Organiser": list_event.organizer
                                .emailAddress.name,
                            "grp": x.GroupName
                        }
                    });
                } else if (list_event.recurrence.range.type ===
                    "noEnd") {
                    eventsArray.push({
                        "groupId": x.GroupID,
                        "title": list_event.subject,
                        "rrule": {
                            "dtstart": startdate,
                            "freq": "MONTHLY",
                            "interval": list_event.recurrence
                                .pattern.interval,
                            "until": "2099-12-31",
                            "byweekday": weekDay,
                            "bysetpos": weekOfMonth[list_event
                                .recurrence.pattern.index]
                        },
                        "duration": msToHMS(enddate.getTime() -
                            startdate.getTime()),
                        "allDay": list_event.isAllDay,
                        "backgroundColor": groupColorCodes.get(x.GroupID),
                        "borderColor": groupColorCodes.get(x.GroupID),
                        "extraParams": {
                            "Req_attendees": reqAttendeesList,
                            "Op_attendees": optAttendeesList,
                            "Organiser": list_event.organizer
                                .emailAddress.name,
                            "grp": x.GroupName
                        }
                    });
                    calendar.addEvent({
                        "groupId": x.GroupID,
                        "title": list_event.subject,
                        "rrule": {
                            "dtstart": startdate,
                            "freq": "MONTHLY",
                            "interval": list_event.recurrence
                                .pattern.interval,
                            "until": "2099-12-31",
                            "byweekday": weekDay,
                            "bysetpos": weekOfMonth[list_event
                                .recurrence.pattern.index]
                        },
                        "duration": msToHMS(enddate.getTime() -
                            startdate.getTime()),
                        "allDay": list_event.isAllDay,
                        "backgroundColor": groupColorCodes.get(x.GroupID),
                        "borderColor": groupColorCodes.get(x.GroupID),
                        "extraParams": {
                            "Req_attendees": reqAttendeesList,
                            "Op_attendees": optAttendeesList,
                            "Organiser": list_event.organizer
                                .emailAddress.name,
                            "grp": x.GroupName
                        }
                    });
                } else {
                    eventsArray.push({
                        "groupId": x.GroupID,
                        "title": list_event.subject,
                        "rrule": {
                            "dtstart": startdate,
                            "freq": "MONTHLY",
                            "interval": list_event.recurrence
                                .pattern.interval,
                            "count": list_event.recurrence.range
                                .numberOfOccurrences,
                            "byweekday": weekDay,
                            "bysetpos": weekOfMonth[list_event
                                .recurrence.pattern.index]
                        },
                        "duration": msToHMS(enddate.getTime() -
                            startdate.getTime()),
                        "allDay": list_event.isAllDay,
                        "backgroundColor": groupColorCodes.get(x.GroupID),
                        "borderColor": groupColorCodes.get(x.GroupID),
                        "extraParams": {
                            "Req_attendees": reqAttendeesList,
                            "Op_attendees": optAttendeesList,
                            "Organiser": list_event.organizer
                                .emailAddress.name,
                            "grp": x.GroupName
                        }
                    });
                    calendar.addEvent({
                        "groupId": x.GroupID,
                        "title": list_event.subject,
                        "rrule": {
                            "dtstart": startdate,
                            "freq": "MONTHLY",
                            "interval": list_event.recurrence
                                .pattern.interval,
                            "count": list_event.recurrence.range
                                .numberOfOccurrences,
                            "byweekday": weekDay,
                            "bysetpos": weekOfMonth[list_event
                                .recurrence.pattern.index]
                        },
                        "duration": msToHMS(enddate.getTime() -
                            startdate.getTime()),
                        "allDay": list_event.isAllDay,
                        "backgroundColor": groupColorCodes.get(x.GroupID),
                        "borderColor": groupColorCodes.get(x.GroupID),
                        "extraParams": {
                            "Req_attendees": reqAttendeesList,
                            "Op_attendees": optAttendeesList,
                            "Organiser": list_event.organizer
                                .emailAddress.name,
                            "grp": x.GroupName
                        }
                    });
                }
            }
        }
    });
}

//To generate random colors for groups
function getRandomColor() {
    var letters = "0123456789ABCDEF";
    var color = "#";
    var i = 0;
    while (i < 6) {
        color += letters[Math.floor(Math.random() * 16)];
        i = i + 1;
    }
    while (true) {
        if (color != "#408E45" && color != "#192F54") {
            return color;
        }
    }
    return color;
}

//To get events of selected user
var countChecked = function () {
    var ID = $("input[name=Users]:checked").val();
    document.getElementById("loading").style.visibility = "visible";
    document.getElementById("desc1").innerHTML = "";
    select = 1;
    $.ajax({
        url: "/Calendar/GetUserSpecificData/" + ID,
        method: "get",
        dataType: "json",
        success: function (data) {
            eventsArray = [];
            event = data;
            clickedIds = [];
            calendar.removeAllEvents();
            groupIds = [];
            groupColorCodes.clear();
            groupColorDeselect.clear();
            var tiles = "";
            event.forEach(function (group) {
                if (group.EventList !== null) {
                    if (!groupColorCodes.has(group.GroupID)) {
                        if (!groupColorSelect.has(group.GroupID)) {
                            groupColorSelect.set(group.GroupID,
                                getRandomColor());
                        }
                        groupColorCodes.set(group.GroupID, groupColorSelect.get(group
                            .GroupID));
                        groupColorDeselect.set(group.GroupID, "#f2f2f2");
                        tiles +=
                            `<div id="${group.GroupID}" clicked="false" onclick=divClick("${group.GroupID}") style="cursor:pointer; padding:1em; margin-bottom:5px; background-color:${groupColorCodes.get(group.GroupID)};">${group.GroupName}</div>`;
                        groupIds.push(group.GroupID);
                    }
                    CalendarEvents(group);
                }
            });
            legends.innerHTML = tiles;
            calendar.render();
            document.getElementById("loading").style
                .visibility = "hidden";
        },
        error: function (jqXHR, textStatus, errorMessage) {
            console.log(textStatus);
        }
    });
};

//filter events every time group is selected or deselcted from the groups list
function divClick(id) {
    document.getElementById("loading").style.visibility = "visible";
    document.getElementById("desc1").innerHTML = "";
    var groupClicked = document.getElementById(id);
    var selectedGroup;
    var index;
    if (groupClicked.getAttribute("clicked") == "false") {
        document.getElementById("loading").style.visibility = "visible";
        if (clickedIds.length == 0) {
            calendar.removeAllEvents();
        }
        clickedIds.push(id);
        groupEvents = event.filter((x) => x.GroupID == id);
        groupEvents.forEach(function (x) {
            if (x.EventList !== null) {
                clickedGroupCalendar(x);
            }
        });
        calendar.render();
        if (select) {
            select = 0;
            groupIds.forEach(function (tile) {
                groupClicked = document.getElementById(tile);
                groupClicked.setAttribute("clicked", "false");
                groupClicked.style.backgroundColor = groupColorDeselect.get(tile);
            });
            selectedGroup = document.getElementById(id);
            selectedGroup.style.backgroundColor = groupColorSelect.get(id);
            selectedGroup.setAttribute("clicked", "true");
        } else {
            selectedGroup = document.getElementById(id);
            selectedGroup.style.backgroundColor = groupColorSelect.get(id);
            selectedGroup.setAttribute("clicked", "true");
            if (clickedIds.length == groupIds.length) {
                select = 1;
                groupIds.forEach(function (tile) {
                    groupClicked = document.getElementById(tile);
                    groupClicked.setAttribute("clicked", "false");
                });
                clickedIds = [];
            }
        }
    } else {
        calendar.removeAllEvents();
        index = clickedIds.indexOf(id);
        if (index > -1) {
            clickedIds.splice(index, 1);
            selectedGroup = document.getElementById(id);
            selectedGroup.style.backgroundColor = groupColorDeselect.get(id);
            selectedGroup.setAttribute("clicked", "false");
        }
        if (clickedIds.length == 0) {
            calendar.addEventSource(eventsArray);
            select = 1;
            groupIds.forEach(function (tile) {
                groupClicked = document.getElementById(tile);
                groupClicked.setAttribute("clicked", "false");
                groupClicked.style.backgroundColor = groupColorSelect.get(tile);
            });
        } else {
            clickedIds.forEach(function (ids) {
                groupEvents = event.filter((x) => x.GroupID == ids);
                groupEvents.forEach(function (x) {
                    if (x.EventList !== null) {
                        clickedGroupCalendar(x);
                    }
                });
            });
            selectedGroup = document.getElementById(id);
            selectedGroup.style.backgroundColor = groupColorDeselect.get(id);
            selectedGroup.setAttribute("clicked", "false");
        }
        calendar.render();
    }
    document.getElementById("loading").style.visibility = "hidden";
}


//To Retrieve events of Selected Groups
function clickedGroupCalendar(groupDetails) {
    groupDetails.EventList.forEach(function (list_event) {
        var startdate = new Date(list_event.start.dateTime);
        var enddate = new Date(list_event.end.dateTime);
        var reqAttendeesList = [];
        var optAttendeesList = [];
        list_event.attendees.forEach(function (attendee) {
            if (attendee.type === "required") {
                reqAttendeesList.push(attendee.emailAddress.name);
            } else {
                optAttendeesList.push(attendee.emailAddress.name);
            }
        });
        if (list_event.recurrence === null) {
            startdate.setHours(startdate.getHours() + 5);
            startdate.setMinutes(startdate.getMinutes() + 30);
            enddate.setHours(enddate.getHours() + 5);
            enddate.setMinutes(enddate.getMinutes() + 30);
            calendar.addEvent({
                "groupId": groupDetails.GroupID,
                "title": list_event.subject,
                "start": startdate,
                "end": enddate,
                "allDay": list_event.isAllDay,
                "backgroundColor": groupColorCodes.get(groupDetails.GroupID),
                "borderColor": groupColorCodes.get(groupDetails.GroupID),
                "extraParams": {
                    "Req_attendees": reqAttendeesList,
                    "Op_attendees": optAttendeesList,
                    "Organiser": list_event.organizer
                        .emailAddress.name,
                    "grp": groupDetails.GroupName
                }
            });
        } else {
            startdate.setHours(startdate.getHours() + 11);
            enddate.setHours(enddate.getHours() + 11);
            if (list_event.recurrence.pattern.type === "daily") {
                if (list_event.recurrence.range.type === "endDate") {
                    endDate = new Date(list_event.recurrence.range
                        .endDate);
                    endDate.setDate(endDate.getDate() + 1);
                    calendar.addEvent({
                        "groupId": groupDetails.GroupID,
                        "title": list_event.subject,
                        "rrule": {
                            "dtstart": startdate,
                            "freq": "DAILY",
                            "interval": list_event.recurrence
                                .pattern.interval,
                            "until": endDate
                        },
                        "duration": msToHMS(enddate.getTime() -
                            startdate.getTime()),
                        "allDay": list_event.isAllDay,
                        "backgroundColor": groupColorCodes.get(groupDetails.GroupID),
                        "borderColor": groupColorCodes.get(groupDetails.GroupID),
                        "extraParams": {
                            "Req_attendees": reqAttendeesList,
                            "Op_attendees": optAttendeesList,
                            "Organiser": list_event.organizer
                                .emailAddress.name,
                            "grp": groupDetails.GroupName
                        }
                    });
                } else if (list_event.recurrence.range.type ===
                    "noEnd") {
                    calendar.addEvent({
                        "groupId": groupDetails.GroupID,
                        "title": list_event.subject,
                        "rrule": {
                            "dtstart": startdate,
                            "freq": "DAILY",
                            "interval": list_event.recurrence
                                .pattern.interval,
                            "until": "2099-12-31"
                        },
                        "duration": msToHMS(enddate.getTime() -
                            startdate.getTime()),
                        "allDay": list_event.isAllDay,
                        "backgroundColor": groupColorCodes.get(groupDetails.GroupID),
                        "borderColor": groupColorCodes.get(groupDetails.GroupID),
                        "extraParams": {
                            "Req_attendees": reqAttendeesList,
                            "Op_attendees": optAttendeesList,
                            "Organiser": list_event.organizer
                                .emailAddress.name,
                            "grp": groupDetails.GroupName
                        }
                    });
                } else {
                    calendar.addEvent({
                        "groupId": groupDetails.GroupID,
                        "title": list_event.subject,
                        "rrule": {
                            "dtstart": startdate,
                            "freq": "DAILY",
                            "interval": list_event.recurrence
                                .pattern.interval,
                            "count": list_event.recurrence.range
                                .numberOfOccurrences
                        },
                        "duration": msToHMS(enddate.getTime() -
                            startdate.getTime()),
                        "allDay": list_event.isAllDay,
                        "backgroundColor": groupColorCodes.get(groupDetails.GroupID),
                        "borderColor": groupColorCodes.get(groupDetails.GroupID),
                        "extraParams": {
                            "Req_attendees": reqAttendeesList,
                            "Op_attendees": optAttendeesList,
                            "Organiser": list_event.organizer
                                .emailAddress.name,
                            "grp": groupDetails.GroupName
                        }
                    });
                }
            } else if (list_event.recurrence.pattern.type ===
                "weekly") {
                var weekNum = [];
                list_event.recurrence.pattern.daysOfWeek.forEach(
                    function (y) {
                        weekNum.push(rruleWeekDays[y]);
                    });
                if (list_event.recurrence.range.type === "endDate") {
                    endDate = new Date(list_event.recurrence.range
                        .endDate);
                    endDate.setDate(endDate.getDate() + 1);
                    calendar.addEvent({
                        "groupId": groupDetails.GroupID,
                        "title": list_event.subject,
                        "rrule": {
                            "dtstart": startdate,
                            "freq": "WEEKLY",
                            "interval": list_event.recurrence
                                .pattern.interval,
                            "until": endDate,
                            "byweekday": weekNum,
                            "wkst": list_event.recurrence
                                .pattern.firstDayOfWeek
                        },
                        "duration": msToHMS(enddate.getTime() -
                            startdate.getTime()),
                        "allDay": list_event.isAllDay,
                        "backgroundColor": groupColorCodes.get(groupDetails.GroupID),
                        "borderColor": groupColorCodes.get(groupDetails.GroupID),
                        "extraParams": {
                            "Req_attendees": reqAttendeesList,
                            "Op_attendees": optAttendeesList,
                            "Organiser": list_event.organizer
                                .emailAddress.name,
                            "grp": groupDetails.GroupName
                        }
                    });
                } else if (list_event.recurrence.range.type ===
                    "noEnd") {
                    calendar.addEvent({
                        "groupId": groupDetails.GroupID,
                        "title": list_event.subject,
                        "rrule": {
                            "dtstart": startdate,
                            "freq": "WEEKLY",
                            "interval": list_event.recurrence
                                .pattern.interval,
                            "until": "2099-12-31",
                            "byweekday": weekNum,
                            "wkst": list_event.recurrence
                                .pattern.firstDayOfWeek
                        },
                        "duration": msToHMS(enddate.getTime() -
                            startdate.getTime()),
                        "allDay": list_event.isAllDay,
                        "backgroundColor": groupColorCodes.get(groupDetails.GroupID),
                        "borderColor": groupColorCodes.get(groupDetails.GroupID),
                        "extraParams": {
                            "Req_attendees": reqAttendeesList,
                            "Op_attendees": optAttendeesList,
                            "Organiser": list_event.organizer
                                .emailAddress.name,
                            "grp": groupDetails.GroupName
                        }
                    });
                } else {
                    calendar.addEvent({
                        "groupId": groupDetails.GroupID,
                        "title": list_event.subject,
                        "rrule": {
                            "dtstart": startdate,
                            "freq": "WEEKLY",
                            "interval": list_event.recurrence
                                .pattern.interval,
                            "byweekday": weekNum,
                            "wkst": list_event.recurrence
                                .pattern.firstDayOfWeek,
                            "count": list_event.recurrence.range
                                .numberOfOccurrences
                        },
                        "duration": msToHMS(enddate.getTime() -
                            startdate.getTime()),
                        "allDay": list_event.isAllDay,
                        "backgroundColor": groupColorCodes.get(groupDetails.GroupID),
                        "borderColor": groupColorCodes.get(groupDetails.GroupID),
                        "extraParams": {
                            "Req_attendees": reqAttendeesList,
                            "Op_attendees": optAttendeesList,
                            "Organiser": list_event.organizer
                                .emailAddress.name,
                            "grp": groupDetails.GroupName
                        }
                    });
                }
            } else if (list_event.recurrence.pattern.type ===
                "absoluteMonthly") {
                if (list_event.recurrence.range.type === "endDate") {
                    endDate = new Date(list_event.recurrence.range
                        .endDate);
                    endDate.setDate(endDate.getDate() + 1);
                    calendar.addEvent({
                        "groupId": groupDetails.GroupID,
                        "title": list_event.subject,
                        "rrule": {
                            "dtstart": startdate,
                            "freq": "MONTHLY",
                            "interval": list_event.recurrence
                                .pattern.interval,
                            "until": endDate,
                            "bymonthday": list_event.recurrence
                                .pattern.dayOfMonth
                        },
                        "duration": msToHMS(enddate.getTime() -
                            startdate.getTime()),
                        "allDay": list_event.isAllDay,
                        "backgroundColor": groupColorCodes.get(groupDetails.GroupID),
                        "borderColor": groupColorCodes.get(groupDetails.GroupID),
                        "extraParams": {
                            "Req_attendees": reqAttendeesList,
                            "Op_attendees": optAttendeesList,
                            "Organiser": list_event.organizer
                                .emailAddress.name,
                            "grp": groupDetails.GroupName
                        }
                    });
                } else if (list_event.recurrence.range.type ===
                    "noEnd") {
                    calendar.addEvent({
                        "groupId": groupDetails.GroupID,
                        "title": list_event.subject,
                        "rrule": {
                            "dtstart": startdate,
                            "freq": "MONTHLY",
                            "interval": list_event.recurrence
                                .pattern.interval,
                            "until": "2099-12-31",
                            "bymonthday": list_event.recurrence
                                .pattern.dayOfMonth
                        },
                        "duration": msToHMS(enddate.getTime() -
                            startdate.getTime()),
                        "allDay": list_event.isAllDay,
                        "backgroundColor": groupColorCodes.get(groupDetails.GroupID),
                        "borderColor": groupColorCodes.get(groupDetails.GroupID),
                        "extraParams": {
                            "Req_attendees": reqAttendeesList,
                            "Op_attendees": optAttendeesList,
                            "Organiser": list_event.organizer
                                .emailAddress.name,
                            "grp": groupDetails.GroupName
                        }
                    });
                } else {
                    calendar.addEvent({
                        "groupId": groupDetails.GroupID,
                        "title": list_event.subject,
                        "rrule": {
                            "dtstart": startdate,
                            "freq": "MONTHLY",
                            "interval": list_event.recurrence
                                .pattern.interval,
                            "bymonthday": list_event.recurrence
                                .pattern.dayOfMonth,
                            "count": list_event.recurrence.range
                                .numberOfOccurrences
                        },
                        "duration": msToHMS(enddate.getTime() -
                            startdate.getTime()),
                        "allDay": list_event.isAllDay,
                        "backgroundColor": groupColorCodes.get(groupDetails.GroupID),
                        "borderColor": groupColorCodes.get(groupDetails.GroupID),
                        "extraParams": {
                            "Req_attendees": reqAttendeesList,
                            "Op_attendees": optAttendeesList,
                            "Organiser": list_event.organizer
                                .emailAddress.name,
                            "grp": groupDetails.GroupName
                        }
                    });
                }
            } else if (list_event.recurrence.pattern.type ===
                "relativeMonthly") {
                var weekDay = [];
                list_event.recurrence.pattern.daysOfWeek.forEach(
                    function (p) {
                        weekDay.push(rruleWeekDays[p]);
                    });
                endDate = new Date(list_event.recurrence.range
                    .endDate);
                endDate.setDate(endDate.getDate() + 1);
                if (list_event.recurrence.range.type === "endDate") {
                    calendar.addEvent({
                        "groupId": groupDetails.GroupID,
                        "title": list_event.subject,
                        "rrule": {
                            "dtstart": startdate,
                            "freq": "MONTHLY",
                            "interval": list_event.recurrence
                                .pattern.interval,
                            "until": endDate,
                            "byweekday": weekDay,
                            "bysetpos": weekOfMonth[list_event
                                .recurrence.pattern.index]
                        },
                        "duration": msToHMS(enddate.getTime() -
                            startdate.getTime()),
                        "allDay": list_event.isAllDay,
                        "backgroundColor": groupColorCodes.get(groupDetails.GroupID),
                        "borderColor": groupColorCodes.get(groupDetails.GroupID),
                        "extraParams": {
                            "Req_attendees": reqAttendeesList,
                            "Op_attendees": optAttendeesList,
                            "Organiser": list_event.organizer
                                .emailAddress.name,
                            "grp": groupDetails.GroupName
                        }
                    });
                } else if (list_event.recurrence.range.type ===
                    "noEnd") {
                    calendar.addEvent({
                        "groupId": groupDetails.GroupID,
                        "title": list_event.subject,
                        "rrule": {
                            "dtstart": startdate,
                            "freq": "MONTHLY",
                            "interval": list_event.recurrence
                                .pattern.interval,
                            "until": "2099-12-31",
                            "byweekday": weekDay,
                            "bysetpos": weekOfMonth[list_event
                                .recurrence.pattern.index]
                        },
                        "duration": msToHMS(enddate.getTime() -
                            startdate.getTime()),
                        "allDay": list_event.isAllDay,
                        "backgroundColor": groupColorCodes.get(groupDetails.GroupID),
                        "borderColor": groupColorCodes.get(groupDetails.GroupID),
                        "extraParams": {
                            "Req_attendees": reqAttendeesList,
                            "Op_attendees": optAttendeesList,
                            "Organiser": list_event.organizer
                                .emailAddress.name,
                            "grp": groupDetails.GroupName
                        }
                    });
                } else {
                    calendar.addEvent({
                        "groupId": groupDetails.GroupID,
                        "title": list_event.subject,
                        "rrule": {
                            "dtstart": startdate,
                            "freq": "MONTHLY",
                            "interval": list_event.recurrence
                                .pattern.interval,
                            "count": list_event.recurrence.range
                                .numberOfOccurrences,
                            "byweekday": weekDay,
                            "bysetpos": weekOfMonth[list_event
                                .recurrence.pattern.index]
                        },
                        "duration": msToHMS(enddate.getTime() -
                            startdate.getTime()),
                        "allDay": list_event.isAllDay,
                        "backgroundColor": groupColorCodes.get(groupDetails.GroupID),
                        "borderColor": groupColorCodes.get(groupDetails.GroupID),
                        "extraParams": {
                            "Req_attendees": reqAttendeesList,
                            "Op_attendees": optAttendeesList,
                            "Organiser": list_event.organizer
                                .emailAddress.name,
                            "grp": groupDetails.GroupName
                        }
                    });
                }
            }
        }
    });
}