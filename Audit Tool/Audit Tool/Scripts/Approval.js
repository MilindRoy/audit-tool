﻿$(document).ready(function () {
	getUserName();

	document.getElementById("ApproveAudit").onclick = setComments;
	document.getElementById("RejectAudit").onclick = setComments;
	$('#ApproveAudit').tooltip();
	$('#RejectAudit').tooltip();
});

function getUserName() {

	let sUserNameURL = "/Account/GetUsername";
	//Make Ajax request to get user name
	$.ajax({
		url: sUserNameURL,
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		success: populateUsername,
		error: function () {
			var userLabel = document.getElementById("userLabel");
			userLabel.innerHTML = "Username failure";
		}
	});
}

function populateUsername(username) {
	let userLabel = document.getElementById("userLabel");
	let userIcon = document.getElementById("userLabelDiv");
	userLabel.innerHTML = "Hello, " + username;
	userIcon.setAttribute("data-toggle", "tooltip");
	userIcon.setAttribute("data-placement", "bottom");
	userIcon.setAttribute("title", userLabel.innerHTML);
	$('#userLabelDiv').tooltip();
	userName = username;
}

function setComments() {
	let sSetComments = "/Approval/SetComments";
	let sCommentsValue = document.getElementById("ReviewComments").value;
	$.ajax({
		url: sSetComments,
		method: "post",
		contentType: "application/json; charset=utf-8",
		data: JSON.stringify({
			comments: sCommentsValue
		}), 
		dataType: "json",
		success: logSuccess,
		error: function (a, b, error) { }
	});
}

function logSuccess() {
	//console.log("success");

}