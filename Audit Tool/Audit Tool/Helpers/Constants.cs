﻿#region Summary
// *****************************************************************
// Project:     ExportToPPT
// Solution:    ExportToPPTWebApp
//
// Author:  MAQ Software
// Date:    September 07, 2018
// Description: Class for maintaining all string constants
// Change History:
// Name             Date                Version             Description
// ---------------------------------------------------------------------------------------------------------------------------------
//  Deepak G     September 07, 2018      2.0.0.0            Class for maintaining all string constants
// ---------------------------------------------------------------------------------------------------------------------------------
// Copyright (C) MAQ Software
// ---------------------------------------------------------------------------------------------------------------------------------
#endregion
namespace Audit_Tool.Helpers
{
    using System;
    using System.Configuration;
    using System.Globalization;

    /// <summary>
    /// Class for maintaining all string constants
    /// </summary>
    public static class Constants
    {

        #region Authorization

        /// <summary>
        /// TENANT value for tenant key
        /// </summary>
        internal static string TENANT_VALUE = ConfigurationManager.AppSettings["ida:Tenant"];

        /// <summary>
        /// Audience value for audience key
        /// </summary>
        public static string clientId = ConfigurationManager.AppSettings["ida:ClientId"];
        public static string clientSecret = ConfigurationManager.AppSettings["clientSecret"];
        public static string appkey = ConfigurationManager.AppSettings["ida:Appkey"];
        public static string aadInstance = ConfigurationManager.AppSettings["ida:AADInstance"];
        public static string tenant = ConfigurationManager.AppSettings["ida:Tenant"];
        public static string postLogoutRedirectUri = ConfigurationManager.AppSettings["ida:PostLogoutRedirectUri"];
        public static string claimIdentifier = ConfigurationManager.AppSettings["claimIdentifier"];
        public static string powerBIResourceId = ConfigurationManager.AppSettings["PowerBIResourceId"];
        public static string authority = String.Format(CultureInfo.InvariantCulture, aadInstance, tenant);
        public static string GraphResourceId = ConfigurationManager.AppSettings["graphResourceId"];
        public static string GraphAPIVersion = ConfigurationManager.AppSettings["graphAPIVersion"];
        public static string GraphJsonValueNode = ConfigurationManager.AppSettings["graphJsonValueNode"];
        public static string GraphJsonMailNode = ConfigurationManager.AppSettings["graphJsonMailNode"];
        public static string GraphAPIDataTypeKey = ConfigurationManager.AppSettings["graphAPIDataTypeKey"];
        public static string GraphAPIDataTypeValue = ConfigurationManager.AppSettings["graphAPIDataTypeValue"];
        public static string GraphAPIScopes = ConfigurationManager.AppSettings["graphAPIScopes"];
        public static string SecurityGroupEmail = ConfigurationManager.AppSettings["SecurityGroupAlias"];
        public static string SecurityGroupName = ConfigurationManager.AppSettings["SecurityGroupName"];
        public static string SecurityGroupID = ConfigurationManager.AppSettings["SecurityGroupID"];
        public static string SecurityGroupRedisKey = "SecurityGroupObjectID";
        public static string GraphEndpoint = "https://graph.microsoft.com";

        public static string sessionFolder = ConfigurationManager.AppSettings["SessionFolder"];
        public static string sessionFolderParent = ConfigurationManager.AppSettings["SessionFolderParent"];
        public static string appPath = ConfigurationManager.AppSettings["AppPath"];
        public static string errorFilePath = ConfigurationManager.AppSettings["ErrorFilePath"];

        public static string signInHTML = ConfigurationManager.AppSettings["signInHTML"];
        public static string SMTPServerName = ConfigurationManager.AppSettings["SMTPServerName"];
        public static string SMTPPort = ConfigurationManager.AppSettings["SMTPPort"];
        public static string EmailSenderID = ConfigurationManager.AppSettings["EmailSender"];
        public static string EmailSenderPassword = ConfigurationManager.AppSettings["EmailSenderPassword"];
        public static string EmailEnableSSL = ConfigurationManager.AppSettings["EmailEnableSSL"];

        public static string AppURL = ConfigurationManager.AppSettings["AppURL"];

        //HTML element names
        public static string activeReviewerBag = "active-reviewer-bag";
        public static string scoreBag = "score-bag";
        public static string creatorBag = "creator-bag";
        public static string reviewersBag = "reviewers-bag";
        public static string urlBag = "url-bag";
        public static string updatersBag = "updaters-bag";
        public static string comments = "comments";
        public static string p0Observation = "p0Observation";
        public static string p1Observation = "p1Observation";

        /// <summary>
        /// Constants other than web config.
        /// </summary>
        public static char Comma = ',';
        public static string checklistFile = "~/Files/checklistForAuditType.json";
        #endregion

        public static string ProjectDetails = "Select ProjectID, PMOwner, ProjectName from [VSOHygiene].[ProjectDetail] ORDER BY ProjectName for Json auto";
        public static string ProjectManager = "SELECT DISTINCT PMOwner from [VSOHygiene].[ProjectDetail] WHERE PMOwner<>'NULL' ORDER BY PMOwner for Json auto";
        public static string ProjectReports = "SELECT DISTINCT ReportName FROM dbo.ProdPages where PMOID = {0} for Json auto";
        public static string ProjectCount = "SELECT STR(count(ProjectID)) FROM [VSOHygiene].[ProjectDetail] WHERE PMOwner = '{0}' AND ProjectID = '{1}'";
        public static string ProjectBugs = "SELECT P0BugId,P0BugLink,P1BugId,P1BugLink FROM [ProjectAudit].[AuditTable] AS AT INNER JOIN VSOHygiene.ProjectDetail AS PD ON AT.[ProjectID] = PD.[ProjectID] and AT.PMId=PD.PMOwner WHERE AT.[PMId] = '{0}' AND PD.[ProjectName] = '{1}' AND AT.[AuditType] = '{2}' AND AT.[DateTime] = '{3}' FOR JSON AUTO";
        public static string AuditReviewers = "SELECT Reviewers FROM [ProjectAudit].[AuditTable] AS AT INNER JOIN VSOHygiene.ProjectDetail AS PD ON AT.[ProjectID] = PD.[ProjectID] and AT.PMId=PD.PMOwner WHERE AT.[PMId] = '{0}' AND PD.[ProjectName] = '{1}' AND AT.[AuditType] = '{2}' AND AT.[DateTime] = '{3}'";
        public static string AuditInfoQuery = "SELECT ActiveReviewer,Reviewers, CreatedBy, AuditScore, ProjectURL as ProjectURL, UpdatedBy, PMComments, P0Observation, P1Observation, AT.ProjectID FROM [ProjectAudit].[AuditTable] AS AT INNER JOIN VSOHygiene.ProjectDetail AS PD ON AT.[ProjectID] = PD.[ProjectID] and AT.PMId=PD.PMOwner WHERE AT.[ID] = '{0}' FOR JSON AUTO";
        public static string AuditInfo = "SELECT ActiveReviewer,Reviewers, CreatedBy, AuditScore, ProjectURL as ProjectURL, UpdatedBy, PMComments, P0Observation, P1Observation, AT.ProjectID FROM [ProjectAudit].[AuditTable] AS AT INNER JOIN VSOHygiene.ProjectDetail AS PD ON AT.[ProjectID] = PD.[ProjectID] and AT.PMId=PD.PMOwner WHERE AT.[PMId] = '{0}' AND PD.[ProjectName] = '{1}' AND AT.[AuditType] = '{2}' AND AT.[mapping] = '{3}' FOR JSON AUTO";
        public static string AuditExtraInfo = "SELECT AT.Frequency, POC, Reason, Comments FROM ProjectAudit.AuditTable AS AT INNER JOIN VSOHygiene.ProjectDetail AS PD ON AT.[ProjectID] = PD.[ProjectID] and AT.PMId=PD.PMOwner WHERE AT.[PMId] = '{0}' AND PD.[ProjectName] = '{1}' AND AT.[AuditType] = '{2}' AND AT.[DateTime] = '{3}' FOR JSON AUTO";
        public static string PrevObservations = "SELECT P0Observation AS p0,P1Observation AS p1 FROM ProjectAudit.Observations WHERE [ProjectID] = {0} AND [PMId] = '{1}' AND DateTime = '{2}' FOR JSON AUTO ";
        public static string CountAutoSaveObservations = "SELECT STR(count(ObservationID)) FROM ProjectAudit.Observations WHERE [ProjectID] = {0} AND [PMId] = '{1}' AND DateTime = '{2}' AND AutoSave = {3}";
        public static string UpdateObservations = "Update ProjectAudit.Observations SET P0Observation = '{0}', P1Observation = '{1}' where [ProjectID] = {2} AND PMId = '{3}' AND DateTime = '{4}' AND AutoSave = {5}";
        public static string ChangeOnFinalSubmission = "UPDATE [ProjectAudit].[Observations] set AutoSave = 0 where ProjectId = {0} and PMId = '{1}' and DateTime = '{2}' and AutoSave = {3};";
        public static string SubmittedOrNot = "SELECT SubmitStatus FROM [ProjectAudit].[AuditTable] AS AT INNER JOIN VSOHygiene.ProjectDetail AS PD ON AT.[ProjectID] = PD.[ProjectID] and AT.PMId=PD.PMOwner WHERE AT.[PMId] = '{0}' AND PD.[ProjectName] = '{1}' AND AT.[AuditType] = '{2}' AND AT.[DateTime] = '{3}' FOR JSON AUTO";
        public static string PrevObservForApproval = "SELECT P0Observation, P1Observation FROM [ProjectAudit].[AuditTable] AS AT INNER JOIN VSOHygiene.ProjectDetail AS PD ON AT.[ProjectID] = PD.[ProjectID] and AT.PMId=PD.PMOwner WHERE AT.[PMId] = '{0}' AND PD.[ProjectName] = '{1}' AND AT.[AuditType] = '{2}' AND AT.[DateTime] = '{3}' FOR JSON AUTO ";
        public static string VSOToken = "SELECT DISTINCT(VH.PassWord), VH.VsoPath, VH.AreaPath FROM [VSOHygiene].[ProjectDetail] AS VH inner join [ProjectAudit].[AuditTable] AS AT ON VH.ProjectId = AT.ProjectId WHERE VH.ProjectName ='{0}' FOR JSON AUTO";
        public static string UpdateP0Bug = "UPDATE AT SET AT.[P0BugId] = '{0}', AT.[P0BugLink] = '{1}' FROM [ProjectAudit].[AuditTable] AS AT INNER JOIN [VSOHygiene].[ProjectDetail] AS PD ON AT.[ProjectID] = PD.[ProjectID] and AT.[PMId]=PD.[PMOwner] WHERE AT.[PMId] = '{2}' AND PD.[ProjectName] = '{3}' AND AT.[AuditType] = '{4}' AND AT.[DateTime] = '{5}'";
        public static string UpdateP1Bug = "UPDATE AT SET AT.[P1BugId] = '{0}', AT.[P1BugLink] = '{1}' FROM [ProjectAudit].[AuditTable] AS AT INNER JOIN [VSOHygiene].[ProjectDetail] AS PD ON AT.[ProjectID] = PD.[ProjectID] and AT.[PMId]=PD.[PMOwner] WHERE AT.[PMId] = '{2}' AND PD.[ProjectName] = '{3}' AND AT.[AuditType] = '{4}' AND AT.[DateTime] = '{5}'";
        public static string InsMulCommentsP0 = "INSERT INTO ProjectAudit.Observations(ProjectID,PMId,DateTime,P0Observation, AutoSave, Area, Severity) values ('{0}','{1}','{2}','{3}', {4}, '{5}', '{6}')";
        public static string InsMulCommentsP1 = "INSERT INTO ProjectAudit.Observations(ProjectID,PMId,DateTime,P1Observation, AutoSave, Area, Severity) values ('{0}','{1}','{2}','{3}', {4}, '{5}', '{6}')";
        public static string UpdMulComments = "UPDATE ProjectAudit.Observations SET P0Observation = '{0}' WHERE ProjectID = {1} AND PMId = '{2}' AND DateTime = '{3}'";
        public static string DelMulComments = "DELETE FROM ProjectAudit.Observations WHERE ProjectID = {0} AND PMId = '{1}' AND DateTime = '{2}'";
        public static string PrevMulComments = "SELECT P0Observation, P1Observation, Area, Severity FROM ProjectAudit.Observations WHERE ProjectID = {0} AND PMId = '{1}' AND DateTime = '{2}' FOR JSON AUTO";
        public static string InsertAuditData = "INSERT INTO ProjectAudit.AuditTable(ProjectID,PMId, AuditType,CheckListJson, P0Observation, P1Observation, CreatedBy, UpdatedBy,IsApproved, reviewers, AuditScore, ProjectURL, SubmitStatus, Frequency, POC, Reason, Comments) values({0},'{1}','{2}','{3}','{4}','{5}','{6}','{7}',{8},'{9}',{10},'{11}',{12}, '{13}', '{14}', '{15}', '{16}')";
        public static string PMNameAndEmail = "SELECT PMName, email FROM ProjectAudit.ProjectManager WHERE PMName='{0}' FOR json auto";
        public static string AllPMEmail = "SELECT email FROM ProjectAudit.ProjectManager FOR json auto";
        public static string ProjectName = "SELECT ProjectName FROM [VSOHygiene].[ProjectDetail] WHERE ProjectID = {0} FOR json auto";
        public static string ProjectDate = "SELECT MAX(DateTime) AS DateTime FROM ProjectAudit.AuditTable WHERE ProjectID = '{0}' AND PMId ='{1}' AND AuditType = '{2}' FOR json auto";
        public static string InsertObservations = "INSERT INTO ProjectAudit.Observations(ProjectID,PMId,DateTime,P0Observation,P1Observation,AutoSave) values ({0},'{1}','{2}','{3}','{4}',{5})";
        public static string PrevChecklist = "SELECT top 1 CheckListJson AS checklist,reviewers,P0Observation AS p0,P1Observation AS p1, IsApproved as approve FROM ProjectAudit.AuditTable WHERE ProjectID={0} AND PMId='{1}' AND AuditType='{2}' AND DateTime='{3}' for json auto";
        public static string UpdateAuditDetails = "Update ProjectAudit.AuditTable set CheckListJson= '{0}', reviewers ='{1}', UpdatedBy = CASE WHEN(CHARINDEX ('{2}',UpdatedBy)= 0) THEN concat(UpdatedBy, ', {3}') ELSE UpdatedBy END, IsApproved=0, AuditScore= {4}, SubmitStatus={5}, ProjectURL='{6}', Frequency='{7}', POC='{8}', Reason='{9}', Comments='{10}' where ProjectID = '{11}' AND PMId ='{12}' AND AuditType = '{13}' AND DateTime = '{14}'";
        public static string PrevDates = "SELECT [DateTime] FROM ProjectAudit.AuditTable WHERE ProjectID={0} AND PMId='{1}' AND AuditType='{2}' ORDER BY [DateTime] desc for json auto";
        public static string AuditStatusYes = "UPDATE ProjectAudit.AuditTable SET isApproved=1, ActiveReviewer='{0}' where ProjectID={1} AND PMId = '{2}' AND AuditType = '{3}' AND DateTime='{4}'";
        public static string AuditStatusNo = "UPDATE ProjectAudit.AuditTable SET isApproved=-1, ActiveReviewer='{0}' where ProjectID={1} AND PMId = '{2}' AND AuditType = '{3}' AND DateTime='{4}'";
        public static string PMAuditApproval = "UPDATE AT SET AT.[HasPMApproved] = '{0}', AT.[PMComments] = '{1}' FROM [ProjectAudit].[AuditTable] AS AT INNER JOIN [VSOHygiene].[ProjectDetail] AS PD ON AT.[PMId] = PD.[PMOwner] AND AT.[ProjectID] = PD.[ProjectID] WHERE AT.[PMId] = '{2}'  AND PD.[ProjectName] = '{3}' AND AT.[AuditType] = '{4}' AND AT.[DateTime] = '{5}'";
        public static string ObservationsHTML =   "<div class='row cancellable-row' id='row{0}'>"
                                                         + "<div class='col-md-7'>"
                                                                + "<div class='form-group'>"
                                                                    + "<input type = 'text' class ='form-control' value='{1}'/>"
                                                                + "</div>"
                                                         + "</div><div class='col-md-2'>"
                                                             + "<div class='form-group'>"
                                                                    + "<select class ='form-control observation-severity'>"
                                                                         + "<option value = '-1'>Select</option>"
                                                                         + "<option value='P0' selected>P0</option>"
                                                                         + "<option value='P1'>P1</option>"
                                                                    + "</select>"
                                                             + "</div>"
                                                         + "</div><div class='col-md-2'>"
                                                             + "<div class='form-group'>"
                                                                 + "<select class ='form-control observation-area'>"
                                                                     + "<option value = '-1' >Select</ option >";
    }
}