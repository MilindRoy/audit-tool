﻿#region Summary
// *****************************************************************
// Project:     ExportToPPT
// Solution:    ExportToPPTWebApp
//
// Author:  MAQ Software
// Date:    September 07, 2018
// Description: Token handler for session
// Change History:
// Name             Date                Version             Description
// ---------------------------------------------------------------------------------------------------------------------------------
//  Deepak G     September 07, 2018      2.0.0.0            Adds token in Current session
// ---------------------------------------------------------------------------------------------------------------------------------
// Copyright (C) MAQ Software
// ---------------------------------------------------------------------------------------------------------------------------------
#endregion

using Microsoft.IdentityModel.Clients.ActiveDirectory;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OpenIdConnect;
using System;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Audit_Tool.Helpers
{
    public class TokenHandler
    {
        /// <summary>
        /// returns access token from authentication context
        /// </summary>
        /// <returns></returns>
        public static async Task<string> AcquireAccessTokenAsync()
        {
            try
            {
                var clientCredential = new ClientCredential(Constants.clientId, Constants.appkey);
                string userObjectID = ClaimsPrincipal.Current.FindFirst(Constants.claimIdentifier).Value;
                AuthenticationResult result;
                AuthenticationContext authenticationContext = new AuthenticationContext(Constants.authority, new NaiveSessionCache(userObjectID));
                try
                {
                    // initialize AuthenticationContext with the token cache of the currently signed in user, as kept in the app's EF DB                
                    result = await authenticationContext.AcquireTokenSilentAsync(Constants.GraphEndpoint, clientCredential, new UserIdentifier(userObjectID, UserIdentifierType.UniqueId));
                    return result.AccessToken;

                }
                catch (AdalSilentTokenAcquisitionException ex)
                {
                    ExceptionLogging.SendErrorToText(ex);
                    return null;
                }
            }
            catch {
                return null;
            }
        }

        /// <summary>
        /// adds access token in current session
        /// </summary>
        /// <param name="owinContext"></param>
        /// <param name="sRedirectURI"></param>
        /// <returns></returns>
        public async Task setAccessToken(IOwinContext owinContext, string sRedirectURI)
        {
            string token = await TokenHandler.AcquireAccessTokenAsync();
            if (String.IsNullOrEmpty(token))
            {
                owinContext.Authentication.Challenge(new AuthenticationProperties { RedirectUri = sRedirectURI }, OpenIdConnectAuthenticationDefaults.AuthenticationType);
            }
            System.Web.HttpContext.Current.Session.Add("AccessToken", token);
        }

    }

}