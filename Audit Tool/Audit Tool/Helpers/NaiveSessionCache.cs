﻿#region Summary
// *****************************************************************
// Project:     ExportToPPT
// Solution:    ExportToPPTWebApp
//
// Author:  MAQ Software
// Date:    September 07, 2018
// Description: Session cache helper
// Change History:
// Name             Date                Version             Description
// ---------------------------------------------------------------------------------------------------------------------------------
//  Deepak G     September 07, 2018      2.0.0.0            Session cache helper
// ---------------------------------------------------------------------------------------------------------------------------------
// Copyright (C) MAQ Software
// ---------------------------------------------------------------------------------------------------------------------------------
#endregion

using Microsoft.IdentityModel.Clients.ActiveDirectory;
using System.Web;

namespace Audit_Tool.Helpers
{
    public class NaiveSessionCache : TokenCache
    {
        private static readonly object FileLock = new object();
        string UserObjectId = string.Empty;
        string CacheId = string.Empty;

        /// <summary>
        /// loads cache
        /// </summary>
        /// <param name="userId"></param>
        public NaiveSessionCache(string userId)
        {
            UserObjectId = userId;
            CacheId = UserObjectId + "_TokenCache";

            this.AfterAccess = AfterAccessNotification;
            this.BeforeAccess = BeforeAccessNotification;
            Load();
        }

        /// <summary>
        /// function used by NaiveSessionCache ro load cache
        /// </summary>
        public void Load()
        {
            lock (FileLock)
            {
                this.Deserialize((byte[])HttpRuntime.Cache[CacheId]);
            }
        }

        /// <summary>
        /// reflect changes in the persistent store
        /// once the write operation took place, restore the HasStateChanged bit to false 
        /// </summary>
        public void Persist()
        {
            lock (FileLock)
            {
                HttpRuntime.Cache[CacheId] = this.Serialize();
                this.HasStateChanged = false;
            }
        }

        /// <summary>
        /// Empties the persistent store.
        /// </summary>
        public override void Clear()
        {
            base.Clear();
            System.Web.HttpContext.Current.Session.Remove(CacheId);
        }

        public override void DeleteItem(TokenCacheItem item)
        {
            base.DeleteItem(item);
            Persist();
        }

        /// <summary>
        /// Reload the cache from the persistent store in case it changed since the last access. 
        /// Triggered right before ADAL needs to access the cache. 
        /// </summary>
        /// <param name="args"></param>
        void BeforeAccessNotification(TokenCacheNotificationArgs args)
        {
            Load();
        }

        /// <summary>
        /// Triggered right after ADAL accessed the cache. 
        /// </summary>
        /// <param name="args"></param>
        void AfterAccessNotification(TokenCacheNotificationArgs args)
        {
            // if the access operation resulted in a cache update 
            if (this.HasStateChanged)
            {
                Persist();
            }
        }
    }
}