﻿using Audit_Tool.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Audit_Tool.Controllers
{
    public interface IEmbedService
    {
        EmbedConfig EmbedConfig { get; }
        bool EmbedReport(string userName, string roles);
    }
}