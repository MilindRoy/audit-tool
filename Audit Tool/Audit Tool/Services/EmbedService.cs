﻿using Audit_Tool.Services;
using Audit_Tool.Models;
using Microsoft.IdentityModel.Clients.ActiveDirectory;

using Microsoft.PowerBI.Api.V2;
using Microsoft.PowerBI.Api.V2.Models;
using Microsoft.Rest;

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Audit_Tool.Controllers;

namespace Audit_Tool.Services
{
    public class EmbedService : IEmbedService
    {
        private static readonly string AuthorityUrl = ConfigurationManager.AppSettings["authorityUrl"];
        private static readonly string ResourceUrl = ConfigurationManager.AppSettings["resourceUrl"];
        private static readonly string ApplicationId = ConfigurationManager.AppSettings["applicationId"];
        private static readonly string ApiUrl = ConfigurationManager.AppSettings["apiUrl"];
        private static readonly string WorkspaceId = ConfigurationManager.AppSettings["workspaceId"];
        private static readonly string ReportId = ConfigurationManager.AppSettings["reportId"];

        private static readonly string AuthenticationType = ConfigurationManager.AppSettings["AuthenticationType"];
        private static readonly NameValueCollection sectionConfig = ConfigurationManager.GetSection(AuthenticationType) as NameValueCollection;
        private static readonly string ApplicationSecret = sectionConfig["applicationSecret"];
        private static readonly string Tenant = sectionConfig["tenant"];
        private static readonly string Username = sectionConfig["pbiUsername"];
        private static readonly string Password = sectionConfig["pbiPassword"];

        public EmbedConfig EmbedConfig
        {
            get { return m_embedConfig; }
        }

        private EmbedConfig m_embedConfig;
        private TokenCredentials m_tokenCredentials;

        public EmbedService()
        {
            m_tokenCredentials = null;
            m_embedConfig = new EmbedConfig();
        }

        public bool EmbedReport(string username, string roles)
        {

            // Get token credentials for user
            var getCredentialsResult = GetTokenCredentials();
            if (!getCredentialsResult)
            {
                // The error message set in GetTokenCredentials
                return false;
            }

            try
            {
                // Create a Power BI Client object. It will be used to call Power BI APIs.
                using (var client = new PowerBIClient(new Uri(ApiUrl), m_tokenCredentials))
                {
                    // Get a list of reports.
                    var reports = client.Reports.GetReportsInGroupAsync(WorkspaceId).Result;

                    // No reports retrieved for the given workspace.
                    if (reports.Value.Count() == 0)
                    {
                        m_embedConfig.ErrorMessage = "No reports were found in the workspace";
                        return false;
                    }

                    Report report;
                    if (string.IsNullOrWhiteSpace(ReportId))
                    {
                        // Get the first report in the workspace.
                        report = reports.Value.FirstOrDefault();
                    }
                    else
                    {
                        report = reports.Value.FirstOrDefault(r => r.Id.Equals(ReportId, StringComparison.InvariantCultureIgnoreCase));
                    }

                    if (report == null)
                    {
                        m_embedConfig.ErrorMessage = "No report with the given ID was found in the workspace. Make sure ReportId is valid.";
                        return false;
                    }

                    var datasets = client.Datasets.GetDatasetByIdInGroupAsync(WorkspaceId, report.DatasetId).Result;

                    m_embedConfig.IsEffectiveIdentityRequired = datasets.IsEffectiveIdentityRequired;
                    m_embedConfig.IsEffectiveIdentityRolesRequired = datasets.IsEffectiveIdentityRolesRequired;
                    GenerateTokenRequest generateTokenRequestParameters;
                    // This is how you create embed token with effective identities

                    // Generate Embed Token for reports without effective identities.
                    generateTokenRequestParameters = new GenerateTokenRequest(accessLevel: "view");
                    //Caching
                    System.Runtime.Caching.MemoryCache memoryCache = System.Runtime.Caching.MemoryCache.Default;
                    DateTimeOffset expiration = DateTimeOffset.UtcNow.AddMinutes(55);
                    //memoryCache.Add(report.Id, m_embedConfig.EmbedToken, expiration);

                    // var tokenResponse = await client.Reports.GenerateTokenInGroupAsync(WorkspaceId, report.Id, generateTokenRequestParameters);
                   

                    List<string> storedReportIdDetails = new List<string>();

                    if (memoryCache.Contains(report.Id))
                    {
                        //m_embedConfig.EmbedToken.Token = null;
                        var listItems = memoryCache.Get(ReportId);

                        storedReportIdDetails = listItems as List<string>;
                        //storedReportIdDetails.Concat(listItems as List<string>);
                        //var stringList = listItems.OfType<string>();
                        if (DateTimeOffset.UtcNow.DateTime.CompareTo(DateTime.Parse(storedReportIdDetails[1], CultureInfo.InvariantCulture)) >= 0)
                        {
                            var tokenResponse = client.Reports.GenerateTokenInGroupAsync(WorkspaceId, report.Id, generateTokenRequestParameters).Result;
                            if (tokenResponse == null)
                            {
                                m_embedConfig.ErrorMessage = "Failed to generate embed token.";
                                return false;
                            }
                            m_embedConfig.EmbedToken = tokenResponse.Token;
                            List<string> accessTokenDetails = new List<string>();
                            accessTokenDetails.Add(tokenResponse.Token);
                            accessTokenDetails.Add(tokenResponse.Expiration.ToString());
                            accessTokenDetails.Add(tokenResponse.TokenId);
                            //string accessToken = m_embedConfig.EmbedToken.Token;
                            memoryCache.Add(report.Id, accessTokenDetails, expiration);
                            //accessToken = GenerateEmbedTokenIfNotInCache(ReportId);
                        }
                        else
                        {
                             //m_embedConfig.EmbedToken.Token = null;
                            m_embedConfig.EmbedToken = storedReportIdDetails[0];
                           // m_embedConfig.EmbedToken.TokenId = storedReportIdDetails[2];
                        }
                    }
                    else

                    {
                        var tokenResponse = client.Reports.GenerateTokenInGroupAsync(WorkspaceId, report.Id, generateTokenRequestParameters).Result;
                        if (tokenResponse == null)
                        {
                            m_embedConfig.ErrorMessage = "Failed to generate embed token.";
                            return false;
                        }
                        m_embedConfig.EmbedToken = tokenResponse.Token;
                        List<string> sample = new List<string>();
                        sample.Add("H4sIAAAAAAAEACWWtQ7sCBJF_-WlHslMI01gZreZMjO1mb3af99eTV4V1NU9qvOfP1b6fKe0-PP3H5L8oJ0fH32W2o7t1DYcCPqCB5fNzTsALxmEk_zFMO46Gf4qQ_RSTzqE4pU0xpS38lRvrl-vkCJfkjqepSY-B_dbx-GRJSJp7JP4nG_7IPIjPHrMZoWFkN5kz8n7yNkqMqJkDTSTwc8BcJoyk6ghsbwPSoIXGmvpVA-tzOGMc1_AStuCLShBBNfAZn9tTbfEnBz8mL1YXZm37aD03Rk9pnIec9OdFfSqnipTBLbODZ-YZxf0IVNg7UYux1sgAj4WAZNkocmvF6-nL2CSVETmXTM-3W6gBwXGUNOF66obwfg2CKDGHr3CdU21srqvfpVAVuwgCjBkKELlRxUE2vAtl7mTZfp5pQlhQ9EKKYSHVID8LaHo2ZYQkg5wgOC0RHWtz1aOu9HgQ7fNSJphwn5kTfSrUCDdc7nVNmwYVWZsMVXMvC5V_WaWmuoQPJb7PMNd9FvmADRI43qZDNOvolwfEhlpByKk8TZKdzALzT2lVKZ9Rdc_NgrvqFsd-CZfoWrXpg9hb3WhnuGLBgm_PLQVYpL3aQOTDz6fE11Bt1GogfeKl8bMirczVf10iGqoKh5BBF4Lqoh23BnwY3iOq4c3zHkXwWuadxmyx_2-zwg2Kh1NeGhWO7qpWPBy57PZUvWIsctv7ZIFWQEfzdl891PBOtuvsdEXadwEls8L8g4-39JksR8SGshAYEo_zpMoJcIeEMiamlmgeA6dw-wAiIs84eBpdAfdTuUeYHP7vTWRWcyhZjag0XJcqdYzCRkgHLrn8cXEm5NETa8Jevmq-WzuFIL1tVH-NNfWjusB_MNDSAy0UEoGRjsTAXfXEcFXrPjzbfjiGzUiHdXYRrl-Pgr9eE1y1AcXLeYJ9LFpR8EzAs4EXtuddGbnoKVLPCIcl_JhRyL894CIk-FfHslEO0nCEyYNF991HVWL2OhqkbYCycMerPVrHaFBCjH7bMi4G-XKWDxJEIgmRyqT4BTLreGfzStn-EvmSyaDqJXOiZAEfIXYY5RF5A_OssAAmb4VCvnQgT9TD2Ya4dnGYByjapklea5oMMEVdl5ubB5lVe2yaB0qkHCZqrkiel0PbqVigs03P_4vFgUN56y2kOaKWNZNBybsLIf8OxuMfQzgxumQZyj7HLm4kBv1M0WsxOf64a1B34qajpnfAqTQKAAJ6463TPbQRFPj5HR19J6ca-p0IFfHE5dz1yRYHGI9BCFjRvp8xUslTq7M8hbtWY4TC89rvrg6eEyYxUD5AdxopZ0aeHk8jOsfvqRya1cuA8p64KSlbzNqJ6TLWQRdfDJs4xUi5OQchBD5cyxBgmED-ACKoFOJdhxvFbFAZEfQgdgIumcfu8QMEZbBof61LdFwom2urNBGCf44YpeRxxTs17cjnMlTnNBKtH3Pe2VQWNh53VhH4caYNSeMPM7gOuC2h9laIoK3JIM6T9cJmdnNOZfnCiwfwqULY7EjLbA7Vg9W3HeuNY4VDSUVSMrXkCsnN8HB15PfvsAw5Szl10ZMRzRx0p0JR_In3fC8tcuE58-EM7fHGuKTUuJeoHKvjwEWl1xnJAdie1gvA1WZLCsw0iU1KlaUYwfVSklZ1k8gA7-vpTf-A7szEezvcmcng0khEu-i5HOARHy4gwoN1gGGxW66AiMzHBsfswz4g8RM6PjshceQIFGC8WBe5X1iEAls_uPwLhT3zny8qkWvhMecIoV-MJeGtO7NAF0LVD7ngdgpT8JSvSQnqJT5DsHeX3gqcT6JPSR08qNQySDU9otQO62nr-jR8amRDgxrabqWSS0iIgtEDxDjmSoGSKmlYucrHXC-kZgMXJY3ZlbcG27Hf3BbZzN0ta8mvI1Ql2-5muWvfDKwZPOwHxVV_-1BMDN4vowfEDCgCrBfumLTaW4903YRZ7w_xjshJ8Z5rHPIx82vw6ftmB-jGqXXLbm0nsqqhVFvUfHcCl7H7aUFPGfGomxsmX9FsE1nEgr2FF_1TEoCM_vln7MaiL4uzkAnKk_3cgD8pK8mKa_59eXYMfEE5xHndwxIeEo_Gpg3twpoI6AGc90XDDnsAp2yOu7JQ6vy0-XfTrGzuk_ngW9XfC9Q-057ctoM9ZPsmiYDjod0gShtxTk1k-1OerLqBIeUUtMe3LZmiuanPYIcjC5w74CSj3ehH1MurPUpeKeosdxgNx_lDXT3PdpQmCFIgZM5kM5mj3Q4WwB3lmbbhfqff_789Ydbn3mftPL5aYrRFvNjirqCawOg8G1VSlxG03550ruCDiAIb_WQRx6R6luUYpezZl_-WHhW1wvWiEaoVKNy3Z_n9YUZCMObCxk78KCSiCELQcnEhNTm-wlTKn4L9-CRyku05jHqWweK9IJYcXInb1jaA2MvZDQZewqwR8rRtJVgHWWHplCasgGYe9Qmcq-dj3O0SfBan8vx7d8vhfSXVxuxXgWsGfDU1Q8gm-XAQJbaRBRBUJebOyI_e-R46wiz_WygxQGgCB2QxLwQ6HbKExvWCwiL_wYkpv42sZZdj721aKv-MvfDNglyttbYHerOpokFwS3uqV20uJGnBvSrxVvoLofcztVJCfvPuv6N-ZmbclWCX8qs2e9Ky53C02x3GZhglyzwv1NuW4_pfqzlb-wwIh_zSlotWWF4-062Xt-AfXDMbBQexrDAC9stBP0Xo5VYUZsS3m6z8bvBIWaSAn5n_sFk53Bv96hY4bD0XDbx1Q3HUZ8o-UstrnkK95jlSqgYSgY61K9t9vRW5uwa6OBdz3OHi_4juvJ_1oHJUol-l-0WfQflJEXzVIQbSAIaPE1sQ5MN5KNSipyxwq813xdUDdcHCO5v4_8MsgpdQbXO9Il3MknztGJv2fc2Y7bzsYqUWwho51s-i9wte0PBVrNbHkWy0xlKynqCW8WOIEtA2L20rDZU5wEXskJBF4yb2rhlzX6BX_dKeRLizzkcjs7yFCIyiZ7PmjFHfTX6yVDH_T_m__4PvP6_95oLAAA=");
                        //accessTokenDetails.Add(m_embedConfig.EmbedToken.Expiration.ToString());
                        //sample.Add("30b3fc6b - 9a7a - 4ab6 - a4e2 - bf1985471a49");
                        //m_embedConfig.EmbedToken.TokenId = sample[1];
                        //m_embedConfig.EmbedToken.Token = sample[0];
                        List<string> accessTokenDetails = new List<string>();
                        accessTokenDetails.Add(tokenResponse.Token);
                        accessTokenDetails.Add(tokenResponse.Expiration.ToString());
                        accessTokenDetails.Add(tokenResponse.TokenId);
                        
                        memoryCache.Add(report.Id, accessTokenDetails, expiration);
                    }





                    // Generate Embed Configuration.
                    //m_embedConfig.EmbedToken = tokenResponse;
                    m_embedConfig.EmbedUrl = report.EmbedUrl;
                    m_embedConfig.Id = report.Id;
                }
            }
            catch (HttpOperationException exc)
            {
                m_embedConfig.ErrorMessage = string.Format("Status: {0} ({1})\r\nResponse: {2}\r\nRequestId: {3}", exc.Response.StatusCode, (int)exc.Response.StatusCode, exc.Response.Content, exc.Response.Headers["RequestId"].FirstOrDefault());
                return false;
            }

            return true;
        }
        //public EmbedConfig GetEmbedToken(string reportId)
        //{
        //    // This is how you create embed token with effective identities
        //    var getCredentialsResult = GetTokenCredentials();

        //    // Generate Embed Token for reports without effective identities.
        //    GenerateTokenRequest generateTokenRequestParameters = new GenerateTokenRequest(accessLevel: "view");

        //    var client = new PowerBIClient(new Uri(ApiUrl), m_tokenCredentials);
        //    var tokenResponse = client.Reports.GenerateTokenInGroupAsync(WorkspaceId, reportId, generateTokenRequestParameters).Result;
        //    m_embedConfig.EmbedToken = tokenResponse;

        //    return m_embedConfig;
        //}
        //public string GenerateEmbedTokenIfNotInCache(string reportId)
        //{
        //    EmbedService embedService = new EmbedService();
        //    EmbedConfig m_embedConfig;
        //    System.Runtime.Caching.MemoryCache memoryCache = System.Runtime.Caching.MemoryCache.Default;
        //    m_embedConfig = (EmbedConfig)embedService.GetEmbedToken(ReportId);
        //    DateTimeOffset expiration = DateTimeOffset.UtcNow.AddMinutes(55);
        //    List<string> accessTokenDetails = new List<string>();
        //    accessTokenDetails.Add(m_embedConfig.EmbedToken.Token);
        //    accessTokenDetails.Add(m_embedConfig.EmbedToken.Expiration.ToString());
        //    string accessToken = m_embedConfig.EmbedToken.Token;
        //    memoryCache.Add(reportId, accessTokenDetails, expiration);
        //    return accessToken;
        //}
        [HttpGet]
        // [Route("api/Employee/GetAccessToken/{reportId}")]
        //public string GetAccessToken()
        //{
        //    var accessToken = "";
        //    List<string> storedReportIdDetails = null;
        //    System.Runtime.Caching.MemoryCache memoryCache = System.Runtime.Caching.MemoryCache.Default;
        //    if (memoryCache.Contains(ReportId))
        //    {
        //        var listItems = memoryCache.Get(ReportId);
        //        storedReportIdDetails = listItems as List<string>;
        //        if (DateTimeOffset.UtcNow.DateTime.CompareTo(DateTime.Parse(storedReportIdDetails[1], CultureInfo.InvariantCulture)) >= 0)
        //        {
        //            accessToken = GenerateEmbedTokenIfNotInCache(ReportId);
        //        }
        //        else
        //        {
        //            accessToken = storedReportIdDetails[0];
        //        }
        //    }
        //    else
        //    {
        //        accessToken = GenerateEmbedTokenIfNotInCache(ReportId);
        //    }
        //    return accessToken;
        //}
        

      

        /// <summary>
        /// Check if web.config embed parameters have valid values.
        /// </summary>
        /// <returns>Null if web.config parameters are valid, otherwise returns specific error string.</returns>
        private string GetWebConfigErrors()
        {
            // Application Id must have a value.
            if (string.IsNullOrWhiteSpace(ApplicationId))
            {
                return "ApplicationId is empty. please register your application as Native app in https://dev.powerbi.com/apps and fill client Id in web.config.";
            }

            // Application Id must be a Guid object.
            Guid result;
            if (!Guid.TryParse(ApplicationId, out result))
            {
                return "ApplicationId must be a Guid object. please register your application as Native app in https://dev.powerbi.com/apps and fill application Id in web.config.";
            }

            // Workspace Id must have a value.
            if (string.IsNullOrWhiteSpace(WorkspaceId))
            {
                return "WorkspaceId is empty. Please select a group you own and fill its Id in web.config";
            }

            // Workspace Id must be a Guid object.
            if (!Guid.TryParse(WorkspaceId, out result))
            {
                return "WorkspaceId must be a Guid object. Please select a workspace you own and fill its Id in web.config";
            }

            if (AuthenticationType.Equals("MasterUser"))
            {
                // Username must have a value.
                if (string.IsNullOrWhiteSpace(Username))
                {
                    return "Username is empty. Please fill Power BI username in web.config";
                }

                // Password must have a value.
                if (string.IsNullOrWhiteSpace(Password))
                {
                    return "Password is empty. Please fill password of Power BI username in web.config";
                }
            }
            else
            {
                if (string.IsNullOrWhiteSpace(ApplicationSecret))
                {
                    return "ApplicationSecret is empty. please register your application as Web app and fill appSecret in web.config.";
                }

                // Must fill tenant Id
                if (string.IsNullOrWhiteSpace(Tenant))
                {
                    return "Invalid Tenant. Please fill Tenant ID in Tenant under web.config";
                }
            }

            return null;
        }

        private AuthenticationResult DoAuthentication()
        {
            AuthenticationResult authenticationResult = null;
            if (AuthenticationType.Equals("MasterUser"))
            {
                var authenticationContext = new AuthenticationContext(AuthorityUrl);

                // Authentication using master user credentials
                var credential = new UserPasswordCredential(Username, Password);
                authenticationResult = authenticationContext.AcquireTokenAsync(ResourceUrl, ApplicationId, credential).Result;

            }
            else
            {
                // For app only authentication, we need the specific tenant id in the authority url
                var tenantSpecificURL = AuthorityUrl.Replace("common", Tenant);
                var authenticationContext = new AuthenticationContext(tenantSpecificURL);

                // Authentication using app credentials
                var credential = new ClientCredential(ApplicationId, ApplicationSecret);
                authenticationResult = authenticationContext.AcquireTokenAsync(ResourceUrl, credential).Result;
            }

            return authenticationResult;
        }

        private bool GetTokenCredentials()
        {
            // var result = new EmbedConfig { Username = username, Roles = roles };
            var error = GetWebConfigErrors();
            if (error != null)
            {
                m_embedConfig.ErrorMessage = error;
                return false;
            }

            // Authenticate using created credentials
            AuthenticationResult authenticationResult = null;
            try
            {
                authenticationResult = DoAuthentication();
            }
            catch (AggregateException exc)
            {
                m_embedConfig.ErrorMessage = exc.InnerException.Message;
                return false;
            }

            if (authenticationResult == null)
            {
                m_embedConfig.ErrorMessage = "Authentication Failed.";
                return false;
            }

            m_tokenCredentials = new TokenCredentials(authenticationResult.AccessToken, "Bearer");
            return true;
        }
    }
}