﻿#region Summary
// *****************************************************************
// Project:     AuditTool
// Solution:    AuditToolWebApp
//
// Author:  MAQ Software
// Date:    January 28, 2020
// Description: app class
// Change History:
// Name             Date                Version             Description
// ---------------------------------------------------------------------------------------------------------------------------------
//  Deepak G     September 07, 2018      2.0.0.0            app class
//  Harsh L      February  06, 2020      2.0.0.0            app class
// ---------------------------------------------------------------------------------------------------------------------------------
// Copyright (C) MAQ Software
// ---------------------------------------------------------------------------------------------------------------------------------
#endregion

namespace Audit_Tool
{
    using System;
    using System.Threading.Tasks;
    using System.Web;
    using Audit_Tool.Helpers;
    using Microsoft.IdentityModel.Clients.ActiveDirectory;
    using Microsoft.IdentityModel.Tokens;
    using Microsoft.Owin.Security;
    using Microsoft.Owin.Security.Cookies;
    using Microsoft.Owin.Security.Notifications;
    using Microsoft.Owin.Security.OpenIdConnect;
    using Owin;
    using System.Web.Mvc;
    using System.Configuration;
    using System.Web.Configuration;

    public partial class Startup
    {
        public static string AccessToken = string.Empty;

        /// <summary>
        /// The Client ID is used by the application to uniquely identify itself to Azure AD.
        /// The Metadata Address is used by the application to retrieve the signing keys used by Azure AD.
        /// The AAD Instance is the instance of Azure, for example public Azure or Azure China.
        /// The Authority is the sign-in URL of the tenant.
        /// The Post Logout Redirect Uri is the URL where the user will be redirected after they sign out.
        /// </summary>
        /// <param name="app"></param>
        public void ConfigureAuth(IAppBuilder app)
        {

            app.SetDefaultSignInAsAuthenticationType(CookieAuthenticationDefaults.AuthenticationType);
            app.UseCookieAuthentication(new CookieAuthenticationOptions());
            app.UseOpenIdConnectAuthentication(
                new OpenIdConnectAuthenticationOptions
                {
                    ClientId = Constants.clientId,
                    Authority = Constants.authority,
                    PostLogoutRedirectUri = Constants.postLogoutRedirectUri + Constants.signInHTML,
                    // Scope = "Sign in and read user profile",
                    ResponseType = "code id_token",
                    TokenValidationParameters = new TokenValidationParameters
                    {
                        SaveSigninToken = true,
                        ValidateIssuer = false,
                    },
                    Notifications = new OpenIdConnectAuthenticationNotifications()
                    {
                        AuthorizationCodeReceived = this.OnAuthorizationCodeReceived,
                        RedirectToIdentityProvider = (context) =>
                        {
                            // This ensures that the address used for sign in and sign out is picked up dynamically from the request
                            // this allows you to deploy your app (to Azure Web Sites, for example) without having to change settings
                            // Remember that the base URL of the address used here must be provisioned in Azure AD beforehand.
                            string appBaseUrl = context.Request.Scheme + "://" + context.Request.Host + context.Request.PathBase;

                            context.ProtocolMessage.RedirectUri = appBaseUrl + "/";
                            context.ProtocolMessage.PostLogoutRedirectUri = appBaseUrl + Constants.signInHTML;
                            return Task.FromResult(0);
                        },
                        AuthenticationFailed = context =>
                        {
                            context.HandleResponse();
                            context.Response.Redirect("/Error?message=" + context.Exception.Message);
                            return Task.FromResult(0);
                        },
                    },
                });

        }


        /// <summary>
        /// adds access token in current context after authorization
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        [ValidateAntiForgeryToken]
        private async Task OnAuthorizationCodeReceived(AuthorizationCodeReceivedNotification context)
        {
            var code = context.Code;

            ClientCredential credential = new ClientCredential(Constants.clientId, Constants.appkey);
            string userObjectID = context.AuthenticationTicket.Identity.FindFirst(Constants.claimIdentifier).Value;
            Microsoft.IdentityModel.Clients.ActiveDirectory.AuthenticationContext authContext = new Microsoft.IdentityModel.Clients.ActiveDirectory.AuthenticationContext(Constants.authority, new NaiveSessionCache(userObjectID));

            // If you create the redirectUri this way, it will contain a trailing slash.  
            // Make sure you've registered the same exact Uri in the Azure Portal (including the slash).
            Uri uri = new Uri(HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Path));

            AuthenticationResult result = await authContext.AcquireTokenByAuthorizationCodeAsync(code, uri, credential, Constants.GraphEndpoint);
            AccessToken = result.AccessToken;

            HttpContext.Current.Session.Add("AccessToken", AccessToken);

        }
    }
}