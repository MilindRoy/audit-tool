// <copyright  company="Microsoft"  file="SecurityHandler.cs">
// Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
// ===============================================================================
// ------------------------------------------------------------------------------
// Project:        GFP with Power BI
// Dependencies:   None
// Description:    Class for getting current logged in user alias and name
// ------------------------------------------------------------------------------
// ===============================================================================
// 
//      Developed by:
//      MAQ Software
//      Digital Marketing and Technology Solutions
//      Redmond / Hyderabad / Mumbai
// 
// Change Tracking
// Date                 Author          Description
// Feb 02, 2017         v-shrank        Created
// ===============================================================================
// ------------------------------------------------------------------------------

namespace GlobalReportFramework.Common
{
    using Data;
    using Helpers;
    using Microsoft.Owin;
    using Models;
    using Newtonsoft.Json.Linq;

    #region usings
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Diagnostics;
    using System.Globalization;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Security.Claims;
    using System.Text;
    using System.Threading.Tasks;
    using System.Web;
    using System.Web.Mvc;
    #endregion
    /// <summary>
    /// To handler Security related Scenarios
    /// </summary>
    public static class SecurityHandler
    {

        /// <summary>
        /// Method to apply impersonation
        /// </summary>
        /// <returns>user alias</returns>
        public static string GetCurrentUserAlias()
        {
            string currentUserName = String.Empty;
            try
            {
                currentUserName = HttpContext.Current.User.Identity.Name;
                if (!String.IsNullOrEmpty(currentUserName))
                {
                    if (currentUserName.Split('@').Length >= 2)
                    {
                        currentUserName = currentUserName.Split('@')[0].ToLower(CultureInfo.InvariantCulture);
                    }
                    else
                    {
                        currentUserName = null;
                    }
                }
                else
                {
                    currentUserName = null;
                }
            }
            catch (IndexOutOfRangeException exceptionObj)
            {
                ApplicationInsightsEventHandler.Write(exceptionObj);
                System.Diagnostics.Trace.TraceError("Exception occured: {0}", exceptionObj.Message);
                currentUserName = null;
            }
            catch (Exception exceptionObj)
            {
                ApplicationInsightsEventHandler.Write(exceptionObj);
                System.Diagnostics.Trace.TraceError("Exception occured: {0}", exceptionObj.Message);
                currentUserName = null;
            }
            return currentUserName;
        }

        public static async Task SetUserRole(string graphToken)
        {
            string UserAlias = SecurityHandler.GetCurrentUserAlias();
            // Verification Code Expired or Session variable is null
            if ((null == HttpContext.Current.Session[Constants.CONSTANTS_IS_ADMIN]) || ((DateTime)HttpContext.Current.Session[Constants.CONSTANTS_IS_ADMIN_TIMEOUT_CHECK]).AddMinutes(Constants.CONSTANTS_IS_ADMIN_TIMEOUT) < DateTime.Now)
            {
                try
                {
                    await CheckIfAdmin(graphToken);
                    Trace.TraceError("Check if user is admin or not complete [User: " + UserAlias + "] (" + DateTime.UtcNow + "): " + Utilities.watch.ElapsedMilliseconds);
                    System.Web.HttpContext.Current.Session.Add(Constants.CONSTANTS_IS_ADMIN, Constants.IS_ADMIN);
                    System.Web.HttpContext.Current.Session.Add(Constants.CONSTANTS_IS_ADMIN_TIMEOUT_CHECK, DateTime.Now);
                    Trace.TraceError("Fetch user admin records complete [User: " + UserAlias + "] (" + DateTime.UtcNow + "): " + Utilities.watch.ElapsedMilliseconds);
                }
                catch (Exception ExceptionObj)
                {
                    Trace.TraceError("Fetch user admin records failed  [User: " + UserAlias + "] with [Exception Message: {0}] (" + DateTime.UtcNow + "): " + Utilities.watch.ElapsedMilliseconds, ExceptionObj.Message);
                    ApplicationInsightsEventHandler.LogEvent(ExceptionObj.Message);
                }
            }
        }

        /// <summary>
        /// To fetch data from Cube or DB based on the connection string and query (by calling the On-premise data service) 
        /// </summary>
        /// <returns></returns>
        public static async Task<ActionResult> CheckIfAdmin(string AccessToken)
        {
            string userObjectID = ClaimsPrincipal.Current.FindFirst(Constants.claimIdentifier).Value;
            try
            {
                string GraphAPIEndPointURL = Constants.GraphResourceId + "/myorganization/isMemberOf?api-version=" + Constants.GraphAPIVersion;
                using (HttpClientHandler handler = new HttpClientHandler())
                {
                    using (HttpClient client = new HttpClient(handler))
                    {
                        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", AccessToken);
                        HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "relativeAddress");
                        await getCachedGroupId(AccessToken);
                        request.Content = new StringContent("{\"groupId\":\"" + Constants.SecurityGroupID + "\",\"memberId\":\"" + userObjectID + "\"}",
                                                            Encoding.UTF8,
                                                            "application/json");//CONTENT-TYPE header
                        HttpResponseMessage message = Task.Run(() =>
                        {
                            return client.PostAsync(GraphAPIEndPointURL, request.Content);
                        }).Result;
                        dynamic response = Task.Run(() =>
                        {
                            return message.Content.ReadAsAsync<dynamic>();
                        }).Result;
                        if (null != response["value"])
                        {
                            try
                            {
                                Constants.IS_ADMIN = (bool)response["value"];
                            }
                            catch
                            {
                                
                                Trace.TraceError("Check if user exists from the allowed security group failed (" + DateTime.UtcNow + "): " + Utilities.watch.ElapsedMilliseconds);
                            }
                        }
                        else
                        {
                            Constants.IS_ADMIN = false;
                            Trace.TraceError("Check if user exists from the allowed security group failed (" + DateTime.UtcNow + "): " + Utilities.watch.ElapsedMilliseconds);
                        }
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                ApplicationInsightsEventHandler.Write(ex);
                return null;
            }
        }

        public static async Task<ActionResult> getCachedGroupId(string AccessToken)
        {
            //Get the feature config from database
            DataFetcherConfig connectionManager = null;
            //JavaScriptSerializer javaScriptSerializer = null;
            List<FeatureConfig> list = new List<FeatureConfig>();
            string cachedValue = String.Empty, key = String.Empty, Group = Common.Constants.SecurityGroupRedisKey;
            connectionManager = new DataFetcherConfig();
            connectionManager.RedisEndPoint = Common.Constants.REDIS_END_POINT_VALUE;
            connectionManager.RedisAccessKey = Common.Constants.REDIS_ACCESS_KEY_VALUE;
            key = Group;
            try
            {
                Trace.TraceError("Send request to fetch configuration from Redis init (" + DateTime.UtcNow + "): " + Utilities.watch.ElapsedMilliseconds);
                // Object holding configuration of grid in json string format
                cachedValue = DataFetcher.GetDataFromRedisCache(connectionManager, key);
                Trace.TraceError("Send request to fetch configuration from Redis complete (" + DateTime.UtcNow + "): " + Utilities.watch.ElapsedMilliseconds);
            }
            catch (Exception exception)
            {
                // Redis peaked
                ApplicationInsightsEventHandler.Write(exception);
            }

            if (!string.IsNullOrWhiteSpace(cachedValue))
            {
                Constants.SecurityGroupID = cachedValue;
            }
            else
            {
                try
                {
                    await getGroupId(AccessToken);
                }
                catch (Exception ex)
                {
                    ApplicationInsightsEventHandler.Write(ex);
                    return null;
                }
            }
            return null;
        }

        /// <summary>
        /// To fetch data from Cube or DB based on the connection string and query (by calling the On-premise data service) 
        /// </summary>
        /// <param name="memberID">Object id of the AAD identity of the logged in user</param>
        /// <returns></returns>
        public static async Task<ActionResult> getGroupId(string AccessToken)
        {
            string userObjectID = ClaimsPrincipal.Current.FindFirst(Constants.claimIdentifier).Value;
            try
            {

                string GraphAPIEndPointURL = Constants.GraphResourceId + "/" + Constants.tenant + "/groups?api-version=" + Constants.GraphAPIVersion + "&$filter=displayName eq " + "'" + (Constants.SecurityGroupName).Trim() + "'";
                using (HttpClientHandler handler = new HttpClientHandler())
                {
                    using (HttpClient client = new HttpClient(handler))
                    {
                        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", AccessToken);
                        HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "relativeAddress");
                        HttpResponseMessage message = Task.Run(() =>
                        {
                            return client.GetAsync(GraphAPIEndPointURL);
                        }).Result;
                        dynamic response = Task.Run(() =>
                        {
                            return message.Content.ReadAsAsync<dynamic>();
                        }).Result;
                        if (null != response["value"] && null != response["value"][0] && null != response["value"][0]["objectId"])
                        {
                            Constants.SecurityGroupID = (String)response["value"][0]["objectId"];
                        }
                        else
                        {
                            Trace.TraceError("Check if user exists from the allowed security group failed (" + DateTime.UtcNow + "): " + Utilities.watch.ElapsedMilliseconds);
                        }
                        return response;
                    }
                }
            }
            catch (Exception ex)
            {
                ApplicationInsightsEventHandler.Write(ex);
                return null;
            }
        }
    }
}