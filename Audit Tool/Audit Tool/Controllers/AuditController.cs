﻿using Audit_Tool.Classes;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace Audit_Tool.Controllers
{
    public class AuditController : Controller
    {
        // GET: Audit
        public ActionResult Index(string ProjectName, string PMName, string AuditType, string DateTime, string id)
        {
            SQLData sql = new SQLData();
            //string ProjectName = Request.QueryString["ProjectName"];
            //string PMName = Request.QueryString["PMName"];
            //string AuditType = Request.QueryString["AuditType"];
            //string DateTime = Request.QueryString["Date"];
            Dictionary<string, string> prevAuditDict = sql.RetrievePrevAuditDataQueryString(id);
           // string prevAuditData = JsonConvert.DeserializeObject(prevAuditDict);
            this.ViewBag.ProjectName = ProjectName;
            this.ViewBag.PMName = PMName;
            this.ViewBag.AuditType = AuditType;
            this.ViewBag.Date = DateTime;
            
           this.ViewBag.Creator = prevAuditDict["creator-bag"];
           
            //this.ViewBag.Creator = prevAuditDict;
            this.ViewBag.AuditScore = prevAuditDict["score-bag"];
            this.ViewBag.UpdatedBy = prevAuditDict["updaters-bag"];
            this.ViewBag.Reviewers = prevAuditDict["reviewers-bag"];
            this.ViewBag.ActiveReviewer = prevAuditDict["active-reviewer-bag"];
            this.ViewBag.Url = prevAuditDict["url-bag"];
            string projectId = prevAuditDict["ProjectID"];
            string data = sql.RetrievePrevChecklist(projectId, AuditType, PMName, DateTime);
            data = data.Replace("&quot;", "\"");
           // JObject data1 = JObject.Parse(data);
            //string hh = "ho";
            this.ViewBag.Checklist = data.ToString();
            return View();
        }
    }
}