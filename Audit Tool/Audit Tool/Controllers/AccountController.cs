﻿#region Summary
// *****************************************************************
// Project:     ExportToPPT
// Solution:    ExportToPPTWebApp
//
// Author:  MAQ Software
// Date:    September 07, 2018
// Description: performs all account related actions/operations
// Change History:
// Name             Date                Version             Description
// ---------------------------------------------------------------------------------------------------------------------------------
//  Deepak G     September 07, 2018      2.0.0.0            performs all account related actions/operations
// ---------------------------------------------------------------------------------------------------------------------------------
// Copyright (C) MAQ Software
// ---------------------------------------------------------------------------------------------------------------------------------
#endregion
namespace Audit_Tool.Controllers
{
    using System;
    using System.Linq;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Script.Serialization;
    using Audit_Tool.Helpers;
    using Microsoft.Owin.Security;
    using Microsoft.Owin.Security.OpenIdConnect;

    public class AccountController : Controller
    {
        /// <summary>
        /// performs signin operation.
        /// </summary>
        /// <returns>HttpUnauthorizedResult.</returns>
        public ActionResult SignIn()
        {
            this.HttpContext.GetOwinContext().Authentication.Challenge(new AuthenticationProperties { RedirectUri = "/" }, OpenIdConnectAuthenticationDefaults.AuthenticationType);
            return new HttpUnauthorizedResult();
        }

        /// <summary>
        /// performs signout operation using lambda expression.
        /// lambda expression returns cookie and authentication type i.e openidconnect
        /// </summary>
        public void SignOut()
        {
            this.Request.GetOwinContext()
            .Authentication
            .SignOut(this.HttpContext.GetOwinContext()
                           .Authentication.GetAuthenticationTypes()
                           .Select(o => o.AuthenticationType).ToArray());

        }

        //[HttpPost]
        //public JsonResult SessionInterval()
        //{
        //    Configuration conf = WebConfigurationManager.OpenWebConfiguration(System.Web.Hosting.HostingEnvironment.ApplicationVirtualPath);
        //    SessionStateSection section = (SessionStateSection)conf.GetSection("system.web/sessionState");
        //    int timeout = (int)section.Timeout.TotalMinutes * 60;
        //    return Json(timeout, JsonRequestBehavior.AllowGet);
        //}

        /// <summary>
        /// Gets access token from current session and returns access in json format.
        /// </summary>
        /// <returns>Json data containing access token.</returns>
        public async Task<ActionResult> GetToken()
        {
            var token = System.Web.HttpContext.Current.Session["AccessToken"];
            string accessToken = string.Empty;
            if (token == null)
            {
                TokenHandler handler = new TokenHandler();
                await handler.setAccessToken(this.HttpContext.GetOwinContext(), Constants.postLogoutRedirectUri);
                accessToken = System.Web.HttpContext.Current.Session["AccessToken"].ToString();
            }
            else
            {
                accessToken = token.ToString();
            }

            return this.Json(accessToken, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Returns username in json format from current context.
        /// </summary>
        /// <returns>Json.</returns>
        public JsonResult GetUsername()
        {
            string username;
            try
            {
                username = ((ClaimsIdentity)System.Web.HttpContext.Current.User.Identity).FindFirst("name").Value;
            }
            catch (Exception)
            {
                username = string.Empty;
            }

            return this.Json(username, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Returns emailid in json format from current user identity.
        /// </summary>
        /// <returns>Json.</returns>
        public JsonResult GetEmailId()
        {
            string emailId;
            try
            {
                emailId = System.Web.HttpContext.Current.User.Identity.Name;
            }
            catch (Exception)
            {
                emailId = null;
            }

            return this.Json(emailId, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Checks if user is in the SG group and returns true or false based on it.
        /// </summary>
        /// <returns>Json.</returns>
        public JsonResult CheckUserinSG()
        {
            var token = System.Web.HttpContext.Current.Session["AccessToken"];
            bool checkUser = false;
            JsonResult username = this.GetEmailId();
            string user = new JavaScriptSerializer().Serialize(username.Data);
            string graphAPIEndPointURL = string.Format("https://graph.microsoft.com/beta/groups/{0}/members", Constants.SecurityGroupID);

            string userList = GraphAPIHelper.GetUserList(token.ToString(), graphAPIEndPointURL);
            string[] ulists = userList.Split(',');
            for (int i = 0; i < ulists.Length; i++)
            {
                if (user.Contains(ulists[i]))
                {
                    checkUser = true;
                }
            }

            return this.Json(checkUser, JsonRequestBehavior.AllowGet);
        }
    }
}
