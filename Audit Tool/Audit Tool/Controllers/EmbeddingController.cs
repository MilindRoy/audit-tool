﻿using Audit_Tool.Models;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Reflection;
using System.Web.Mvc;
using Audit_Tool.Services;

namespace Audit_Tool.Controllers
{
    public class EmbeddingController : Controller
    {
        // GET: Embedding
        //public ActionResult Index()
        //{
        //    return View();
        //}
        public ActionResult Index()
        {
            var result = new IndexConfig();
            var assembly = Assembly.GetExecutingAssembly().GetReferencedAssemblies().Where(n => n.Name.Equals("Microsoft.PowerBI.Api")).FirstOrDefault();
            if (assembly != null)
            {
                result.DotNETSDK = assembly.Version.ToString(3);
            }
            return View(result);
        }
        private readonly IEmbedService m_embedService;

        public EmbeddingController()
        {
            m_embedService = new EmbedService();
        }
        public ActionResult EmbedReport(string username, string roles)
        {
            var embedResult =  m_embedService.EmbedReport(username, roles);
            if (embedResult)
            {
                return View(m_embedService.EmbedConfig);
            }
            else
            {
                return View(m_embedService.EmbedConfig);
            }
        }
    }
}