﻿namespace Audit_Tool.Controllers
{
    using Audit_Tool.Helpers;
    using System;
    using System.Web.Mvc;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;
    using System.Web.Script.Serialization;

    [Authorize]
    public class UpdateChecklistController : Controller
    { 

        public ActionResult updateChecklist()
        {

            AccountController ac = new AccountController();
            JsonResult flag = ac.CheckUserinSG();
            string sFlag = new JavaScriptSerializer().Serialize(flag.Data);
            if (sFlag=="false")
            {
                return Redirect("/Error/Error/");
            }
            string auditType = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Files/AuditType.txt"));
            ViewBag.AuditType = auditType.Split(',');
            
            return View();
        }
        [HttpPost]
        public JsonResult modifyChecklist(string auditType, string updatedString)
        {
            string sourceFile = System.Web.HttpContext.Current.Server.MapPath("~/Files/checklist/checklistForAuditType.json");
            string destinationFile = System.Web.HttpContext.Current.Server.MapPath("~/Files/checklist/checklistForAuditType" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".json");
            try
            {
                //System.IO.File.Create(destinationFile);
                //System.IO.File.Copy(sourceFile, destinationFile, true);
                string jsonForTheCheckList = System.IO.File.ReadAllText(sourceFile);
                JObject objectForTheChecklist = JObject.Parse(jsonForTheCheckList);
                var auditDefaultChecklist = objectForTheChecklist[auditType];

                JObject modifiedChecklistForAudit = JObject.Parse(updatedString);
                objectForTheChecklist[auditType] = modifiedChecklistForAudit;

                string modifiedChecklist = JsonConvert.SerializeObject(objectForTheChecklist);

                System.IO.File.WriteAllText(destinationFile, modifiedChecklist);

                return Json("Success", JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return Json("Error", JsonRequestBehavior.AllowGet);
            }
        }
    }
}