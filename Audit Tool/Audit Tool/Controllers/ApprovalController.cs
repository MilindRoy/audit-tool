﻿namespace Audit_Tool.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Text;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Script.Serialization;
    using Audit_Tool.Classes;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    [Authorize]
    public class ApprovalController : Controller
    {
        private static string PMComments = string.Empty;
        private SQLData sql = new SQLData();

        /// <summary>
        /// Opens up the approval page for users vith valid email ID's of PMs.
        /// </summary>
        /// <param name="projectName">Name of project.</param>
        /// <param name="pMName">Project Manager name.</param>
        /// <param name="auditType">Name of the type of audit.</param>
        /// <param name="date">Datetime of the audit creation.</param>
        /// <returns>A view to either error page or approval page.</returns>
        public ActionResult Approve(string projectName, string pMName, string auditType, string date)
        {
                Dictionary<string, string> comments = this.sql.RetrievePrevAuditData(projectName, pMName, auditType, date);
                string pMemails = this.sql.GetPMEmails();
                JavaScriptSerializer js = new JavaScriptSerializer();
                var dynJson = js.Deserialize<dynamic>(pMemails);
                bool pMflag = false;
                AccountController ac = new AccountController();
                int length = dynJson.Length;
                for (int numberOfManagers = 0; numberOfManagers < length; numberOfManagers++)
                {
                    JsonResult username = ac.GetEmailId();
                    string user = JsonConvert.SerializeObject(username.Data);
                    string email = dynJson[numberOfManagers]["email"];
                    if (user.Contains(email))
                    {
                        pMflag = true;
                    }
                }

                if (!pMflag)
                {
                    return this.Redirect("/Error/Error/");
                }

                string reviewersList = comments["reviewers-bag"];
                string[] auditComments = sql.GetAuditComments(projectName, pMName, auditType, date);
                auditComments[0] = HttpUtility.UrlDecode(auditComments[0], System.Text.Encoding.Default);
                auditComments[1] = HttpUtility.UrlDecode(auditComments[1], System.Text.Encoding.Default);
                this.ViewBag.ProjectName = projectName;
                this.ViewBag.PMName = pMName;
                this.ViewBag.AuditType = auditType;
                this.ViewBag.Date = date;
                this.ViewBag.P0Comments = auditComments[0];
                this.ViewBag.P1Comments = auditComments[1];
                this.ViewBag.Reviewers = reviewersList;
                this.ViewBag.Comments = comments["comments"];
                this.ViewBag.EndPoint = string.Concat("/Approval/UpdateDatabase", "?", "ProjectName=", HttpUtility.UrlEncode(projectName), "&PMName=", HttpUtility.UrlEncode(pMName), "&AuditType=", HttpUtility.UrlEncode(auditType), "&Date=", HttpUtility.UrlEncode(date), "&Passed=Approved");
                this.ViewBag.EndPointFailed = string.Concat("/Approval/UpdateDatabase", "?", "ProjectName=", HttpUtility.UrlEncode(projectName), "&PMName=", HttpUtility.UrlEncode(pMName), "&AuditType=", HttpUtility.UrlEncode(auditType), "&Date=", HttpUtility.UrlEncode(date), "&Passed=Rejected");
                return this.View();
        }

        // To give page only to PM
        //public bool CheckPMAccess()
        //{
        //    bool isPM = false;
        //    JsonResult PMemails = sql.GetPMEmails();
        //    AccountController ac = new AccountController();
        //    JsonResult username = ac.GetEmailId();
        //    string user = new JavaScriptSerializer().Serialize(username.Data);
        //    string PMuser = new JavaScriptSerializer().Serialize(PMemails.Data);
        //    return isPM;
        //    //JObject jo = new JObject();
        //}

        /// <summary>
        /// Updates entries in database and displays message to user.
        /// </summary>
        /// <param name="projectName">Project Name.</param>
        /// <param name="pMName">Project Manager Name.</param>
        /// <param name="auditType">Name of the type of audit.</param>
        /// <param name="date">Datetime from when the project was created.</param>
        /// <param name="passed">Indicates whether the audit was approved or rejcted.</param>
        /// <returns>A view.</returns>
        [HttpPost]
        public ActionResult UpdateDatabase(string projectName, string pMName, string auditType, string date, string passed)
        {
            string comments = PMComments;
            SQLData sql = new SQLData();
            Dictionary<string, string> createdBy = sql.RetrievePrevAuditData(projectName, pMName, auditType, date);
            bool isUpdated = sql.ApproveAuditForPM(projectName, pMName, auditType, date, passed, comments);
            string result = string.Empty;
            string messageToDisplay = string.Empty;

            if (isUpdated && passed == "Approved")
            {
                string[] auditComments = sql.GetAuditComments(projectName, pMName, auditType, date);
                auditComments[0] = HttpUtility.UrlDecode(auditComments[0], System.Text.Encoding.Default);
                auditComments[1] = HttpUtility.UrlDecode(auditComments[1], System.Text.Encoding.Default);
                result = this.CreateBug(projectName, pMName, auditType, createdBy["creator-bag"], auditComments, date);
            }

            if (passed == "Approved")
            {
                if (result == "success")
                {
                    messageToDisplay = "Approved the audit and successfully took the respective VSO actions";
                }
                else
                {
                    messageToDisplay = "Approved the audit but failed to take the respective VSO actions";
                }
            }
            else
            {
                messageToDisplay = "Rejected the audit successfully";
            }

            this.ViewBag.Message = isUpdated ? messageToDisplay : "Something went wrong. Please try again later.";
            return this.View();
        }

        /// <summary>
        /// Returns success on bug creation otherwise returns error.
        /// </summary>
        /// <param name="projectName">Name of the project.</param>
        /// <param name="pMName">Nmae of the project manager.</param>
        /// <param name="auditType">Name of the audit type.</param>
        /// <param name="createdBy">Email id of the audit creator.</param>
        /// <param name="auditComments">Comments given by manager.</param>
        /// <param name="date">Datetime of audit creation.</param>
        /// <returns>string.</returns>
        public string CreateBug(string projectName, string pMName, string auditType, string createdBy, string[] auditComments, string date)
        {
            try
            {
                string[] VSO = this.sql.GetVSOToken(projectName);
                if (VSO[0] == string.Empty)
                {
                    return "failure";
                }

                string _credentials = Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(string.Format("{0}:{1}", "", VSO[0])));

                object[] detailsOfBug = new object[4];

                for (int iteratorToCreateBugs = 0; iteratorToCreateBugs < 2; iteratorToCreateBugs++)
                {
                    if (auditComments[iteratorToCreateBugs] != string.Empty)
                    {
                        detailsOfBug[0] = new { op = "add", path = "/fields/System.Title", value = "P" + iteratorToCreateBugs + " Bug from the '" + auditType + "' audit by " + createdBy + " on " + date };
                        detailsOfBug[1] = new { op = "add", path = "/fields/Microsoft.VSTS.TCM.ReproSteps", value = auditComments[iteratorToCreateBugs] };
                        detailsOfBug[2] = new { op = "add", path = "/fields/Microsoft.VSTS.Common.Priority", value = "1" };
                        detailsOfBug[3] = new { op = "add", path = "/fields/Microsoft.VSTS.Common.Severity", value = "1 - Critical" };
                        int createdBugId = 0;
                        string createdBugURL = string.Empty;

                        using (var client = new HttpClient())
                        {
                            client.DefaultRequestHeaders.Accept.Clear();
                            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", _credentials);

                            //serialize the fields array into a json string
                            var bugsValue = new StringContent(JsonConvert.SerializeObject(detailsOfBug), Encoding.UTF8, "application/json-patch+json");

                            var method = new HttpMethod("PATCH");
                            var request = new HttpRequestMessage(method, VSO[1] + VSO[2] + "/_apis/wit/workitems/$Bug?api-version=2.2") { Content = bugsValue };
                            var response = client.SendAsync(request).Result;

                            var result = response.Content.ReadAsStringAsync().Result;
                            dynamic dynJson = JObject.Parse(result);
                            string createdBug = dynJson.id.ToString();
                            createdBugId = int.Parse(createdBug);
                            createdBugURL = HttpUtility.UrlEncode(dynJson.url.ToString());

                            this.sql.StoreBugs(projectName, pMName, auditType, createdBy, date, iteratorToCreateBugs, createdBugId, createdBugURL);
                        }
                    }
                }

                return "success";
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return "failure";
            }
        }

        /// <summary>
        /// Sets the comments entered by manager into Viewbag.
        /// </summary>
        /// <param name="comments">comments given by manager.</param>
        /// <returns>View.</returns>
        [HttpPost]
        public ActionResult SetComments(string comments)
        {
            PMComments = comments;
            return this.View();
        }
    }
}