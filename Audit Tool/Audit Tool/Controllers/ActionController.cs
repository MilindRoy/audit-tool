﻿namespace Audit_Tool.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Web.Mvc;
    using Audit_Tool.Classes;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;
    using static Models.Structures;
    using context = System.Web.HttpContext;

    public class ActionController : Controller
    {
        private SQLData sql = new SQLData();

        /// <summary>
        /// Gives us the latest checklist json file from directory.
        /// </summary>
        /// <param name="directory">Directory where the file is stored.</param>
        /// <returns>File.</returns>
        public static FileInfo GetNewestFile(DirectoryInfo directory)
        {
            return directory.GetFiles()
                .Union(directory.GetDirectories().Select(d => GetNewestFile(d)))
                .OrderByDescending(f => (f == null ? DateTime.MinValue : f.LastWriteTime))
                .FirstOrDefault();
        }

        /// <summary>
        /// Returns all the values which fill the "Checklist" column in all the tables.
        /// These tables show up when user chooses an Audit type.
        /// This value is taken from a json file named "checklistForAuditType.json".
        /// </summary>
        /// <param name="auditType">type of audit selected</param>
        /// <returns>string.</returns>
        [HttpPost]
        public string GetCheckList(string auditType)
        {
            FileInfo newestFile = GetNewestFile(new DirectoryInfo(context.Current.Server.MapPath("~/Files/checklist/")));
            string getChecklist = System.IO.File.ReadAllText(newestFile.FullName);
            JObject data = JObject.Parse(getChecklist);
            try
            {
                // changed the variable type from var to a suggested type "Newtonsoft.Json.Linq.JToken"
                Newtonsoft.Json.Linq.JToken list = data[auditType];
                return JsonConvert.SerializeObject(list);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
            }

            return null;
        }

        //public JsonResult GetReports(string projectId)
        //{
        //    try
        //    {
        //        string reports = this.sql.RetrieveReports(projectId);
        //        return this.Json(reports, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        ExceptionLogging.SendErrorToText(ex);
        //        return this.Json("Error", JsonRequestBehavior.AllowGet);
        //    }
        //}

        /// <summary>
        ///  Retrieves and returns the projectID, projectManagerID and Project Name ordered by project name.
        /// </summary>
        /// <returns>string of projects.</returns>
        [HttpPost]
        public string GetProjects()
        {
            string data = this.sql.CommonQuery("GetProjects");
            if (data.Equals(string.Empty))
            {
                data = "0";
            }

            return data;
        }

        /// <summary>
        ///  Retrieves and returns in Json the bugID for p0 and p1 Observation.
        /// </summary>
        /// <param name="projectName">Name of project.</param>
        /// <param name="pMName">Name of manager.</param>
        /// <param name="auditType">Type of audit.</param>
        /// <param name="date">Date on which audit was created.</param>
        /// <returns>Json.</returns>
        [HttpPost]
        public JsonResult GetBugs(string projectName, string pMName, string auditType, string date)
        {
            try
            {
                string[] bugs = this.sql.RetrieveBugs(projectName, pMName, auditType, date);
                return this.Json(bugs, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return this.Json("Error", JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Retrieves and returns data in Json for all fields displayed on Audit page except for bug ID.
        /// </summary>
        /// <param name="projectName">Name of project.</param>
        /// <param name="pMName">Name of manager.</param>
        /// <param name="auditType">Type of audit.</param>
        /// <param name="date">Date on which audit was created.</param>
        /// <returns>Json.</returns>
        [HttpPost]
        public JsonResult GetPrevAuditData(string projectName, string pMName, string auditType, string date)
        {
            try
            {
                Dictionary<string, string> prevAuditDict = this.sql.RetrievePrevAuditData(projectName, pMName, auditType, date);
                string prevAuditData = JsonConvert.SerializeObject(prevAuditDict, Formatting.Indented);
                return this.Json(prevAuditData, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return this.Json("Error", JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GetPrevAuditData2(string projectName, string pMName, string auditType, string date)
        {
            try
            {
                Dictionary<string, string> prevAuditDict2 = this.sql.RetrievePrevAuditData2(projectName, pMName, auditType, date);
                string prevAuditData2 = JsonConvert.SerializeObject(prevAuditDict2, Formatting.Indented);
                return this.Json(prevAuditData2, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return this.Json("Error", JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// In case of multiple comments for an audit, this function returns json data which is shown in different rows
        /// one below the other in a table on Audit page.
        /// </summary>
        /// <param name="projectId">Chosen project name.</param>
        /// <param name="projectManagerId">Chosen manager name.</param>
        /// <param name="date">Date of audit creation.</param>
        /// <returns>Json.</returns>
        [HttpPost]
        public JsonResult GetPreviousObservations(string projectId, string projectManagerId, string auditType, string date)
        {
            try
            {
                string getObservations = this.sql.GetPreviousObservations(projectId, projectManagerId, auditType, date, 0);
                return this.Json(getObservations, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return this.Json("Error", JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Returns Json result using the parameters for getting value unique value.
        /// </summary>
        /// <param name="projectName">Name of project.</param>
        /// <param name="pMName">Project manager name.</param>
        /// <param name="auditType">Type of audit.</param>
        /// <param name="date">Date of audit creation.</param>
        /// <returns>Jsonresult containig column name as key and corresponding value.</returns>
        [HttpPost]
        public JsonResult GetSubmitStatus(string projectName, string pMName, string auditType, string date)
        { 
            bool res = this.sql.RetrieveSubmitStatus(projectName, pMName, auditType, date);
            return this.Json(res, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Retrieves and returns the emails of people who updated the audit.
        /// </summary>
        /// <param name="projectId">Chosen project name</param>
        /// <param name="auditType">Type of audit.</param>
        /// <param name="projectManagerId">Chosen manager name.</param>
        /// <param name="date">Date of audit creation.</param>
        /// <returns>string.</returns>
        [HttpPost]
        public string GetPrevChecklist(string projectId, string auditType, string projectManagerId, string date)
        {
            string data = this.sql.RetrievePrevChecklist(projectId, auditType, projectManagerId, date);
            if (data.Equals(string.Empty))
            {
                data = "0";
            }

            return data;
        }

        ///// <summary>
        ///// Inserts multiple observations in Observations table.
        ///// </summary>
        ///// <param name="projectId">ID of the project.</param>
        ///// <param name="projectManagerId">Project Manager name.</param>
        ///// <param name="auditDate">Date of the audit creation.</param>
        ///// <param name="comments">string containing all observations</param>
        ///// <returns>A string stating success or failure.</returns>
        //[HttpPost]
        //public string SubmitMultipleObservations(string projectId, string projectManagerId, string auditDate, string auditType, string[] comments, string[] severity, string[] area)
        //{
        //    bool result = this.sql.InsertMultipleObservations(projectId, projectManagerId, auditDate, auditType, comments, severity, area);
        //    if (result)
        //    {
        //        return "1";
        //    }

        //    return "0";
        //}

        /// <summary>
        /// Returns all the project managers.
        /// </summary>
        /// <returns>string.</returns>
        [HttpPost]
        public string GetProjectManager()
        {
            string data = this.sql.CommonQuery("GetProjectManager");
            if (data.Equals(string.Empty))
            {
                data = "0";
            }

            return data;
        }

        /// <summary>
        /// Inserts/Updates the Audit Table with the new entry.
        /// Runs when user presses the submit key after making chnages in the audit form.
        /// </summary>
        /// <param name="details">A class defined inside the Model named 'Structures.cs'.</param>
        /// <param name="callAutoSave">A parameter to indicate if the call came from Autosave function in Index.js.</param>
        /// <returns>string.</returns>
        [HttpPost]
        public string SubmitAudit(SubmitStruct details, bool callAutoSave, string[] mulObservations, string[] severity, string[] area)
        {
            PropertyInfo[] properties = typeof(SubmitStruct).GetProperties();
            foreach (PropertyInfo property in properties)
            {
                if (property.GetValue(details) == null)
                {
                    property.SetValue(details, string.Empty);
                }
            }

            string data = "0";
            int sendMailOrNo = (details.saveOrSubmit == "Submit") ? 1 : 0;

            try
            {
                if (details.flag == "0")
                {
                    data = this.sql.InsertAuditTable(details, sendMailOrNo, callAutoSave, mulObservations, severity, area);
                }
                else
                {
                    string date = details.flag;
                    data = this.sql.UpdateAuditTable(details, date, sendMailOrNo, callAutoSave, mulObservations, severity, area);
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return "0";
            }

            return data;
        }

        /// <summary>
        /// Using the unique combination of the paramtres we retrieve the dates of previous audits
        /// and display them in drop down box of audit date on home page.
        /// </summary>
        /// <param name="projectId">Selected project name.</param>
        /// <param name="auditType">Type of audit.</param>
        /// <param name="projectManagerId">Chosen project manager name.</param>
        /// <returns>string.</returns>
        public string GetPreviousAuditDates(string projectId, string auditType, string projectManagerId)
        {
            string data = this.sql.GetPreviousAuditDates(projectId, auditType, projectManagerId);
            return data;
        }

        /// <summary>
        /// Asks confirmation for approval/rejection of audit.
        /// </summary>
        /// <param name="projectId">Project name slected on home page.</param>
        /// <param name="auditType">Type of audit.</param>
        /// <param name="projectManagerId">Chosen project manager name.</param>
        /// <param name="dateTime">Selected date from previous avaiable audits.</param>
        /// <param name="projectName">Name of the project.</param>
        /// <param name="flag">Used as a parameter to either approve or reject audit.</param>
        /// <param name="username">Stores the email id of the user who approves or rejects the audit.</param>
        /// <returns>integer value.</returns>
        public string ApproveRejectAudit(string projectId, string auditType, string projectManagerId, string dateTime, string projectName, string flag, string username)
        {
            string reviewers = this.sql.RetrieveAuditReviewers(projectName, projectManagerId, auditType, dateTime, projectId);
            if (reviewers.Equals("0"))
            {
                return "failure";
            }

            string[] allReviewers = reviewers.Split(new string[] { "," }, StringSplitOptions.None);
            bool verified = false;
            for (int i = 0; i < allReviewers.Length; i++)
            {
                if (allReviewers[i].Trim().Equals(username))
                {
                    verified = true;
                }
            }

            if (verified)
            {
                if (flag.Equals("1"))
                {
                    return this.sql.ApproveAudit(projectId, auditType, projectManagerId, dateTime, username);
                }
                else if (flag.Equals("0"))
                {
                    return this.sql.RejectAudit(projectId, auditType, projectManagerId, dateTime, username);
                }
            }

            return "restricted";
        }
    }
}