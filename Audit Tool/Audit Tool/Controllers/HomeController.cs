﻿#region Summary
// *****************************************************************
// Project:     AuditTool
// Solution:    AuditToolWebApp
//
// Author:  MAQ Software
// Date:    January 28, 2020
// Description: app class
// Change History:
// Name             Date                Version             Description
// ---------------------------------------------------------------------------------------------------------------------------------
//  Deepak G     September 07, 2018      2.0.0.0            app class
//  Harsh L      Febuary   06, 2020       
// ---------------------------------------------------------------------------------------------------------------------------------
// Copyright (C) MAQ Software
// ---------------------------------------------------------------------------------------------------------------------------------
#endregion
namespace Audit_Tool.Controllers
{
    using System.Web;
    using System.Web.Mvc;
    using Audit_Tool.Helpers;

    public class HomeController : Controller
    {
        /// <summary>
        /// Reads data from AuditType.txt and returns view which is used to represent names in dropdown of Audit type on index page.
        /// </summary>
        /// <returns>View.</returns>
        [Authorize]
        public async System.Threading.Tasks.Task<ActionResult> Index()
        {
            TokenHandler handler = new TokenHandler();
            await handler.setAccessToken(this.HttpContext.GetOwinContext(), "/");
            string auditType = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/Files/AuditType.txt"));

            this.ViewBag.AuditType = auditType.Split(Constants.Comma);
            this.ViewBag.ProjectName = new string[]{ "loading" };
            return this.View();
        }

        [Authorize]
        public ActionResult Test()
        {
            return null;
        }

    }
}