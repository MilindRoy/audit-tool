﻿namespace Audit_Tool.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using System.Text;
    using System.Web.Mvc;
    using Audit_Tool.Models;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;
    using RestSharp;

    [Authorize]
    public class CalendarController : Controller
    {
        /// <summary>
        /// Returns the calendar view.
        /// </summary>
        /// <returns>View.</returns>
        public ActionResult CalendarView()
        {
            return this.View();
        }

        /// <summary>
        /// Gets all user data.
        /// </summary>
        /// <returns>Json array.</returns>
        [Route("Calendar/GetAllUsersData")]
        [HttpGet]
        public JArray GetAllUsersData()
        {
            // Preparing for authorization (Delegated Access Token)
            string accessToken = "Bearer " + System.Web.HttpContext.Current.Session["AccessToken"].ToString();

            // Here, page size is 500
            RestClient client = new RestClient("https://graph.microsoft.com/v1.0/users?$top=500");
            client.Timeout = -1;
            RestRequest request = new RestRequest(Method.GET);
            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            request.AddHeader("Host", "graph.microsoft.com");
            request.AddHeader("Authorization", accessToken);
            IRestResponse response = client.Execute(request);

            // JSON data received from the server
            JObject json = JObject.Parse(response.Content);

            // Converting JSON object to JSON array
            JArray arr = (JArray)json["value"];

            // Filtering Display Name and ID and converting to a list
            var users = arr.Select(x => new UserDetails { Name = x["displayName"].ToString(), ID = x["id"].ToString() }).ToList();

            List<UserDetails> userDetails = users;

            // Since number of users are greater than 500, we need to fetch user details from subsequent pages
            try
            {
                while (true)
                {
                    // Link that refer us to the next page
                    string referLink = json["@odata.nextLink"].ToString();
                    client = new RestClient(referLink);
                    client.Timeout = -1;
                    request = new RestRequest(Method.GET);
                    request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
                    request.AddHeader("Host", "graph.microsoft.com");
                    request.AddHeader("Authorization", accessToken);
                    response = client.Execute(request);
                    json = JObject.Parse(response.Content);
                    arr = (JArray)json["value"];
                    users = arr.Select(x => new UserDetails { Name = x["displayName"].ToString(), ID = x["id"].ToString() }).ToList();
                    userDetails.AddRange(users);
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            // Converting List to String
            var stringJson = JsonConvert.SerializeObject(userDetails);

            // Converting the above string into JSON array
            return JArray.Parse(stringJson);
        }

        /// <summary>
        /// Returns user specific data.
        /// </summary>
        /// <param name="id">Id of the user.</param>
        /// <returns>Json array.</returns>
        [Route("Calendar/GetUserSpecificData/{ID}")]
        [HttpGet]
        public JArray GetUserSpecificData(string id)
        {
            // Preparing for authorization (Delegated Access Token)
            string accessToken = "Bearer " + System.Web.HttpContext.Current.Session["AccessToken"].ToString();
            RestClient client = new RestClient("https://graph.microsoft.com/v1.0/users/" + id + "/memberOf");
            client.Timeout = -1;
            RestRequest request = new RestRequest(Method.GET);
            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            request.AddHeader("Host", "graph.microsoft.com");
            request.AddHeader("Authorization", accessToken);
            IRestResponse response = client.Execute(request);

            // JSON data received from the server
            JObject json = JObject.Parse(response.Content);

            // Converting JSON object to JSON array
            JArray arr = (JArray)json["value"];

            // Filtering Group ID, Group Name and Visibility and converting to a list
            var groups = arr.Select(x => new GroupDetails { GroupID = x["id"].ToString(), GroupName = x["displayName"].ToString(), Visibility = x["visibility"] != null ? x["visibility"].ToString() : "" }).ToList();

            // Preparing JSON string
            StringBuilder finalJSON = new StringBuilder("[");
            List<GroupDetails> groupDetails = groups;

            // Iterating over each group that the user belongs to
            foreach (var item in groupDetails)
            {
                finalJSON = finalJSON.Append("{\"EventList\":");
                JArray value = null;
                try
                {
                    // Here, page size is 50
                    string url = "https://graph.microsoft.com/v1.0/groups/" + item.GroupID + "/events?$top=50";
                    client = new RestClient(url);
                    client.Timeout = -1;
                    request = new RestRequest(Method.GET);
                    request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
                    request.AddHeader("Host", "graph.microsoft.com");
                    request.AddHeader("Authorization", accessToken);
                    response = client.Execute(request);

                    // JSON data received from the server
                    json = JObject.Parse(response.Content);

                    // Converting JSON object to JSON array
                    value = (JArray)json["value"];

                    // Since number of events can be greater than 50, we need to fetch event details from subsequent pages
                    try
                    {
                        while (true)
                        {
                            // Link that refer us to the next page
                            string referLink = json["@odata.nextLink"].ToString();
                            client = new RestClient(referLink);
                            client.Timeout = -1;
                            request = new RestRequest(Method.GET);
                            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
                            request.AddHeader("Host", "graph.microsoft.com");
                            request.AddHeader("Authorization", accessToken);
                            response = client.Execute(request);
                            json = JObject.Parse(response.Content);
                            JArray temp = (JArray)json["value"];

                            // Adding events from subsequent pages to the 'value' JArray
                            foreach (var it in temp)
                            {
                                value.Add(it);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        ex.ToString();
                    }
                }
                catch (Exception ex)
                {
                    ex.ToString();
                }

                // Preparing JSON string
                if (value != null)
                {
                    finalJSON = finalJSON.Append(value.ToString());
                    finalJSON = finalJSON.Append(",\"GroupName\":\"" + item.GroupName + "\",\"GroupID\":\"" + item.GroupID + "\",\"Visibility\":\"" + item.Visibility + "\"},");
                }
                else
                {
                    finalJSON = finalJSON.Append("null");
                    finalJSON = finalJSON.Append(",\"GroupName\":\"" + item.GroupName + "\",\"GroupID\":\"" + item.GroupID + "\",\"Visibility\":null},");
                }
            }

            // Removing comma from the last
            finalJSON = finalJSON.Remove(finalJSON.Length - 1, 1);

            // Appending closing square bracket to the string
            finalJSON = finalJSON.Append("]");

            // Converting the JSON string to JArray
            return JArray.Parse(finalJSON.ToString());
        }

        /// <summary>
        /// Returns data for the user who has signed in.
        /// </summary>
        /// <returns>Json array.</returns>
        [Route("Calendar/GetSignedInUserDataJD")]
        [HttpGet]
        public JArray GetSignedInUserDataJD()
        {
            // Preparing POST request for obtaining Application Access Token
            var client = new RestClient("https://login.microsoftonline.com/e4d98dd2-9199-42e5-ba8b-da3e763ede2e/oauth2/v2.0/token");
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Host", "login.microsoftonline.com");
            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            request.AddHeader("scope", "https%3A%2F%2Fgraph.microsoft.com%2F.default");
            request.AddParameter("client_id", "ea1e44a1-5f6b-46be-b07b-2b1698f327ef");
            request.AddParameter("scope", "https://graph.microsoft.com/.default");
            request.AddParameter("client_secret", "m7FA-uR.s-uGB7iRfW7OoyIm6AkGHRj-");
            request.AddParameter("grant_type", "client_credentials");
            IRestResponse response = client.Execute(request);

            // Response from server
            JObject json = JObject.Parse(response.Content);

            // Fetching Access Token from the response
            string accessToken = json["access_token"].ToString();

            // Creating a list of groups
            List<GroupDetails> groupDetails = new List<GroupDetails>();

            // 0 - Code Review Team
            // 1 - Architecture Review Team
            groupDetails.Add(new GroupDetails() { GroupName = "Code Review Team", GroupID = "18fe27c1-ff5f-48df-9279-990c673b9bb4", EmailID = "codereviewteam@maqsoftware.com", Visibility = "Private" });
            groupDetails.Add(new GroupDetails() { GroupName = "Architecture Review Team", GroupID = "2444413c-14c0-4b6a-87be-e7520967ed92", EmailID = "ART@maqsoftware.com", Visibility = "Private" });

            // Creating an array of type JArray for storing events
            JArray[] events = new JArray[2];

            // 0 - Code Review Team
            // 1 - Architecture Review Team
            for (int i = 0; i < events.Length; i++)
            {
                events[i] = new JArray();
            }

            // Preparing GET request
            string authorization = "Bearer " + accessToken;
            string fill = ConfigurationManager.AppSettings["ida:Keywords"];
            string[] keywords = fill.Split(',');
            keywords = keywords.Select(x => "startsWith(subject, '" + x + "')").ToArray();
            string res = string.Join(" or ", keywords);
            client = new RestClient("https://graph.microsoft.com/v1.0/users/45e31b97-4c4b-48b5-83c6-4dc02748828d/calendar/events?$top=500&$filter=" + res);
            client.Timeout = -1;
            request = new RestRequest(Method.GET);
            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            request.AddHeader("Host", "graph.microsoft.com");
            request.AddHeader("Authorization", authorization);
            response = client.Execute(request);

            // JSON data received from the server
            json = JObject.Parse(response.Content);

            // Converting JSON object to JSON array
            JArray arr = (JArray)json["value"];

            // Filtering attendees and converting to a list
            var attendeesList = arr.Select(x => x["attendees"]).ToList();

            // Segreggating event data into groups on the basis of email address present in the attendees object
            var countEvent = 0;
            attendeesList.ForEach(x =>
            {
                var list1 = x.Where(b => b["emailAddress"]["address"].ToString().Equals(groupDetails[0].EmailID)).ToList();
                var list2 = x.Where(b => b["emailAddress"]["address"].ToString().Equals(groupDetails[1].EmailID)).ToList();
                if (list1.Count != 0)
                {
                    events[0].Add(arr[countEvent]);
                }

                if (list2.Count != 0)
                {
                    events[1].Add(arr[countEvent]);
                }

                countEvent++;
            });

            // Since number of events are greater than 500, we need to fetch event details from subsequent pages
            try
            {
                while (true)
                {
                    // Link to fetch event details from subsequent pages
                    string referLink = json["@odata.nextLink"].ToString();
                    client = new RestClient(referLink);
                    client.Timeout = -1;
                    request = new RestRequest(Method.GET);
                    request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
                    request.AddHeader("Host", "graph.microsoft.com");
                    request.AddHeader("Authorization", authorization);
                    response = client.Execute(request);
                    json = JObject.Parse(response.Content);
                    arr = (JArray)json["value"];
                    attendeesList = arr.Select(x => x["attendees"]).ToList();
                    countEvent = 0;
                    attendeesList.ForEach(x =>
                    {
                        var list1 = x.Where(b => b["emailAddress"]["address"].ToString().Equals(groupDetails[0].EmailID)).ToList();
                        var list2 = x.Where(b => b["emailAddress"]["address"].ToString().Equals(groupDetails[1].EmailID)).ToList();
                        if (list1.Count != 0)
                        {
                            events[0].Add(arr[countEvent]);
                        }

                        if (list2.Count != 0)
                        {
                            events[1].Add(arr[countEvent]);
                        }

                        countEvent++;
                    });
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            // Passing event details to their respective groups
            groupDetails[0].EventList = events[0];
            groupDetails[1].EventList = events[1];

            // Preparing JSON string
            StringBuilder finalJSON = new StringBuilder("[");
            for (int i = 0; i < events.Length; i++)
            {
                finalJSON = finalJSON.Append("{\"EventList\":");
                finalJSON = finalJSON.Append(events[i].ToString());
                finalJSON = finalJSON.Append(",\"GroupName\":\"" + groupDetails[i].GroupName + "\",\"GroupID\":\"" + groupDetails[i].GroupID + "\",\"Visibility\":\"" + groupDetails[i].Visibility + "\"},");
                if (i == (events.Length - 1))
                {
                    finalJSON = finalJSON.Remove(finalJSON.Length - 1, 1);
                }
            }

            finalJSON = finalJSON.Append("]");

            // Converting JSON string to JArray
            return JArray.Parse(finalJSON.ToString());
        }
    }
}