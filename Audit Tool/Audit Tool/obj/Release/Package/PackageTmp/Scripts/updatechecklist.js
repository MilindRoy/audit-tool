﻿$(document).ready(function () {
	$("#error-container").hide();
	var sGetEmailid = "/Account/GetEmailId";
	var sEmailId = "";
	var sGEmailId = "";
	var res = "";
	$.ajax({
		url: sGetEmailid,
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		success: (data) => {
			
			populateEmail(data);
		},
		error: function (a, b, error) { }
	});

	function populateEmail(email) {
		sEmailId = email;

	}
	let sUpdateChecklist1 = "/UpdateChecklist/ValidateUser";

	$.ajax({
		url: sUpdateChecklist1,
		method: "get",
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		success: function (data) {
		
			if (data === 'Error') {
				alert("Some error occurred. Please try again");
			}
			else {
				
				populateSGEmail(data);
				
			}


		},
		error: function (data) {
			
		}
	});

	function populateSGEmail(email) {
		sGEmailId = email;
		res = sGEmailId.split(",");
		//console.log("res: " + res);
		res.forEach(finduser);

	}

	
	
	
	function finduser(item, index)
	{
		if (sEmailId === item)
		{
			$("#site-container").show();
			$("#error-container").hide();
		}
		else {
			$("#site-container").hide();
			$("#error-container").show();
		}
	}
	
    let auditTypeSelectBox = document.querySelector("#audit-type-select-update");

    auditTypeSelectBox.addEventListener("change", function (e) {
        if (this.value == "-1") {
            resetAllElements();
            return;
        }
        fillRequirementChecklistTableToUpdate();
    });

    
	
});



function resetAllElements() {
    let tableContainer = document.querySelector("#requirement-checklist-update");
    tableContainer.innerHTML = "";
    document.getElementById("buttonRow").classList.add("hidden");
}

function fillRequirementChecklistTableToUpdate() {
    let loader = document.getElementById("loader-container");

    let _auditType = document.getElementById("audit-type-select-update").value;

    let ajax = new XMLHttpRequest();
    ajax.open("POST", "/Action/GetCheckList");
    ajax.setRequestHeader('Content-Type', 'application/json');

    ajax.addEventListener("load", function (e) {
        loader.classList.add("hidden");
        let json = e.currentTarget.responseText;
        if (json == '0' || json == '' || json == null) {
            let tableContainer = document.querySelector("#requirement-checklist-update");
            tableContainer.innerHTML = "<h6>No Data</h6>";
            return;
        }
        renderCheckListNewTableUpdateChecklist(JSON.parse(json));
    });
    ajax.addEventListener("error", function (e) {
        alert("Error");
    });

    ajax.send(JSON.stringify({ auditType: _auditType }));
    loader.classList.remove("hidden");
}

function renderCheckListNewTableUpdateChecklist(checklist) {
    document.getElementById("buttonRow").classList.remove("hidden");

    let thead = `<thead>
            <tr>
                <th>Checklist</th>
            </tr>
    </thead>`;

    let tableContainer = document.querySelector("#requirement-checklist-update");
    tableContainer.innerHTML = "";

    if (checklist == '' || checklist == undefined) {
        table.innerHTML = "<tr><td>No checklist available</td><tr/>";
    }

    let tr, td, textarea, fieldset, legend;
    let counter = 0;
    $.each(checklist, function (key, value) {

        fieldset = document.createElement('FIELDSET');
        legend = document.createElement('legend');
        legend.classList.add("table-legend");
        legend.appendChild(document.createTextNode(key + " "));

        var pluslink = document.createElement('a');
        pluslink.addEventListener('click', function () { addChecklist(key, counter, $(key).length - 1) });
        //pluslink.onclick = addChecklist(key);
        //pluslink.href = "#";
        pluslink.classList.add("iconLink");
        pluslink.classList.add("fa");
        pluslink.classList.add("fa-plus-square");
        pluslink.style.cursor = "pointer";
        legend.appendChild(pluslink);

        legend.classList.add("text-center");
        fieldset.appendChild(legend);

        table = document.createElement('table');
        table.classList.add("table", "table-bordered", "table-responsive", "table-hovered");

        table.setAttribute("id", key);
        var temp = key;

        //table.innerHTML = thead;
        tbody = document.createElement('tbody');

        value.forEach(function (element, index) {

            tr = document.createElement("tr");
            td = document.createElement("td");
            td.classList.add("updateChecklistClass");
            td.style.width = "1150px";
            td.setAttribute("name", `checklist-${counter}-${index}`);
            td.setAttribute("contenteditable", "false");
            //text = document.createElement('div');
            //text.setAttribute("contenteditable", "true");
            //text.setAttribute("style", "float: left;");
            //td.appendChild(text);
            //text.setAttribute("contenteditable", "true");
            td.innerHTML = element;


            cross = document.createElement('div');
            cross.classList.add("iconLink");
            cross.classList.add("fa");
            cross.classList.add("fa-remove");
            td.appendChild(cross);
            cross.addEventListener('click', function () { deleteRow(this) });
            cross.style.cssFloat = "right";
            cross.style.cursor = "pointer";
            //cross.setAttribute("readonly", "readonly");
            cross.setAttribute("contenteditable", "false");

            edit = document.createElement('div');
            edit.classList.add("iconlink");
            edit.classList.add("fa");
            edit.classList.add("fa-edit");
            td.appendChild(edit);
            edit.style.cssFloat = "right";
            edit.style.marginRight = "10px";
            edit.style.marginTop = "2px";
            edit.style.cursor = "pointer";
            edit.contentEditable = "false";
            var temp1 = td.getAttribute('name');
            edit.addEventListener('click', function () { editRow(temp1, temp) });
            //edit.setAttribute("style", "margin-right: 2px;");
            //edit.setAttribute("style", "padding-left: 1040px;");
            //edit.setAttribute("style", "padding-bottom: 2px;");
            td.appendChild(edit);

            tr.appendChild(td);

            tbody.appendChild(tr);
        });
        counter++;
        table.appendChild(tbody);
        fieldset.appendChild(table);

        tableContainer.appendChild(fieldset);

    });
}

function updateChecklist() {
    let iLengthOfLegends = document.getElementsByClassName("table-legend").length;
    let oUpdatedJSONForTheAudit = {};
    for (let iIteratorToFindOffsetName = 0; iIteratorToFindOffsetName < iLengthOfLegends; iIteratorToFindOffsetName++) {
        let sOffsetName = document.getElementsByClassName("table-legend")[iIteratorToFindOffsetName].innerText;
        oUpdatedJSONForTheAudit[sOffsetName] = [];
        let iLengthOfData = document.getElementsByClassName("table")[iIteratorToFindOffsetName].childNodes[0].childElementCount;
        for (let iIteratorToAddData = 0; iIteratorToAddData < iLengthOfData; iIteratorToAddData++) {
            let sData = document.getElementsByClassName("table")[iIteratorToFindOffsetName].childNodes[0].childNodes[iIteratorToAddData].childNodes[0].innerText;
            oUpdatedJSONForTheAudit[sOffsetName].push(sData);
        }
    }
    let sAauditType = document.getElementById("audit-type-select-update").value;

    let sUpdateChecklist = "/UpdateChecklist/modifyChecklist";
    let sModifiedChecklist = JSON.stringify(oUpdatedJSONForTheAudit);
    $.ajax({
        url: sUpdateChecklist,
        method: "post",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({ auditType: sAauditType, updatedString: sModifiedChecklist }),
        dataType: "json",
        success: function (data) {
            if (data === 'Error') {
                alert("Some error occurred. Please try again");
            }
            else {
                alert("Checklist for the audit type '" + sAauditType + "' has been successfully updated.");
            }
            resetAllElements();

            let auditTypeSelectBox = document.querySelector("#audit-type-select-update");
            auditTypeSelectBox.value = "-1";

        },
        error: function (data) {
            //console.log(data);
        }
    });

}

function resetChecklist() {
    resetAllElements();
    fillRequirementChecklistTableToUpdate();
}

function addChecklist(key, index, counter) {
    tableRef = document.getElementById(key);
    newRow = tableRef.insertRow();
    newCell = newRow.insertCell(0);
    newCell.setAttribute("contenteditable", "true");
    newCell.style.width = "1150px"
    newCell.setAttribute("name", `key-${counter}-${index}`);

    //text = document.createElement('div');
    ////text.setAttribute("contenteditable", "true");
    //text.setAttribute("style", "float: left;");
    //newCell.appendChild(text);

    cross = document.createElement('div');
    cross.classList.add("iconLink");
    cross.classList.add("fa");
    cross.classList.add("fa-remove");
    newCell.appendChild(cross);
    cross.addEventListener('click', function () { deleteRow(this) });
    cross.setAttribute("style", "float: right;");
    cross.style.cursor = "pointer";
    cross.setAttribute("contenteditable", "false");

    text = document.createElement('div');
    //text.setAttribute("contenteditable", "true");
    text.setAttribute("style", "float: left;");
    newCell.appendChild(text);
}

function deleteRow(arg) {
    $(arg).closest("tr").remove();
}

function editRow(arg, key) {

    tableRef = document.getElementById(key);
    var tds = tableRef.getElementsByTagName("td");
    
    for (i = 0; i < tds.length; i++) {

        if (tds[i].getAttribute('name') != arg) {
            tds[i].contentEditable = 'false';
            tds[i].classList.add("updateChecklistClass");

        }

        if (tds[i].getAttribute('name') == arg) {
            tds[i].contentEditable = 'true';
            tds[i].classList.remove("updateChecklistClass");

        }

    }
}