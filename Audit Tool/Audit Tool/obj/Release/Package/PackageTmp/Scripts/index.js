﻿$(document).ready(function () {
    getUserName();

    renderProjectManagers();
    renderProjects();

    let auditTypeSelectBox = document.querySelector("#audit-type-select");
    let auditDateSelectBox = document.querySelector("#audit-select-box");

    document.querySelector("#project-manager-name").addEventListener("change", function (e) {
        document.getElementById("creator-row").classList.add("hidden");
        document.getElementById("updaters-row").classList.add("hidden");
        document.getElementById("reviewers-bag").value = "";
        document.getElementById("url-bag").value = "";

        let PMname = this.options[this.selectedIndex].value;

        if (this.selectedIndex != 0) {
            loadAllProjectsForTrimming();
            let projectSelect = document.querySelectorAll("#project-name-select option");
            for (let index = 0; index < projectSelect.length; index++) {
                if (PMname != projectSelect[index].getAttribute('for')) {
                    $(projectSelect[index]).remove();
                }
            }
        }
        else {
            removeAllProjects();
        }

        auditTypeSelectBox.selectedIndex = 0;
        auditDateSelectBox.innerHTML = "<option value='-1'>New Audit</option>";

        resetResultTable();
    });

    document.querySelector("#project-name-select").addEventListener("change", function (e) {
        auditTypeSelectBox.selectedIndex = 0;
        auditDateSelectBox.innerHTML = "<option value='-1'>New Audit</option>";
        resetResultTable();
        document.getElementById("creator-row").classList.add("hidden");
        document.getElementById("updaters-row").classList.add("hidden");
        document.getElementById("reviewers-bag").value = "";
        document.getElementById("url-bag").value = "";

    });

    auditTypeSelectBox.addEventListener("change", function (e) {
        if (this.value == "-1") {
            resetResultTable();
            return;
        }
        renderAuditDateSelectBox();
        fillRequirementChecklistTable();
        //resetResultTable();
    });

    //document.querySelector("#submit-audit-btn").innerText = "Submit";

    document.querySelector("#submit-audit-btn").addEventListener("click", function (e) {
        submitAudit($(this).val());
    });


    document.querySelector("#save-audit-btn").addEventListener("click", function (e) {
        submitAudit($(this).val());
    });

    auditDateSelectBox.addEventListener("change", function (e) {
        if (this.value == "-1") {
            fillRequirementChecklistTable();
        }
        else {
            let date = this.value.trim();
            getPreviousAudit(date);
        }
    });

});

let oProjectsJSON;

function getUserName() {

    let sUserNameURL = "/Account/GetUsername";
    //Make Ajax request to get user name
    $.ajax({
        url: sUserNameURL,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: populateUsername,
        error: function (a, b, error) {
            var userLabel = document.getElementById("userLabel");
            userLabel.innerHTML = "Username failure";
        }
    });
}

function populateUsername(username) {
    let userLabel = document.getElementById("userLabel");
    userLabel.innerHTML = "Hello, " + username;
    userLabel.setAttribute("title", username);
    userName = username;
}

function renderChecklist() {
    if (sEmailId === "amrishs@maqsoftware.com" || sEmailId === "meghad@maqsoftware.com" || sEmailId === "rakshitg@maqsoftware.com" || sEmailId === "harshl@maqsoftware.com" || sEmailId === "ravikumart@maqsoftware.com") {
        document.getElementById("checklist-container").classList.remove("hidden");
    }
}

function renderAuditDateSelectBox() {

    let _pMId = document.getElementById("project-manager-name").value;
    let _projectId = document.getElementById("project-name-select").value;
    let _auditType = document.getElementById("audit-type-select").value;
    let select = document.querySelector("#audit-select-box");

    let params = {
        'projectId': _projectId,
        'auditType': _auditType,
        'projectManagerId': _pMId,
    }
    params = JSON.stringify(params);
    let ajax = new XMLHttpRequest();
    ajax.open("POST", "/Action/GetPreviousAuditDates");
    ajax.setRequestHeader('Content-Type', 'application/json');
    ajax.addEventListener("load", function (e) {
        let json = e.currentTarget.responseText;


        let html = `<option value="-1" selected>New Audit</option>`;
        if (json == '' || json == '0' || json == null || json == undefined) {
            html += "<option value='0' disabled>No previous audit available</option>"
            select.innerHTML = html;
            return;
        }
        json = JSON.parse(json);
        json.forEach(function (element, index) {
            let datetime = element.DateTime.substring(0, element.DateTime.indexOf('.')).replace("T", "  ");
            html += `<option value="${element.DateTime}">${datetime}</option>`
        });
        select.innerHTML = html;
    });
    ajax.addEventListener("error", function (e) {
        alert("Server error");
    });
    ajax.send(params);
    select.innerHTML = `<option value="-1" selected>Loading...</option>`;
}

function getPreviousAudit(date) {

    let loader = document.getElementById("loader-container");
    let _pMId = document.getElementById("project-manager-name").value;
    let _projectId = document.getElementById("project-name-select").value;
    let _auditType = document.getElementById("audit-type-select").value;
    let sProjectName = $("#project-name-select option:selected").text();

    let params = {
        'projectId': _projectId,
        'auditType': _auditType,
        'projectManagerId': _pMId,
        'date': date
    }

    params = JSON.stringify(params);

    document.getElementById("creator-row").classList.remove("hidden");
    document.getElementById("updaters-row").classList.remove("hidden");
    document.getElementById("reviewers-bag").value = "";
    document.getElementById("url-bag").value = "";

    var sGetReviewers = "/Action/GetReviewers";
    $.ajax({
        url: sGetReviewers,
        method: "post",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({ projectName: sProjectName, PMName: _pMId, AuditType: _auditType, Date: date }),
        dataType: "json",
        success: function (data) {
            if (data === 'Error') {
                
                document.getElementById("reviewers-bag").value = "Some error occurred. Please try again."
            }
            else {
                document.getElementById("reviewers-bag").value = data;
            }
        },
        error: function (data) {
            //console.log(data);
        }
    });

    var sProjectURL = "/Action/GetProjectURL";
    $.ajax({
        url: sProjectURL,
        method: "post",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({ projectName: sProjectName, PMName: _pMId, AuditType: _auditType, Date: date }),
        dataType: "json",
        success: function (data) {
            if (data === 'Error') {
                document.getElementById("url-bag").value = "Some error occurred. Please try again."
            }
            else {
                document.getElementById("url-bag").value = data;
            }
        },
        error: function (data) {
            //console.log(data);
        }
    });
    document.getElementById("url-bag").readOnly = true;

    var sGetCreator = "/Action/GetCreator";
    $.ajax({
        url: sGetCreator,
        method: "post",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({ projectName: sProjectName, PMName: _pMId, AuditType: _auditType, Date: date }),
        dataType: "json",
        success: function (data) {
            if (data === 'Error') {
                document.getElementById("creator-bag").value = "Some error occurred. Please try again."
            }
            else {
                document.getElementById("creator-bag").value = data;
            }
        },
        error: function (data) {
            //console.log(data);
        }
    });

    var sGetScore = "/Action/GetAuditScore";
    $.ajax({
        url: sGetScore,
        method: "post",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({ projectName: sProjectName, PMName: _pMId, AuditType: _auditType, Date: date }),
        dataType: "json",
        success: function (data) {
            if (data === 'Error') {
                document.getElementById("score-bag").value = "Some error occurred. Please try again."
            }
            else {
                document.getElementById("score-bag").value = data;
            }
        },
        error: function (data) {
            //console.log(data);
        }
    });

    

    var sGetPreviousObservations = "/Action/GetPreviousObservations";
    $.ajax({
    	url: sGetPreviousObservations,
    	method: "post",
    	contentType: "application/json; charset=utf-8",
    	data: JSON.stringify({ projectId: _projectId, projectManagerId: _pMId, Date: date }),
    	dataType: "json",
    	success: function (data) {
    		if (data === 'Error') {
    			document.getElementById("all-observations").value = "Some error occurred. Please try again."
    		}
    		else {
    			
    			

    			
    				document.getElementById("all-observations").innerHTML = data;
    			
		
    		}
    	},
    	error: function (data) {
    		//console.log(data);
    	}
    });

    var sGetUpdaters = "/Action/GetUpdaters";
    $.ajax({
        url: sGetUpdaters,
        method: "post",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({ projectName: sProjectName, PMName: _pMId, AuditType: _auditType, Date: date }),
        dataType: "json",
        success: function (data) {
            if (data === 'Error') {
                document.getElementById("updaters-bag").value = "Some error occurred. Please try again."
            }
            else {
                document.getElementById("updaters-bag").value = data;
            }
        },
        error: function (data) {
            //console.log(data);
        }
    });

    var sGetBugs = "/Action/GetBugs";
    $.ajax({
        url: sGetBugs,
        method: "post",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({ projectName: sProjectName, PMName: _pMId, AuditType: _auditType, Date: date }),
        dataType: "json",
        success: function (data) {
            if (data === 'Error') {
                document.getElementById("p0-bug").value = "Some error occurred";
                document.getElementById("p1-bug").value = "Some error occurred";

            }
            else {
                document.getElementById("p0-bug").value = data[0];
                document.getElementById("P0BugRedirect").setAttribute("href", data[1]);
                document.getElementById("p1-bug").value = data[2];
                document.getElementById("P1BugRedirect").setAttribute("href", data[3]);
            }
        },
        error: function (data) {
            //console.log(data);
        }
    });
 
    let ajax = new XMLHttpRequest();
    ajax.open("POST", "/Action/GetPreviousAudit");
    ajax.setRequestHeader('Content-Type', 'application/json');
    ajax.addEventListener("load", function (e) {
        loader.classList.add("hidden");
        let json = e.currentTarget.responseText;
        if (json == '0' || json == '' || json == null) {
            resetResultTable();
            alert("Error GetPreviousAudit");
            return;
        }  
        json = JSON.parse(json);    
        let isApproved = json[0].approve;
        let checklistJSon = JSON.parse(json[0].checklist.replace(/&quot;/g, "\""));

      
        renderCheckListTable(checklistJSon, isApproved);

        document.querySelector("#submit-audit-btn").innerText = "Update";
        let formBtn = document.querySelector("#form-btn-container");
        formBtn.classList.remove("hidden");
        document.getElementById("buttonRow").classList.remove("hidden");
        document.getElementById("Bug-id-container").classList.remove("hidden");

        });
    
    ajax.addEventListener("error", function (e) {
        resetResultTable();
        alert("Server error");
    });
    ajax.send(params);
    loader.classList.remove("hidden");

}

let ajaxCount = 0;
let idEnableSelect;
function renderProjectManagers() {

    idEnableSelect = setInterval(enableSelect, 300);

    let loader = document.getElementById("loader-container");
    let PMNameSelect = document.getElementById("project-manager-name");

    let ajax1 = new XMLHttpRequest();
    ajax1.open("POST", "/Action/GetProjectManager");
    ajax1.addEventListener("load", function (e) {
        loader.classList.add("hidden");
        let json = e.currentTarget.responseText;
        if (json == '0' || json == '' || json == null) {
            alert("No Manager");
            return;
        }
        json = JSON.parse(json);

        PMNameSelect.innerHTML = "";
        let html = `<option value="-1">Select</option>`;
        json.forEach(function (element, index) {
            html += `<option  value="${element.PMOwner}">${element.PMOwner}</option>`;

        });
        PMNameSelect.innerHTML = html;
        ajaxCount++;
    });
    ajax1.addEventListener("error", function (e) {
        ajaxCount++;
        alert("Server error");
    });
    ajax1.send();
    loader.classList.remove("hidden");
}

function renderProjects() {
    idEnableSelect = setInterval(enableSelect, 300);

    let loader = document.getElementById("loader-container");
    let ajax = new XMLHttpRequest();
    let projectNameselect = document.getElementById("project-name-select");

    projectNameselect.classList.add("no-pointer");

    ajax.open("POST", "/Action/GetProjects");
    ajax.addEventListener("load", function (e) {
        loader.classList.add("hidden");
        let json = e.currentTarget.responseText;
        if (json == '0' || json == '' || json == null) {
            alert("No Projects available");
            return;
        }
        json = JSON.parse(json);
        oProjectsJSON = json;
        
        ajaxCount++;
    });
    ajax.addEventListener("error", function (e) {
        ajaxCount++;
        alert("Server error");
    });
    ajax.send();
}

function loadAllProjectsForTrimming() {
    idEnableSelect = setInterval(enableSelect, 300);
    let loader = document.getElementById("loader-container");
    let projectNameselect = document.getElementById("project-name-select");
    projectNameselect.classList.add("no-pointer");
    projectNameselect.innerHTML = '';
    let html = `<option value="-1">Select</option>`;
    oProjectsJSON.forEach(function (element, index) {
        html += `<option for="${element.PMOwner}" value="${element.ProjectID}">${element.ProjectName}</option>`;
    });
    projectNameselect.innerHTML += html;
}

function removeAllProjects() {
    let projectNameselect = document.getElementById("project-name-select");
    projectNameselect.innerHTML = '';
    let html = `<option>Please select a PM to proceed</option>`;
    projectNameselect.innerHTML += html;
}

function enableSelect() {
    if (ajaxCount == 2) {
        clearInterval(idEnableSelect);
        let projectNameselect = document.getElementById("project-name-select");
        projectNameselect.classList.remove("no-pointer");

        let PMNameSelect = document.getElementById("project-manager-name");
        PMNameSelect.classList.remove("no-pointer");
    }
}

function fillRequirementChecklistTable() {

    let loader = document.getElementById("loader-container");
    let tableContainer = document.querySelector("#requirement-checklist");
    tableContainer.innerHTML = "";

    let _auditType = document.getElementById("audit-type-select").value;

    document.getElementById("creator-row").classList.add("hidden");
    document.getElementById("updaters-row").classList.add("hidden");
    document.getElementById("reviewers-bag").value = "";
    document.getElementById("url-bag").value = "";

    let ajax = new XMLHttpRequest();
    ajax.open("POST", "/Action/GetCheckList");
    ajax.setRequestHeader('Content-Type', 'application/json');

    ajax.addEventListener("load", function (e) {
        loader.classList.add("hidden");
        let json = e.currentTarget.responseText;
        if (json == '0' || json == '' || json == null) {
            resetResultTable();
            let tableContainer = document.querySelector("#requirement-checklist");
            tableContainer.innerHTML = "<h6>No Data</h6>";
            let formBtn = document.querySelector("#form-btn-container");
            formBtn.classList.add("hidden");
            document.getElementById("buttonRow").classList.add("hidden");

            return;
        }
        renderCheckListNewTable(JSON.parse(json));
    });
    ajax.addEventListener("error", function (e) {
        resetResultTable();
        alert("Error GetCheckList");
    });

    ajax.send(JSON.stringify({ auditType: _auditType }));
    loader.classList.remove("hidden");
    document.querySelector("#submit-audit-btn").innerText = "Submit";
    let formBtn = document.querySelector("#form-btn-container");
    formBtn.classList.remove("hidden");
    document.getElementById("buttonRow").classList.remove("hidden");

}

function renderCheckListNewTable(checklist) {
    document.querySelector("#sign-off-container").innerHTML = "";

    let thead = `<thead>
            <tr>
                <th>Checklist</th>
                <th colspan="3" class="text-center">Yes/No/NA</th>
                <th>Comment</th>
            </tr>
    </thead>`;

    let tableContainer = document.querySelector("#requirement-checklist");
    tableContainer.innerHTML = "";

    if (checklist == '' || checklist == undefined) {
        table.innerHTML = "<tr><td>No checklist available</td><tr/>";
    }

    let tr, td, textarea, fieldset, legend;
    let counter = 0;
    $.each(checklist, function (key, value) {

        fieldset = document.createElement('FIELDSET');
        legend = document.createElement('legend');
        legend.classList.add("table-legend");
        legend.appendChild(document.createTextNode(key));
        legend.classList.add("text-center");
        fieldset.appendChild(legend);

        table = document.createElement('table');
        table.classList.add("table", "table-bordered", "table-responsive", "table-hovered");

        table.innerHTML = thead;
        tbody = document.createElement('tbody');

        value.forEach(function (element, index) {

            tr = document.createElement("tr");
            td = document.createElement("td");
            td.style.width = "750px";
            td.setAttribute("name", `checklist-${counter}-${index}`);
            td.innerHTML = element;
            tr.appendChild(td);


            td = document.createElement("td");
            td.innerHTML = `<label class="radio"><input type="radio" for="yes" name="status-radio-${counter}-${index}">
                                  <span>Yes</span>
                            </label>`;
            tr.appendChild(td);

            td = document.createElement("td");
            td.innerHTML = `<label class="radio"><input type="radio" for="no" checked="checked" name="status-radio-${counter}-${index}">
                                  <span>No</span>
                            </label>`;
            tr.appendChild(td);

            td = document.createElement("td");
            td.innerHTML = `<label class="radio"><input type="radio" for="na" name="status-radio-${counter}-${index}">
                                  <span>NA</span>
                            </label>`;
            tr.appendChild(td);

            td = document.createElement("td");
            textarea = document.createElement("textarea");
            textarea.setAttribute("name", `comment-${counter}-${index}`);
            td.appendChild(textarea);
            tr.appendChild(td);

            tbody.appendChild(tr);
        });
        counter++;
        table.appendChild(tbody);
        fieldset.appendChild(table);

        tableContainer.appendChild(fieldset);
    });

    document.getElementById("p0-observations").value = "";
    document.getElementById("p1-observations").value = "";
}

function decodeHTMLEntities(htmlString) {
    let html = $('<textarea />').html(htmlString);
    let sConvertedString = html.text();
    sConvertedString = unescape(sConvertedString);
    return sConvertedString;
}


function renderCheckListTable(checklist, isApproved) {
    signOffRender(isApproved);

    let thead = `<thead>
            <tr>
                <th>Checklist</th>
                <th colspan="3" class="text-center">Yes/No/NA</th>
                <th>Comment</th>
            </tr>
    </thead>`;

    let tableContainer = document.querySelector("#requirement-checklist");
    tableContainer.innerHTML = "<hr/>";

    if (checklist == '' || checklist == undefined) {
        table.innerHTML = "<tr><td>No checklist available</td><tr/>";
    }

    let tr, td, textarea, fieldset, legend;
    let counter = 0;

    checklist.forEach(function (element, index) {

        fieldset = document.createElement('FIELDSET');
        legend = document.createElement('legend');
        legend.appendChild(document.createTextNode(element[0]));
        legend.classList.add("table-legend");
        legend.classList.add("text-center");
        fieldset.appendChild(legend);

        table = document.createElement('table');
        table.classList.add("table", "table-bordered", "table-responsive", "table-hovered");
        table.innerHTML = thead;
        tbody = document.createElement('tbody');

        element[1].forEach(function (ele, ind) {

            tr = document.createElement("tr");
            td = document.createElement("td");
            td.style.width = "750px";
            td.setAttribute("name", `checklist-${counter}-${ind}`);
            td.innerHTML = decodeHTMLEntities(ele["observations"]);
            tr.appendChild(td);

            let yes, no, na;
            if (ele["yesno"] == "Yes") {
                yes = "checked";
                no = "";
                na = "";
            }
            else if (ele["yesno"] == "No") {
                yes = "";
                no = "checked";
                na = "";
            }
            else {
                yes = "";
                no = "";
                na = "checked";
            }

            td = document.createElement("td");
            td.innerHTML = `<label class="radio"><input type="radio" for="yes" name="status-radio-${counter}-${ind}" ${yes}>
                                  <span>Yes</span>
                                </label>`;
            tr.appendChild(td);

            td = document.createElement("td");
            td.innerHTML = `<label class="radio"><input type="radio" for="no"  name="status-radio-${counter}-${ind}" ${no}>
                                  <span>No</span>
                            </label>`;
            tr.appendChild(td);

            td = document.createElement("td");
            td.innerHTML = `<label class="radio"><input type="radio" for="na"  name="status-radio-${counter}-${ind}" ${na}>
                                  <span>NA</span>
                            </label>`;
            tr.appendChild(td);

            td = document.createElement("td");
            textarea = document.createElement("textarea");
            textarea.setAttribute("name", `comment-${counter}-${ind}`);
            textarea.innerText = decodeHTMLEntities(ele["comment"]);
            td.appendChild(textarea);
            tr.appendChild(td);

            tbody.appendChild(tr);
        });
        counter++;
        table.appendChild(tbody);
        fieldset.appendChild(table);

        tableContainer.appendChild(fieldset);
    });
  
}

function signOffRender(isApproved) {
    let container = document.querySelector("#sign-off-container");
    let yes, no;
    if (isApproved == 1 || isApproved == -1) {
        yes = "checked";
        no = "";
    }
    else if (isApproved == 0) {
        no = "checked";
        yes = "";
    }
    let approveText = "Approve", rejectText = "Reject";
    if (isApproved == 1) {
        approveText = "Approved";
        rejectText = "Reject";
    }
    else if (isApproved == -1) {
        rejectText = "Rejected";
        approveText = "Approve";
    }


    let html = `<div class="col-md-6">
        <div class="row">
            <div class="col-md-4">
                Signed Off By Audit Tool:
            </div>
            <div class="col-md-8">
                <label class="radio no-pointer">
                    <input type="radio" name="signoff" ${yes}/>
                    <span>Yes</span>
                </label>
                <label class="radio no-pointer">
                    <input type="radio" name="signoff" ${no}/>
                    <span>No</span>
                </label>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="row">
            <div class="col-md-4">
                Audit Request:
            </div>
            <div class="col-md-4">
                <button id="approve-btn" onclick="approveRejectAudit(1)" ><span>${approveText}</span></button>
            </div>
            <div class="col-md-4">
                <button id="reject-btn" onclick="approveRejectAudit(0)"><span>${rejectText}</span></button>
            </div>
        </div>
    </div>`;

    container.innerHTML = html;
}


function approveRejectAudit(flag) {

    let _pMId = document.getElementById("project-manager-name").value;
    let _projectId = document.getElementById("project-name-select").value;
    let _auditType = document.getElementById("audit-type-select").value;
    let auditDateTime = document.querySelector("#audit-select-box").value;
    let params = {
        'projectId': _projectId,
        'auditType': _auditType,
        'projectManagerId': _pMId,
        'dateTime': auditDateTime,
        'flag': flag
    }
    params = JSON.stringify(params);

    let approveBtn = document.getElementById("approve-btn");
    let rejectBtn = document.getElementById("reject-btn");

    let ajax = new XMLHttpRequest();
    ajax.open("POST", "/action/approveRejectAudit");
    ajax.setRequestHeader('Content-Type', 'application/json');
    ajax.addEventListener("load", function (e) {

        if (e.currentTarget.responseText == "1") {
            let radio = document.querySelectorAll("#sign-off-container input[name='signoff']");
            radio[0].checked = true;;

            if (flag == 1) {
                rejectBtn.classList.remove("no-pointer");
                approveBtn.firstChild.innerText = "Approved";
                rejectBtn.firstChild.innerText = "Reject";
            } else {
                approveBtn.classList.remove("no-pointer");
                approveBtn.firstChild.innerText = "Approve";
                rejectBtn.firstChild.innerText = "Rejected";
            }
        }
        else {
            approveBtn.classList.remove("no-pointer");
            rejectBtn.classList.remove("no-pointer");
            approveBtn.firstChild.innerText = "Approve";
            rejectBtn.firstChild.innerText = "Reject";
        }
    });
    ajax.upload.addEventListener("progress", function (e) {
        approveBtn.classList.add("no-pointer");
        rejectBtn.classList.add("no-pointer");
        if (flag == 1) {
            approveBtn.firstChild.innerText = "Waiting ...";
        } else {
            rejectBtn.firstChild.innerText = "Waiting ...";
        }
    });
    ajax.addEventListener("error", function (e) {
        approveBtn.classList.remove("no-pointer");
        rejectBtn.classList.remove("no-pointer");
        approveBtn.firstChild.innerText = "Approve";
        rejectBtn.firstChild.innerText = "Reject";
        alert("Error approveRejectAudit");
    });
    ajax.send(params);

}

var sGetEmailid = "/Account/GetEmailId";
var sEmailId = "";
$.ajax({
    url: sGetEmailid,
    contentType: "application/json; charset=utf-8",
    dataType: "json",
    success: populateEmail,
    error: function (a, b, error) { }
});

function populateEmail(email) {
    sEmailId = email;
    renderChecklist();
}

function submitAudit(sSaveOrSubmit) {
    let sReviewers = document.getElementById("reviewers-bag").value;
    let sUrl = document.getElementById("url-bag").value;
    let sReviewersSplitted = sReviewers.split(",");

    for (let iIteratorToValidateReviewers = 0; iIteratorToValidateReviewers < sReviewersSplitted.length; iIteratorToValidateReviewers++) {
        if (!sReviewersSplitted[iIteratorToValidateReviewers].includes("maqsoftware.com") && sReviewersSplitted[iIteratorToValidateReviewers] != "") {
            alert("Please enter valid email aliases of reviewers (only maqsoftware.com)");
            return;
        }
    }

    let loader = document.getElementById("loader-container");

    let _pMId = document.getElementById("project-manager-name").value;

    let _p0Observations = escape(document.getElementById("p0-observations").value);
    let _p1Observations = escape(document.getElementById("p1-observations").value);

    //_p0Observations = (_p0Observations.includes("'")) ? _p0Observations.replace("'","**Single**") : _p0Observations;
    //_p1Observations = (_p1Observations.includes("'")) ? _p1Observations.replace("'","**Single**") : _p1Observations;

    let _projectId = document.getElementById("project-name-select").value;
    let _auditType = document.getElementById("audit-type-select").value;


    if (_auditType == "-1") {
        alert("Choose audit type");
        return;
    }
    if (_projectId == "-1") {
        alert("Choose Project");
        return;
    }
    if (_pMId == "-1") {
        alert("Choose Project Manager");
        return;
    }

    let fieldset = document.querySelectorAll("#requirement-checklist fieldset");
    let counter = 0;
    let map = new Map();
    let flagForCommentsMandate = 0;
    let iYesCount = 0;
    let iNoCount = 0;
    let iAuditScore = 0;

    fieldset.forEach(function (element, ind) {
        let tableTitle = element.firstChild.innerText;
        let table = element.childNodes[1].querySelectorAll("tbody tr");

        let list = [];
        for (let index = 0; index < table.length; index++) {

            let rec = document.getElementsByName(`checklist-${counter}-${index}`)[0].innerHTML;

            let yesno = document.querySelector(`input[name='status-radio-${counter}-${index}']:checked`);
            yesno = yesno.getAttribute('for');

            let com = escape(document.getElementsByName(`comment-${counter}-${index}`)[0].value);

            if (rec == '' || rec == undefined) {
                rec = "NA";
            }
            if (yesno == '' || yesno == undefined) {
                yesno = "no";
            }
            if (com == '' || com == undefined) {
                com = "";
            }

            if (yesno === "na" && com === "") {
                alert("Comment section is mandatory when NA is selected");
                flagForCommentsMandate = 1;
                return;
            }

            if (yesno === "yes") {
                iYesCount++;
            }
            if (yesno === "no") {
                iNoCount++;
            }

            let obj = {};
            obj.observations = rec;
            obj.yesno = (yesno == "yes") ? "Yes" : (yesno == "no") ? "No" : "NA";
            obj.comment = com;
            list.push(obj);
        }

        counter++;
        map.set(tableTitle, list);
    });

    if (flagForCommentsMandate === 1) {
        return;
    }

    if (_p0Observations == '' || _p0Observations == undefined) {
        _p0Observations = "";
    }
    if (_p1Observations == '' || _p1Observations == undefined) {
        _p1Observations = "";
    }

    iAuditScore = iYesCount / (iYesCount + iNoCount);
    iAuditScore = Math.round(iAuditScore * 100) / 100;

    let params = {
        'projectId': _projectId,
        'auditType': _auditType,
        'projectManagerId': _pMId,
        'p0Observations': _p0Observations,
        'p1Observations': _p1Observations,
        'UpdatedBy': sEmailId,
        'list': JSON.stringify([...map]),
        'saveOrSubmit': sSaveOrSubmit,
        'reviewers': sReviewers,
        'auditScore': iAuditScore,
        'ProjectURL': sUrl
    }

    let auditDate = document.getElementById("audit-select-box").value;
    if (auditDate == "-1") {
        params.flag = "0";
    }
    else {
        params.flag = auditDate;
    }

    params = JSON.stringify(params);

    let url = "";
    url = "/Action/SubmitAudit";
    let ajax = new XMLHttpRequest();
    ajax.open("POST", url);
    ajax.setRequestHeader('Content-Type', 'application/json');

    ajax.addEventListener("load", function (e) {
        loader.classList.add("hidden");
        let data = e.currentTarget.responseText;
        if (data == '0') {
            alert("Error");
        }
        else {
            resetResultTable();
            if (sSaveOrSubmit === "Submit") {
                alert("Successfully submitted the audit");
            }
            else if (sSaveOrSubmit === "Save") {
                alert("Successfully saved the audit");
            }
        }
        document.querySelector("#sign-off-container").innerHTML = "";
    });
    ajax.addEventListener("error", function (e) {
        alert("Server error");
        document.querySelector("#sign-off-container").innerHTML = "";
    });
    ajax.send(params);
    loader.classList.remove("hidden");
}

function resetResultTable() {

    document.querySelector("#sign-off-container").innerHTML = "";
    let table = document.querySelector("#requirement-checklist");
    table.innerHTML = "";
    document.getElementById("p0-observations").value = "";
    document.getElementById("p1-observations").value = ""
    document.getElementById("form-btn-container").classList.add("hidden");
    document.getElementById("buttonRow").classList.add("hidden");
    document.getElementById("Bug-id-container").classList.add("hidden");

    let auditTypeSelectBox = document.querySelector("#audit-type-select");
    auditTypeSelectBox.selectedIndex = 0;

    let auditDateSelectBox = document.querySelector("#audit-select-box")
    auditDateSelectBox.innerHTML = "<option value='-1'>New Audit</option>";
    auditDateSelectBox.selectedIndex = 0;
}
