﻿document.getElementById("ApproveAudit").onclick = setComments;
document.getElementById("RejectAudit").onclick = setComments;

function setComments() {
    let sSetComments = "/Approval/SetComments";
    let sCommentsValue = document.getElementById("ReviewComments").value;
    $.ajax({
        url: sSetComments,
        method: "post",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({ comments: sCommentsValue }),
        dataType: "json",
        success: logSuccess,
        error: function (a, b, error) { }
    });
}

function logSuccess() {
    //console.log("success");

}