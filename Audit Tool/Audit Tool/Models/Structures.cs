﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Audit_Tool.Models
{
    public class Structures
    {
        public class SubmitStruct
        {
            public string projectId { get; set; }
            public string auditType { get; set; }
            public string projectManagerId { get; set; }
            public string p0Observations { get; set; }
            public string p1Observations { get; set; }
            public string updatedBy { get; set; }
            public string list { get; set; }
            public string flag { get; set; }
            public string saveOrSubmit { get; set; }
            public string reviewers { get; set; }
            public string auditScore { get; set; }
            public string projectURL { get; set; }
            public int submitStatus { get; set; }
            public string frequency { get; set; }
            public string poc { get; set; }
            public string reason { get; set; }
            public string comments { get; set; }
        }
    }
}