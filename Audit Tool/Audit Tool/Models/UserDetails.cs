﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Audit_Tool.Models
{
    public class UserDetails
    {
        private String _Name;
        private String _ID;

        public String Name
        {
            get
            {
                return _Name;
            }
            set
            {
                _Name = value;
            }
        }
        public String ID
        {
            get
            {
                return _ID;
            }
            set
            {
                _ID = value;
            }
        }
    }
}