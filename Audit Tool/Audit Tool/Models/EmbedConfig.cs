﻿using Microsoft.PowerBI.Api.V2.Models;
using System;
using System.Web.Mvc;

namespace Audit_Tool.Models
{
    public class EmbedConfig : Controller
    {
        public string Id { get; set; }

        public string EmbedUrl { get; set; }

        public string EmbedToken { get; set; }
        public string Token { get; set; }
        //public EmbedToken EmbedToken.Token { get; set; }
        //public EmbedToken EmbedToken.TokenId { get; set; }

        //public int MinutesToExpiration
        //{
        //    get
        //    {
        //        var minutesToExpiration = EmbedToken.Expiration.Value - DateTime.UtcNow;
        //        return (int)minutesToExpiration.TotalMinutes;
        //    }
        //}

        public bool? IsEffectiveIdentityRolesRequired { get; set; }

        public bool? IsEffectiveIdentityRequired { get; set; }

        public bool EnableRLS { get; set; }

        public string Username { get; set; }

        public string Roles { get; set; }

        public string ErrorMessage { get; internal set; }   
    }
}