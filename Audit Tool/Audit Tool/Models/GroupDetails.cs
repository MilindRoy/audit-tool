﻿using System;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Audit_Tool.Models
{
    public class GroupDetails
    {
        private String _GroupName;
        private String _GroupID;
        public JArray EventList;
        private string _Visibility;
        private String _EmailID;

        public String GroupName
        {
            get
            {
                return _GroupName;
            }
            set
            {
                _GroupName = value;
            }
        }

        public String GroupID
        {
            get
            {
                return _GroupID;
            }
            set
            {
                _GroupID = value;
            }
        }

        public String Visibility
        {
            get
            {
                return _Visibility;
            }
            set
            {
                _Visibility = value;
            }
        }
        public String EmailID
        {
            get
            {
                return _EmailID;
            }
            set
            {
                _EmailID = value;
            }
        }
        public bool Checked
        {
            get;
            set;
        }
    }
}