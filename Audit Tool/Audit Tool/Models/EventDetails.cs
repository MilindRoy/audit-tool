﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Audit_Tool.Models
{
    public class EventDetails
    {
        private String _EventName;
        private String _EventDescription;
        private String _StartDateTime;
        private String _EndDateTime;

        public String EventName
        {
            get
            {
                return _EventName;
            }
            set
            {
                _EventName = value;
            }
        }
        public String EventDescription
        {
            get
            {
                return _EventDescription;
            }
            set
            {
                _EventDescription = value;
            }
        }
        public String StartDateTime
        {
            get
            {
                return _StartDateTime;
            }
            set
            {
                _StartDateTime = value;
            }
        }
        public String EndDateTime
        {
            get
            {
                return _EndDateTime;
            }
            set
            {
                _EndDateTime = value;
            }
        }
    }
}