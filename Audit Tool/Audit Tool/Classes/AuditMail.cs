﻿using System;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Script.Serialization;
using Audit_Tool.Helpers;
using System.Globalization;

namespace Audit_Tool.Classes
{
    public class AuditMail
    {

        public bool SendMail(string receiver, string projectName, string projectManagerName, string auditType, string allObservations , string list, string date,string auditorEmail, string reviewersList, double auditScore)
        {
            receiver = "harshl@maqsoftware.com";
            var js = new JavaScriptSerializer();
            var dynJson = js.Deserialize<dynamic>(list);
            string subject = "[" + projectManagerName + "]: Audit Information for [" + projectName + "]";
            string heading = "Hello " + projectManagerName + ", <br/><br/>Audit Information for [" + projectName + "]";
            string approval = string.Format("<p><a href='{0}/Approval/Approve?ProjectName={1}&PMName={2}&AuditType={3}&Date={4}'>Approve the audit</a></p> <br />", Constants.AppURL, HttpUtility.UrlEncode(projectName), HttpUtility.UrlEncode(projectManagerName), HttpUtility.UrlEncode(auditType), HttpUtility.UrlEncode(date));
            string auditPara = "<br/>Audit Score: " + auditScore + "<br/>";

            //p0Observations = HttpUtility.UrlDecode(p0Observations, System.Text.Encoding.Default);
            //p1Observations = HttpUtility.UrlDecode(p1Observations, System.Text.Encoding.Default);

            //p0Observations = p0Observations.Replace("\n", "<br/>");
            //p1Observations = p1Observations.Replace("\n", "<br/>");
            //if (auditType == "Architecture")
            //{
            //    reviewersList += ", art@maqsoftware.com";
            //}
            string htmlBody = @"<html><body>" + heading + "<br/>Audit Type: " + auditType + "<br/>" + auditPara;
            htmlBody += approval;

           

            foreach (var item in dynJson)
            {
                try
                {
                    int count = 1;
                    foreach (var checklists in item)
                    {
                        string table = "";
                        if (count == 1)
                        {
                            string legend = "<br/><fieldset><legend style='font-size:18px;'><b>" + checklists + "</b></legend>";
                            table += legend;
                            table += @"<table border='1'><thead><tr style='background-color:#0070C0; color:#fff '><th>CheckList</th><th>Yes/No/NA</th><th>Comments</th></tr></thead>";
                            count = 0;
                        }
                        else
                        {
                            table += "<tbody>";
                            foreach (var checklist in checklists)
                            {
                                string observations = checklist["observations"];
                                string yesno = checklist["yesno"];
                                string comment = HttpUtility.UrlDecode(checklist["comment"], System.Text.Encoding.Default).Replace("\n", "<br/>");
                                string tr = @"<tr><td> " + observations + "</td><td> " + yesno + "</td><td> " + comment + "</td> </tr>";
                                table += tr;
                            }
                            table += "</tbody></table></fieldset>";
                        }
                        htmlBody += table;
                    }
                }
                catch (Exception ex)
                {
                    ExceptionLogging.SendErrorToText(ex);
                    return false;
                }
            }
            if (auditType == "Architecture")
            {
                htmlBody += @"<hr/><br/>
                <table border='1' style='width:100%'><thead><tr style='background-color:#0070C0; color:#fff;'><th>Observations</th><th>Severity</th><th>Area</th></tr></thead><tbody>" + allObservations + "</tbody></table>";
            }
            else
            {
                htmlBody += @"<hr/><br/>
                <table border='1' style='width:100%'><thead><tr style='background-color:#0070C0; color:#fff;'><th>P0 Observations</th><th>P1 Observations</th></tr></thead><tbody>" + allObservations + "</tbody></table>";
            }
            string signature = @"<br/><br/><span style='color:#5193D4'>Best Regards,<br/>Audit Team</span><br/>";
            htmlBody += (signature + "</body></html>");

            SendMailSMTP(receiver, htmlBody, subject, reviewersList);
            
            // sendEMailThroughOUTLOOK(receiver, htmlBody,subject);
            return true;
        }


        public void SendMailSMTP(string receiverEmail, string htmlBody, string subject,string reviewersList)
        {

            using (SmtpClient smtpServer = new SmtpClient(Constants.SMTPServerName))
            {
                smtpServer.Host = Constants.SMTPServerName;
                smtpServer.Port = Convert.ToInt16(Constants.SMTPPort, CultureInfo.InvariantCulture);
                smtpServer.Credentials = new NetworkCredential(Constants.EmailSenderID, Constants.EmailSenderPassword);
                smtpServer.EnableSsl = Convert.ToBoolean(Constants.EmailEnableSSL, CultureInfo.InvariantCulture);
                MailMessage mail = new MailMessage(Constants.EmailSenderID, receiverEmail, subject, htmlBody);
                mail.IsBodyHtml = true;
                if (reviewersList != "")
                {
                    if (!reviewersList.Contains("meghad@maqsoftware.com") && receiverEmail!= "meghad@maqsoftware.com" )
                    {
                        //reviewersList+= ", meghad@maqsoftware.com";
                    }
                    mail.CC.Add(reviewersList);
                }
                else
                {
                    if (receiverEmail != "meghad@maqsoftware.com")
                    {
                      //mail.CC.Add("meghad@maqsoftware.com");
                    }
                }
            
                smtpServer.Send(mail);
            }
        }

    }
}