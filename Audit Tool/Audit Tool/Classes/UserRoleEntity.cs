﻿/* --------------------------------------------------------------------------------------
   Author    | Date of Creation/| Description
             | Last Modified    |
   --------------------------------------------------------------------------------------
   Deepak G   | 10/01/2019       | Table entity to get/set values from UserRole table storage
   --------------------------------------------------------------------------------------
 */
namespace Audit_Tool.Controllers
{
    using Microsoft.Azure.CosmosDB.Table;

    public class UserRoleEntity : TableEntity
    {
        //Constructor to define parition key and row Key
        public UserRoleEntity(string partition, string row)
        {
            this.PartitionKey = partition;
            this.RowKey = row;
        }

        //Constructor to initialize
        public UserRoleEntity()
        {

        }
        // security group of user list
        public string SecurityGroupID { get; set; }

        // list of action names comma seperated
        public string HasAccessOn { get; set; }
    }
}