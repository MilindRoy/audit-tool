using Audit_Tool.Helpers;
using Microsoft.Identity.Client;
using Microsoft.Owin.Security.Notifications;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
namespace Audit_Tool.Controllers
{

    public class GraphAPIHelper
    {
        /// <summary>
        /// uses graph api to get user list present in the security group
        /// </summary>
        /// <param name="accessToken">graph api access token</param>
        /// <returns>list of members present in the group</returns>
        public static string GetUserList(string accessToken, string graphAPIEndPointURL)
        {

            // store users mail 
            List<string> userList = new List<string>();

            // make get request to graph api to get list of users present in given security group
            using ( HttpClientHandler handler = new HttpClientHandler())
            {
                using (HttpClient clientGraphAPI = new HttpClient(handler))
                {
                    clientGraphAPI.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
                    HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "relativeAddress");
                    HttpResponseMessage message = clientGraphAPI.GetAsync(graphAPIEndPointURL).Result;

                    string result = message.Content.ReadAsStringAsync().Result;

                    dynamic formattedJson = JObject.Parse(result);

                    foreach (dynamic value in formattedJson[Constants.GraphJsonValueNode])
                    {
                        string dataType = value[Constants.GraphAPIDataTypeKey].ToString();

                        if (string.Equals(dataType, Constants.GraphAPIDataTypeValue))
                        {
                            string mailID = value[Constants.GraphJsonMailNode].ToString().ToLower();

                            // add all mail id in the global list
                            userList.Add(mailID);
                        }

                    }
                }
            }

            // return list of user comma seperated
            return string.Join(Constants.Comma.ToString(), userList.ToArray());
        }
    }
}
