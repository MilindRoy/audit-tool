﻿namespace Audit_Tool.Classes
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data.SqlClient;
    using System.Globalization;
    using System.Net;
    using System.Web;
    using System.Web.Script.Serialization;
    using Audit_Tool.Helpers;
    using Audit_Tool.Models;
    using Microsoft.IdentityModel.Clients.ActiveDirectory;

    public class SQLData
    {
        private string tenantId = ConfigurationManager.AppSettings["tenantId"];
        private string clientId = ConfigurationManager.AppSettings["clientId"];
        private string clientSecret = ConfigurationManager.AppSettings["clientSecret"];
        //private string clientId = ConfigurationManager.AppSettings["ida:clientId"];
        //private string clientSecret = ConfigurationManager.AppSettings["ida:Appkey"];
        private string dbServer = ConfigurationManager.AppSettings["dbServer"];
        private string dbName = ConfigurationManager.AppSettings["dbName"];
        private string instance = ConfigurationManager.AppSettings["instance"];
        private string resourceId = ConfigurationManager.AppSettings["resourceId"];
        private JavaScriptSerializer js = new JavaScriptSerializer();

        /// <summary>
        /// A common method to return project details based on parameter input.
        /// </summary>
        /// <param name="getResults">A string indication which query to run out of the two.</param>
        /// <returns>string.</returns>
        public string CommonQuery(string getResults)
        {
            string query = string.Empty;
            string data;
            if (getResults == "GetProjects")
            {
                query = Constants.ProjectDetails;
            }
            else if (getResults == "GetProjectManager")
            {
                query = Constants.ProjectManager;
            }

            try
            {
                data = this.Select(query);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                data = string.Empty;
            }

            return data;
        }

        //public string RetrieveReports(string projectId)
        //{
        //    string query = string.Format(Constants.ProjectReports, int.Parse(projectId));
        //    string data;

        //    try
        //    {
        //        data = this.Select2(query);
        //    }
        //    catch (Exception ex)
        //    {
        //        ExceptionLogging.SendErrorToText(ex);
        //        data = string.Empty;
        //    }

        //    return data;
        //}

        /// <summary>
        /// This will return a string containing all the email ID's 
        /// </summary>
        /// <returns>A string containing all the PM email ID's in Json Object.</returns>
        public string GetPMEmails()
        {
            string query = string.Empty;
            string data;

            query = Constants.AllPMEmail;

            try
            {
                data = this.Select(query);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                data = string.Empty;
            }

            return data;
        }

        /// <summary>
        /// Check if projects exist for the combination of the parameters.
        /// </summary>
        /// <param name="projectId">Project ID.</param>
        /// <param name="projectManagerId">Project manager name.</param>
        /// <returns>boolean value to confirm if projects exist for parameter combination.</returns>
        public bool CheckPMAndProject(string projectId, string projectManagerId)
        {
            string query = string.Format(Constants.ProjectCount, projectManagerId, projectId);
            try
            {
                string data = this.Select(query);
                if (int.Parse(data) >= 1)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
            }

            return false;
        }

        /// <summary>
        /// Returns links to bug ID's once the audit is approved by project managers.
        /// </summary>
        /// <param name="projectName">Name of project.</param>
        /// <param name="pMName">Project manager name.</param>
        /// <param name="auditType">Audit type.</param>
        /// <param name="date">Date and time on which audit was created.</param>
        /// <returns>Array of string.</returns>
        public string[] RetrieveBugs(string projectName, string pMName, string auditType, string date)
        {
            string query = string.Format(Constants.ProjectBugs, pMName, projectName, auditType, date);
            string[] bugs = new string[4];
            try
            {
                string data = this.Select(query);
                var dynJson = this.js.Deserialize<dynamic>(data);
                bugs[0] = dynJson[0]["P0BugId"].ToString();
                bugs[1] = HttpUtility.UrlDecode(dynJson[0]["P0BugLink"]);
                bugs[2] = dynJson[0]["P1BugId"].ToString();
                bugs[3] = HttpUtility.UrlDecode(dynJson[0]["P1BugLink"]);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                bugs[0] = string.Empty;
                bugs[1] = string.Empty;
                bugs[2] = string.Empty;
                bugs[3] = string.Empty;
            }

            return bugs;
        }

        public Dictionary<string, string> RetrievePrevAuditData2(string projectName, string pMName, string auditType, string date)
        {
            string query = string.Format(Constants.AuditExtraInfo, pMName, projectName, auditType, date);
            string frequency = string.Empty, poc = string.Empty, reason = string.Empty, comments = string.Empty;
            try
            {
                string data = this.Select(query);
                var dynJson = this.js.Deserialize<dynamic>(data);
                if (data.Contains("Frequency"))
                {
                    frequency += dynJson[0]["Frequency"];
                }
                else
                {
                    frequency = string.Empty;
                }

                if (data.Contains("POC"))
                {
                    poc += dynJson[0]["POC"];
                }
                else
                {
                    poc = string.Empty;
                }

                if (data.Contains("Reason"))
                {
                    reason += dynJson[0]["Reason"];
                }
                else
                {
                    reason = string.Empty;
                }

                if (data.Contains("Comments"))
                {
                    comments += dynJson[0]["Comments"];
                }
                else
                {
                    comments = string.Empty;
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);

                // reviewers = sbReviews.ToString().Substring(0, sbReviews.Length - 2);
            }

            Dictionary<string, string> prevAud2 = new Dictionary<string, string>();
            prevAud2.Add("frequency-select", frequency);
            prevAud2.Add("poc-bag", poc);
            prevAud2.Add("reason-select", reason);
            prevAud2.Add("comments-bag", comments);
            return prevAud2;
        }

        public Dictionary<string, string> RetrievePrevAuditDataQueryString(string id)
        {
            string query = string.Format(Constants.AuditInfoQuery, id);
            string reviewers = string.Empty, auditScore = string.Empty, projectURL = string.Empty, updatedBy = string.Empty, activeReviewer = string.Empty, projectID = string.Empty;
            string comments = string.Empty, p0Observation = string.Empty, p1Observation = string.Empty, createdBy = string.Empty;
            try
            {
                string data = this.Select(query);
                var dynJson = this.js.Deserialize<dynamic>(data);
                if (data.Contains("AuditScore"))
                {
                    auditScore += dynJson[0]["AuditScore"];
                }
                else
                {
                    auditScore = string.Empty;
                }
                if (data.Contains("ActiveReviewer"))
                {
                    activeReviewer = dynJson[0]["ActiveReviewer"];
                }
                else
                {
                    activeReviewer = string.Empty;
                }
                if (data.Contains("ProjectURL"))
                {
                    projectURL += dynJson[0]["ProjectURL"];
                }
                else
                {
                    projectURL = string.Empty;
                }

                if (data.Contains("PMComments"))
                {
                    comments += dynJson[0]["PMComments"];
                }
                else
                {
                    comments = string.Empty;
                }

                if (data.Contains("CreatedBy"))
                {
                    createdBy += dynJson[0]["CreatedBy"];
                }
                else
                {
                    createdBy = string.Empty;
                }

                if (data.Contains("UpdatedBy"))
                {
                    updatedBy += dynJson[0]["UpdatedBy"];
                }
                else
                {
                    updatedBy = string.Empty;
                }

                if (data.Contains("Reviewers"))
                {
                    reviewers += dynJson[0]["Reviewers"];
                }
                else
                {
                    reviewers = string.Empty;
                }
                if (data.Contains("ProjectID"))
                {
                    projectID += dynJson[0]["ProjectID"];
                }
                else
                {
                    projectID = string.Empty;
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);

                // reviewers = sbReviews.ToString().Substring(0, sbReviews.Length - 2);
            }

            Dictionary<string, string> prevAud = new Dictionary<string, string>();
            prevAud.Add(Constants.activeReviewerBag, activeReviewer);
            prevAud.Add(Constants.scoreBag, auditScore);
            prevAud.Add(Constants.creatorBag, createdBy);
            prevAud.Add(Constants.reviewersBag, reviewers);
            prevAud.Add(Constants.urlBag, projectURL);
            prevAud.Add(Constants.updatersBag, updatedBy);
            prevAud.Add(Constants.comments, comments);
            prevAud.Add(Constants.p0Observation, p0Observation);
            prevAud.Add(Constants.p1Observation, p1Observation);
            prevAud.Add("ProjectID", projectID);
            return prevAud;
        }

        /// <summary>
        /// Returns the data of any previous audit to be displayed on the form in different fields.
        /// </summary>
        /// <param name="projectName">Name of project.</param>
        /// <param name="pMName">Project manager name.</param>
        /// <param name="auditType">Audit type.</param>
        /// <param name="date">Date and time on which audit was created.</param>
        /// <returns>A dictionary containing key value mapping.</returns>
        public Dictionary<string, string> RetrievePrevAuditData(string projectName, string pMName, string auditType, string date)
        {
            string query = string.Format(Constants.AuditInfo, pMName, projectName, auditType, date);
            string reviewers = string.Empty, auditScore = string.Empty, projectURL = string.Empty, updatedBy = string.Empty, activeReviewer = string.Empty, projectID = string.Empty;
            string comments = string.Empty, p0Observation = string.Empty, p1Observation = string.Empty, createdBy = string.Empty;
            try
            {
                string data = this.Select(query);
                var dynJson = this.js.Deserialize<dynamic>(data);
                if (data.Contains("AuditScore"))
                {
                    auditScore += dynJson[0]["AuditScore"];
                }
                else
                {
                    auditScore = string.Empty;
                }
                if (data.Contains("ActiveReviewer"))
                {
                    activeReviewer = dynJson[0]["ActiveReviewer"];
                }
                else
                {
                    activeReviewer = string.Empty;
                }
                if (data.Contains("ProjectURL"))
                {
                    projectURL += dynJson[0]["ProjectURL"];
                }
                else
                {
                    projectURL = string.Empty;
                }

                if (data.Contains("PMComments"))
                {
                    comments += dynJson[0]["PMComments"];
                }
                else
                {
                    comments = string.Empty;
                }

                if (data.Contains("CreatedBy"))
                {
                    createdBy += dynJson[0]["CreatedBy"];
                }
                else
                {
                    createdBy = string.Empty;
                }

                if (data.Contains("UpdatedBy"))
                {
                    updatedBy += dynJson[0]["UpdatedBy"];
                }
                else
                {
                    updatedBy = string.Empty;
                }

                if (data.Contains("Reviewers"))
                {
                    reviewers += dynJson[0]["Reviewers"];
                }
                else
                {
                    reviewers = string.Empty;
                }
                if (data.Contains("ProjectID"))
                {
                    projectID += dynJson[0]["ProjectID"];
                }
                else
                {
                    projectID = string.Empty;
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);

                // reviewers = sbReviews.ToString().Substring(0, sbReviews.Length - 2);
            }

            Dictionary<string, string> prevAud = new Dictionary<string, string>();
            prevAud.Add(Constants.activeReviewerBag, activeReviewer);
            prevAud.Add(Constants.scoreBag, auditScore);
            prevAud.Add(Constants.creatorBag, createdBy);
            prevAud.Add(Constants.reviewersBag, reviewers);
            prevAud.Add(Constants.urlBag, projectURL);
            prevAud.Add(Constants.updatersBag, updatedBy);
            prevAud.Add(Constants.comments, comments);
            prevAud.Add(Constants.p0Observation, p0Observation);
            prevAud.Add(Constants.p1Observation, p1Observation);
            prevAud.Add("ProjectID", projectID);
            return prevAud;
        }

        /// <summary>
        /// Returns previous observations for P0 and P1 comments.
        /// </summary>
        /// <param name="projectId">Project ID.</param>
        /// <param name="projectManagerId">Name of the project manager.</param>
        /// <param name="date">Date and time on which audit was created.</param>
        /// <returns>string containing previous P0 and P1 comments.</returns>
        public string GetPreviousObservations(string projectId, string projectManagerId, string auditType, string date, int Ismail)
        {
            // used for auto-save operations
            string newquery = string.Format(Constants.CountAutoSaveObservations, int.Parse(projectId), projectManagerId, date, 1);
            int count = int.Parse(this.Select(newquery));
            if (count > 0)
            {
                string updateIntoObservation = string.Format(Constants.ChangeOnFinalSubmission, int.Parse(projectId), projectManagerId, date, 1);
                this.InsertOrUpdate(updateIntoObservation);
            }

            // used for retrieving data while displaying in table
            string query = string.Format(Constants.PrevObservations, projectId, projectManagerId, date);
            string query2 = string.Format(Constants.PrevMulComments, projectId, projectManagerId, date);
            string prevObservation = string.Empty;
            try
            {
                if (auditType != "Architecture")
                {
                    string data = this.Select(query);
                    var dynJson = this.js.Deserialize<dynamic>(data);
                    for (int iteratorToPopulateObservations = 0; ; iteratorToPopulateObservations++)
                    {
                        prevObservation += "<tr><td>" + HttpUtility.UrlDecode(dynJson[iteratorToPopulateObservations]["p0"]) + "</td><td>" + HttpUtility.UrlDecode(dynJson[iteratorToPopulateObservations]["p1"]) + "</td></tr>";
                    }
                }
                else if ((auditType == "Architecture") && (Ismail==1))
                {
                    string data = this.Select(query2);
                    var dynJson = this.js.Deserialize<dynamic>(data);
                    for (int i = 0; ; i++)
                    {
                        if (dynJson[i]["Severity"] == "P0")
                        {
                            prevObservation += "<tr><td>" + HttpUtility.UrlDecode(dynJson[i]["P0Observation"]) + "</td><td>" + dynJson[i]["Severity"] + "</td><td>" + dynJson[i]["Area"] + "</td></tr>";
                        }
                        else
                        {
                            prevObservation += "<tr><td>" + HttpUtility.UrlDecode(dynJson[i]["P1Observation"]) + "</td><td>" + dynJson[i]["Severity"] + "</td><td>" + dynJson[i]["Area"] + "</td></tr>";
                        }
                    }
                }
                else if (auditType == "Architecture")
                {
                    int rownumber = 100;
                    string data = this.Select(query2);
                    var dynJson = this.js.Deserialize<dynamic>(data);
                    if (data != "")
                    {
                        for (int i = 0; ; i++)
                        {
                            if (data.Contains("Severity"))
                            {
                                if (dynJson[i]["Severity"] == "P0")
                                {
                                    prevObservation += string.Format(Constants.ObservationsHTML, rownumber, dynJson[i]["P0Observation"]);
                                    if (dynJson[i]["Area"] == "Security")
                                    {
                                        prevObservation += "<option value= 'Security' selected>Security</option>"
                                                                     + "<option value = 'Performance'>Performance</option>"
                                                                     + "<option value = 'Availability'>Availability</option>"
                                                                     + "<option value = 'Scalability'> Scalability </option>"
                                                                     + "<option value='Others'>Others</option>"
                                                                 + "</select>";
                                    }
                                    else if (dynJson[i]["Area"] == "Performance")
                                    {
                                        prevObservation += "<option value= 'Security'>Security</option>"
                                                                     + "<option value = 'Performance' selected>Performance</option>"
                                                                     + "<option value = 'Availability'>Availability</option>"
                                                                     + "<option value = 'Scalability'> Scalability </option>"
                                                                     + "<option value='Others'>Others</option>"
                                                                 + "</select>";
                                    }
                                    else if (dynJson[i]["Area"] == "Availability")
                                    {
                                        prevObservation += "<option value= 'Security'>Security</option>"
                                                                     + "<option value = 'Performance'>Performance</option>"
                                                                     + "<option value = 'Availability' selected>Availability</option>"
                                                                     + "<option value = 'Scalability'> Scalability </option>"
                                                                     + "<option value='Others'>Others</option>"
                                                                 + "</select>";
                                    }
                                    else if (dynJson[i]["Area"] == "Scalability")
                                    {
                                        prevObservation += "<option value= 'Security'>Security</option>"
                                                                     + "<option value = 'Performance'>Performance</option>"
                                                                     + "<option value = 'Availability'>Availability</option>"
                                                                     + "<option value = 'Scalability' selected> Scalability </option>"
                                                                     + "<option value='Others'>Others</option>"
                                                                 + "</select>";
                                    }
                                    else
                                    {
                                        prevObservation += "<option value= 'Security'>Security</option>"
                                                                     + "<option value = 'Performance'>Performance</option>"
                                                                     + "<option value = 'Availability'>Availability</option>"
                                                                     + "<option value = 'Scalability'> Scalability </option>"
                                                                     + "<option value='Others' selected>Others</option>"
                                                                 + "</select>";
                                    }

                                    prevObservation += "</div>"
                                                         + "</div><div class='col-md-1'>"
                                                             + "<div class='form-group'>"
                                                                 + "<button type = 'button' name='remove' id='" + rownumber + "' class ='btn btn-danger btn_remove'>X</button>"
                                                             + "</div>"
                                                         + "</div>"
                                                     + "</div>";
                                }
                                else
                                {
                                    prevObservation += string.Format(Constants.ObservationsHTML, rownumber, dynJson[i]["P1Observation"]);

                                    if (dynJson[i]["Area"] == "Security")
                                    {
                                        prevObservation += "<option value= 'Security' selected>Security</option>"
                                                                     + "<option value = 'Performance'>Performance</option>"
                                                                     + "<option value = 'Availability'>Availability</option>"
                                                                     + "<option value = 'Scalability'> Scalability </option>"
                                                                     + "<option value='Others'>Others</option>"
                                                                 + "</select>";
                                    }
                                    else if (dynJson[i]["Area"] == "Performance")
                                    {
                                        prevObservation += "<option value= 'Security'>Security</option>"
                                                                     + "<option value = 'Performance' selected>Performance</option>"
                                                                     + "<option value = 'Availability'>Availability</option>"
                                                                     + "<option value = 'Scalability'> Scalability </option>"
                                                                     + "<option value='Others'>Others</option>"
                                                                 + "</select>";
                                    }
                                    else if (dynJson[i]["Area"] == "Availability")
                                    {
                                        prevObservation += "<option value= 'Security'>Security</option>"
                                                                     + "<option value = 'Performance'>Performance</option>"
                                                                     + "<option value = 'Availability' selected>Availability</option>"
                                                                     + "<option value = 'Scalability'> Scalability </option>"
                                                                     + "<option value='Others'>Others</option>"
                                                                 + "</select>";
                                    }
                                    else if (dynJson[i]["Area"] == "Scalability")
                                    {
                                        prevObservation += "<option value= 'Security'>Security</option>"
                                                                     + "<option value = 'Performance'>Performance</option>"
                                                                     + "<option value = 'Availability'>Availability</option>"
                                                                     + "<option value = 'Scalability' selected> Scalability </option>"
                                                                     + "<option value='Others'>Others</option>"
                                                                 + "</select>";
                                    }
                                    else
                                    {
                                        prevObservation += "<option value= 'Security'>Security</option>"
                                                                     + "<option value = 'Performance'>Performance</option>"
                                                                     + "<option value = 'Availability'>Availability</option>"
                                                                     + "<option value = 'Scalability'> Scalability </option>"
                                                                     + "<option value='Others' selected>Others</option>"
                                                                 + "</select>";
                                    }

                                    prevObservation += "</div>"
                                                         + "</div><div class='col-md-1'>"
                                                             + "<div class='form-group'>"
                                                                 + "<button type = 'button' name='remove' id='" + rownumber + "' class ='btn btn-danger btn_remove'>X</button>"
                                                             + "</div>"
                                                         + "</div>"
                                                     + "</div>";
                                }
                            }
                            else
                            {
                                if (data.Contains("P0Observation") || data.Contains("P1Observation"))
                                {
                                    if (dynJson[i]["P0Observation"] != string.Empty)
                                    {
                                        prevObservation += string.Format(Constants.ObservationsHTML, rownumber, HttpUtility.UrlDecode(dynJson[i]["P0Observation"]));
                                        prevObservation += "<option value= 'Security'>Security</option>"
                                                                      + "<option value = 'Performance'>Performance</option>"
                                                                      + "<option value = 'Availability'>Availability</option>"
                                                                      + "<option value = 'Scalability'> Scalability </option>"
                                                                      + "<option value='Others' selected>Others</option>"
                                                                  + "</select>";

                                        prevObservation += "</div>"
                                                            + "</div><div class='col-md-1'>"
                                                                + "<div class='form-group'>"
                                                                    + "<button type = 'button' name='remove' id='" + rownumber + "' class ='btn btn-danger btn_remove'>X</button>"
                                                                + "</div>"
                                                            + "</div>"
                                                        + "</div>";
                                    }

                                    if (dynJson[i]["P1Observation"] != string.Empty)
                                    {
                                        prevObservation += string.Format(Constants.ObservationsHTML, rownumber, HttpUtility.UrlDecode(dynJson[i]["P1Observation"]));
                                        prevObservation += "<option value= 'Security'>Security</option>"
                                                                          + "<option value = 'Performance'>Performance</option>"
                                                                          + "<option value = 'Availability'>Availability</option>"
                                                                          + "<option value = 'Scalability'> Scalability </option>"
                                                                          + "<option value='Others' selected>Others</option>"
                                                                      + "</select>";
                                        prevObservation += "</div>"
                                                            + "</div><div class='col-md-1'>"
                                                                + "<div class='form-group'>"
                                                                    + "<button type = 'button' name='remove' id='" + rownumber + "' class ='btn btn-danger btn_remove'>X</button>"
                                                                + "</div>"
                                                            + "</div>"
                                                        + "</div>";
                                    }
                                }
                            }

                            rownumber--;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
            }

            return prevObservation;
        }

        /// <summary>
        /// Fetches the data of submit status column form database using the four parameters.
        /// </summary>
        /// <param name="projectName">Name of the project.</param>
        /// <param name="pMName">Project Manager Name.</param>
        /// <param name="auditType">Type of audit.</param>
        /// <param name="date">Date when the audit was created.</param>
        /// <returns>boolean value.</returns>
        public bool RetrieveSubmitStatus(string projectName, string pMName, string auditType, string date)
        {
            string query = string.Format(Constants.SubmittedOrNot, pMName, projectName, auditType, date);
            string data = this.Select(query);
            var dynJson = this.js.Deserialize<dynamic>(data);
            bool flag = dynJson[0]["SubmitStatus"];
            return flag;
        }

        /// <summary>
        /// P0 and P1 comments are sent from here to Approval controller.
        /// </summary>
        /// <param name="projectName">Project name.</param>
        /// <param name="pMName">Project manager name.</param>
        /// <param name="auditType">Type of audit</param>
        /// <param name="date">Date and time on which the audit was created.</param>
        /// <returns>Array of string containing P0 and P1 comments.</returns>
        public string[] GetAuditComments(string projectName, string pMName, string auditType, string date)
        {
            string query = string.Format(Constants.PrevObservForApproval, pMName, projectName, auditType, date);
            string[] auditComments = new string[2];

            try
            {
                string data = this.Select(query);
                var dynJson = this.js.Deserialize<dynamic>(data);
                auditComments[0] = dynJson[0]["P0Observation"];
                auditComments[1] = dynJson[0]["P1Observation"];
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                auditComments[0] = string.Empty;
                auditComments[1] = string.Empty;
            }

            return auditComments;
        }

        /// <summary>
        /// Returns token required for vso authentication.
        /// </summary>
        /// <param name="projectName">Name of the project.</param>
        /// <returns>Array of string.</returns>
        public string[] GetVSOToken(string projectName)
        {
            string query = string.Format(Constants.VSOToken, projectName);
            string[] vso = new string[3];
            try
            {
                string data = this.Select(query);
                var dynJson = this.js.Deserialize<dynamic>(data);
                vso[0] = dynJson[0]["PassWord"];
                vso[1] = dynJson[0]["VsoPath"];
                vso[2] = dynJson[0]["AreaPath"];

                if (vso[0].Length != 52)
                {
                    vso[0] = string.Empty;
                    vso[1] = string.Empty;
                    vso[2] = string.Empty;
                }
                else
                {
                    vso[1] = (vso[1][vso[1].Length - 1] != '/') ? vso[1] + '/' : vso[1];
                    vso[2] = vso[2].Split('/')[0];
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                vso[0] = string.Empty;
                vso[1] = string.Empty;
                vso[2] = string.Empty;
            }

            return vso;
        }

        /// <summary>
        /// Updates and stores the bugs.
        /// </summary>
        /// <param name="projectName">Project name.</param>
        /// <param name="pMName">Project manager name.</param>
        /// <param name="auditType">Type of audit.</param>
        /// <param name="createdBy">Creator name.</param>
        /// <param name="date">Date on which the audit was created.</param>
        /// <param name="typeOfBug">Type of bug.</param>
        /// <param name="createdBugId">Id of the created bug.</param>
        /// <param name="createdBugURL">Url of the created bug.</param>
        public void StoreBugs(string projectName, string pMName, string auditType, string createdBy, string date, int typeOfBug, int createdBugId, string createdBugURL)
        {
            try
            {
                string query = string.Empty;
                if (typeOfBug == 0)
                {
                    query = string.Format(Constants.UpdateP0Bug, createdBugId, createdBugURL, pMName, projectName, auditType, date);
                }
                else
                {
                    query = string.Format(Constants.UpdateP1Bug, createdBugId, createdBugURL, pMName, projectName, auditType, date);
                }

                string data = this.Select(query);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
            }
        }

        ///// <summary>
        ///// Inserts details into ProjectAudit.Observations table for the newly created audit.
        ///// </summary>
        ///// <param name="projectId">Project ID.</param>
        ///// <param name="projectManagerName">Project Manger name</param>
        ///// <param name="auditDate">Date and time on which the audit was created.</param>
        ///// <param name="comments">The P0 and P1 observations.</param>
        ///// <returns>A single character string indicating success or failure on insertion.</returns>
        //public bool InsertMultipleObservations(string projectId, string projectManagerName, string auditDate, string auditType, string[] comments, string[] severity, string[] area)
        //{
        //    try
        //    {
        //        if (auditDate == "0")
        //        {
        //            string query = string.Format(Constants.ProjectDate, int.Parse(projectId), projectManagerName, auditType);
        //            string data = this.Select(query);
        //            var dynJson = this.js.Deserialize<dynamic>(data);
        //            string date = dynJson[0]["DateTime"];
        //            string insertIntoObservation;
        //            for (int iteratorForObservations = 0; iteratorForObservations < comments.Length; iteratorForObservations++)
        //            {
        //                if (severity[iteratorForObservations].Equals("P0"))
        //                {
        //                    if (comments[iteratorForObservations] != string.Empty)
        //                    {
        //                        insertIntoObservation = string.Format(Constants.InsMulCommentsP0, int.Parse(projectId), projectManagerName, date, comments[iteratorForObservations], 0, area[iteratorForObservations], severity[iteratorForObservations]);
        //                        this.InsertOrUpdate(insertIntoObservation);
        //                    }
        //                }
        //                else
        //                {
        //                    if (comments[iteratorForObservations] != string.Empty)
        //                    {
        //                        insertIntoObservation = string.Format(Constants.InsMulCommentsP1, int.Parse(projectId), projectManagerName, date, comments[iteratorForObservations], 0, area[iteratorForObservations], severity[iteratorForObservations]);
        //                        this.InsertOrUpdate(insertIntoObservation);
        //                    }
        //                }
        //            }
        //        }
        //        else
        //        {
        //            string deleteFromObservation = string.Format(Constants.DelMulComments, int.Parse(projectId), projectManagerName, auditDate);
        //            this.InsertOrUpdate(deleteFromObservation);
        //            string insertIntoObservation;
        //            for (int iteratorForObservations = 0; iteratorForObservations < comments.Length; iteratorForObservations++)
        //            {
        //                if (severity[iteratorForObservations].Equals("P0"))
        //                {
        //                    if (comments[iteratorForObservations] != string.Empty)
        //                    {
        //                        insertIntoObservation = string.Format(Constants.InsMulCommentsP0, int.Parse(projectId), projectManagerName, auditDate, comments[iteratorForObservations], 0, area[iteratorForObservations], severity[iteratorForObservations]);
        //                        this.InsertOrUpdate(insertIntoObservation);
        //                    }
        //                }
        //                else
        //                {
        //                    if (comments[iteratorForObservations] != string.Empty)
        //                    {
        //                        insertIntoObservation = string.Format(Constants.InsMulCommentsP1, int.Parse(projectId), projectManagerName, auditDate, comments[iteratorForObservations], 0, area[iteratorForObservations], severity[iteratorForObservations]);
        //                        this.InsertOrUpdate(insertIntoObservation);
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ExceptionLogging.SendErrorToText(ex);
        //        return false;
        //    }

        //    return true;
        //}

        /// <summary>
        /// Inserts details generated after creation of a new audit in ProjectAudit.AuditTable.
        /// Sends email to project manager and reviewers.
        /// </summary>
        /// <param name="details">An instance of a strucuted class in a model named Structues.</param>
        /// <param name="sendMailOrNo">integer value indicating if we should send mail or not.</param>
        /// <returns>A single character string indicating success or failure on insertion.</returns>
        public string InsertAuditTable(Structures.SubmitStruct details, int sendMailOrNo, bool callAutoSave, string[] mulObservations, string[] severity, string[] area)
        {
            if (this.CheckPMAndProject(details.projectId, details.projectManagerId) == false)
            {
                return "0";
            }

            if (!details.reviewers.Contains(details.updatedBy))
            {
                if (details.reviewers.Contains("maqsoftware.com"))
                {
                    details.reviewers += ", " + details.updatedBy;
                }
                else
                {
                    details.reviewers = details.updatedBy;
                }
            }

            string query, mailCheckList;
            mailCheckList = details.list;
            details.list = WebUtility.HtmlEncode(details.list);
            details.p0Observations = WebUtility.HtmlEncode(details.p0Observations);
            details.p1Observations = WebUtility.HtmlEncode(details.p1Observations);

            query = string.Format(Constants.InsertAuditData, int.Parse(details.projectId), details.projectManagerId, details.auditType, details.list, details.p0Observations, details.p1Observations, details.updatedBy, details.updatedBy, 0, details.reviewers, double.Parse(details.auditScore), details.projectURL, details.submitStatus, details.frequency, details.poc, details.reason, details.comments);
            try
            {
                this.InsertOrUpdate(query);

                query = string.Format(Constants.PMNameAndEmail, details.projectManagerId);
                string data = this.Select(query);
                var dynJson = this.js.Deserialize<dynamic>(data);

                string pmName = dynJson[0]["PMName"];
                string receiverEmail = dynJson[0]["email"];
                string auditorEmail = details.updatedBy;

                query = string.Format(Constants.ProjectName, details.projectId);
                data = this.Select(query);
                dynJson = this.js.Deserialize<dynamic>(data);
                string projectName = dynJson[0]["ProjectName"];

                query = string.Format(Constants.ProjectDate, int.Parse(details.projectId), details.projectManagerId, details.auditType);
                data = this.Select(query);
                dynJson = this.js.Deserialize<dynamic>(data);
                string auditDate = dynJson[0]["DateTime"];
                string insertIntoObservation;

                if (details.auditType == "Architecture")
                {
                    for (int iteratorForObservations = 0; iteratorForObservations < mulObservations.Length; iteratorForObservations++)
                    {
                        if (severity[iteratorForObservations].Equals("P0"))
                        {
                            if (mulObservations[iteratorForObservations] != string.Empty)
                            {
                                insertIntoObservation = string.Format(Constants.InsMulCommentsP0, int.Parse(details.projectId), details.projectManagerId, auditDate, mulObservations[iteratorForObservations], 0, area[iteratorForObservations], severity[iteratorForObservations]);
                                this.InsertOrUpdate(insertIntoObservation);
                            }
                        }
                        else
                        {
                            if (mulObservations[iteratorForObservations] != string.Empty)
                            {
                                insertIntoObservation = string.Format(Constants.InsMulCommentsP1, int.Parse(details.projectId), details.projectManagerId, auditDate, mulObservations[iteratorForObservations], 0, area[iteratorForObservations], severity[iteratorForObservations]);
                                this.InsertOrUpdate(insertIntoObservation);
                            }
                        }
                    }
                }
                else
                {
                    if ((details.p0Observations != string.Empty) || (details.p1Observations != string.Empty))
                    {
                        if (callAutoSave == true)
                        {
                            insertIntoObservation = string.Format(Constants.InsertObservations, int.Parse(details.projectId), details.projectManagerId, auditDate, details.p0Observations, details.p1Observations, 1);
                            this.InsertOrUpdate(insertIntoObservation);
                        }
                        else
                        {
                            insertIntoObservation = string.Format(Constants.InsertObservations, int.Parse(details.projectId), details.projectManagerId, auditDate, details.p0Observations, details.p1Observations, 0);
                            this.InsertOrUpdate(insertIntoObservation);
                        }
                    }
                }

                if (sendMailOrNo == 1)
                {
                    AuditMail mail = new AuditMail();
                    string reviewersList = details.reviewers;
                    if(details.poc != "")
                    reviewersList += ", "+details.poc;
                    string allObservations = this.GetPreviousObservations(details.projectId, details.projectManagerId, details.auditType, auditDate, 1);
                    mail.SendMail(receiverEmail, projectName, pmName, details.auditType, allObservations, mailCheckList, auditDate, auditorEmail, reviewersList, double.Parse(details.auditScore));
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return "0";
            }

            return "1";
        }

        /// <summary>
        /// Updates details generated after a change in an existing audit in ProjectAudit.AuditTable.
        /// Sends email to project manager and reviewers.
        /// Inserts nwe P0 and P1 comments into ProjectAudit.Observations table durig updation.
        /// </summary>
        /// <param name="details">An instance of a strucuted class in a model named Structues.</param>
        /// <param name="date">Date of previously created audit.</param>
        /// <param name="sendMailOrNo">Integer value indicating if we should send mail or not.</param>
        /// <returns>A single character string indicating success or failure on insertion.</returns>
        public string UpdateAuditTable(Structures.SubmitStruct details, string date, int sendMailOrNo, bool callAutoSave, string[] mulObservations, string[] severity, string[] area)
        {
            if (this.CheckPMAndProject(details.projectId, details.projectManagerId) == false)
            {
                return "0";
            }

            if (!details.reviewers.Contains(details.updatedBy))
            {
                if (details.reviewers.Contains("maqsoftware.com"))
                {
                    details.reviewers += ", " + details.updatedBy;
                }
                else
                {
                    details.reviewers = details.updatedBy;
                }
            }

            string query, mailCheckList, insertIntoObservation, updateIntoObservation;
            mailCheckList = details.list;

            details.list = WebUtility.HtmlEncode(details.list);

            query = string.Format(Constants.UpdateAuditDetails, details.list, details.reviewers, details.updatedBy, details.updatedBy, double.Parse(details.auditScore), details.submitStatus, details.projectURL, details.frequency, details.poc, details.reason, details.comments, int.Parse(details.projectId), details.projectManagerId, details.auditType, date);

            if (details.auditType == "Architecture")
            {
                string deleteFromObservation = string.Format(Constants.DelMulComments, int.Parse(details.projectId), details.projectManagerId, date);
                this.InsertOrUpdate(deleteFromObservation);
                for (int iteratorForObservations = 0; iteratorForObservations < mulObservations.Length; iteratorForObservations++)
                {
                    if (severity[iteratorForObservations].Equals("P0"))
                    {
                        if (mulObservations[iteratorForObservations] != string.Empty)
                        {
                            insertIntoObservation = string.Format(Constants.InsMulCommentsP0, int.Parse(details.projectId), details.projectManagerId, date, mulObservations[iteratorForObservations], 0, area[iteratorForObservations], severity[iteratorForObservations]);
                            this.InsertOrUpdate(insertIntoObservation);
                        }
                    }
                    else
                    {
                        if (mulObservations[iteratorForObservations] != string.Empty)
                        {
                            insertIntoObservation = string.Format(Constants.InsMulCommentsP1, int.Parse(details.projectId), details.projectManagerId, date, mulObservations[iteratorForObservations], 0, area[iteratorForObservations], severity[iteratorForObservations]);
                            this.InsertOrUpdate(insertIntoObservation);
                        }
                    }
                }
            }
            else
            {
                if (callAutoSave == true)
                {
                    string newquery = string.Format(Constants.CountAutoSaveObservations, int.Parse(details.projectId), details.projectManagerId, date, 1);
                    int count = int.Parse(this.Select(newquery));
                    if (count > 0)
                    {
                        updateIntoObservation = string.Format(Constants.UpdateObservations, details.p0Observations, details.p1Observations, int.Parse(details.projectId), details.projectManagerId, date, 1);
                        this.InsertOrUpdate(updateIntoObservation);
                    }
                    else
                    {
                        if ((details.p0Observations != string.Empty) || (details.p1Observations != string.Empty))
                        {
                            insertIntoObservation = string.Format(Constants.InsertObservations, int.Parse(details.projectId), details.projectManagerId, date, details.p0Observations, details.p1Observations, 1);
                            this.InsertOrUpdate(insertIntoObservation);
                        }
                    }
                }
                else
                {
                    string newquery = string.Format(Constants.CountAutoSaveObservations, int.Parse(details.projectId), details.projectManagerId, date, 1);
                    int count = int.Parse(this.Select(newquery));
                    if (count > 0)
                    {
                        updateIntoObservation = string.Format(Constants.ChangeOnFinalSubmission, int.Parse(details.projectId), details.projectManagerId, date, 1);
                        this.InsertOrUpdate(updateIntoObservation);
                    }
                    else
                    {
                        if ((details.p0Observations != string.Empty) || (details.p1Observations != string.Empty))
                        {
                            insertIntoObservation = string.Format(Constants.InsertObservations, int.Parse(details.projectId), details.projectManagerId, date, details.p0Observations, details.p1Observations, 0);
                            this.InsertOrUpdate(insertIntoObservation);
                        }
                    }
                }
            }

            try
            {
                this.InsertOrUpdate(query);

                query = string.Format(Constants.PMNameAndEmail, details.projectManagerId);
                string data = this.Select(query);
                var dynJson = this.js.Deserialize<dynamic>(data);

                string pmName = dynJson[0]["PMName"];
                string receiverEmail = dynJson[0]["email"];
                string auditorEmail = details.updatedBy;
                query = query = string.Format(Constants.ProjectName, details.projectId);
                data = this.Select(query);
                dynJson = this.js.Deserialize<dynamic>(data);
                string projectName = dynJson[0]["ProjectName"];

                if (sendMailOrNo == 1)
                {
                    try
                    {
                        AuditMail mail = new AuditMail();
                        string reviewersList = details.reviewers;
                        if (details.poc != "")
                            reviewersList += ", " + details.poc;
                        string allObservations = this.GetPreviousObservations(details.projectId, details.projectManagerId, details.auditType, date,1);
                        mail.SendMail(receiverEmail, projectName, pmName, details.auditType, allObservations, mailCheckList, date, auditorEmail, reviewersList, double.Parse(details.auditScore));
                    }
                    catch (Exception ex)
                    {
                        ExceptionLogging.SendErrorToText(ex);
                    }
                }

                return "1";
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
            }

            return "0";
        }

        /// <summary>
        /// Retrieves data to be displayed on checklist and previous observations.
        /// </summary>
        /// <param name="projectId">Project ID.</param>
        /// <param name="auditType">Type of audit.</param>
        /// <param name="projectManagerId">Project manager name.</param>
        /// <param name="dateTime">Date and time of the creation of the audit.</param>
        /// <returns>string containing checklist and previous observations.</returns>
        public string RetrievePrevChecklist(string projectId, string auditType, string projectManagerId, string dateTime)
        {
            if (this.CheckPMAndProject(projectId, projectManagerId) == false)
            {
                return "0";
            }

            string query = string.Format(Constants.PrevChecklist, int.Parse(projectId), projectManagerId, auditType, dateTime);
            string data;
            try
            {
                data = this.Select(query);
                return data;
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                data = string.Empty;
            }

            return string.Empty;
        }

        /// <summary>
        /// Retrieves the dates on which audits were created for the combination of method parameters.
        /// </summary>
        /// <param name="projectId">Project ID.</param>
        /// <param name="auditType">Type of audit.</param>
        /// <param name="projectManagerId">Project manager name.</param>
        /// <returns>string containing dates of all existing audits.</returns>
        public string GetPreviousAuditDates(string projectId, string auditType, string projectManagerId)
        {
            if (this.CheckPMAndProject(projectId, projectManagerId) == false)
            {
                return "0";
            }

            // string query = @"SELECT [DateTime] FROM ProjectAudit.AuditTable WHERE ProjectID=" + int.Parse(projectId) + " AND PMId='" + projectManagerId + "' AND AuditType='" + auditType + "' ORDER BY [DateTime] desc for json auto;";
            string query = string.Format(Constants.PrevDates, int.Parse(projectId), projectManagerId, auditType);
            string data;
            try
            {
                data = this.Select(query);
                return data;
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                data = string.Empty;
            }

            return string.Empty;
        }

        /// <summary>
        /// Approves the audit from main form.
        /// </summary>
        /// <param name="projectId">Project ID.</param>
        /// <param name="auditType">Type of audit.</param>
        /// <param name="projectManagerId">Project manager name.</param>
        /// <param name="dateTime">Date and time of the creation of the audit.</param>
        /// <returns>integer value indicating success or failure.</returns>
        public string ApproveAudit(string projectId, string auditType, string projectManagerId, string dateTime, string username)
        {
            if (this.CheckPMAndProject(projectId, projectManagerId) == false)
            {
                return "failure";
            }
            else
            {
                // string query = @"UPDATE ProjectAudit.AuditTable SET isApproved=1 where ProjectID=" + projectId + " AND PMId='" + projectManagerId + "' and AuditType='" + auditType + "' AND DateTime='" + dateTime + "'";
                string query = string.Format(Constants.AuditStatusYes, username, projectId, projectManagerId, auditType, dateTime );
                try
                {
                    this.InsertOrUpdate(query);
                }
                catch (Exception ex)
                {
                    ExceptionLogging.SendErrorToText(ex);
                    return "failure";
                }

                return "success";
            }
        }

        /// <summary>
        /// Rejects the audit.
        /// </summary>
        /// <param name="projectId">Project ID.</param>
        /// <param name="auditType">Type of audit.</param>
        /// <param name="projectManagerId">Project manager name.</param>
        /// <param name="dateTime">Date and time of the creation of the audit.</param>
        /// <returns>integer value indicating success or failure.</returns>
        public string RejectAudit(string projectId, string auditType, string projectManagerId, string dateTime, string username)
        {
            if (this.CheckPMAndProject(projectId, projectManagerId) == false)
            {
                return "failure";
            }

            // string query = @"UPDATE ProjectAudit.AuditTable SET isApproved=-1 where ProjectID=" + projectId + " AND PMId='" + projectManagerId + "' and AuditType='" + auditType + "' AND DateTime='" + dateTime + "'";
            string query = string.Format(Constants.AuditStatusNo, username, projectId, projectManagerId, auditType, dateTime);
            try
            {
                this.InsertOrUpdate(query);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return "failure";
            }

            return "success";
        }

        /// <summary>
        /// A string containing email ID's of all reviewers is returned using the parameters.
        /// </summary>
        /// <param name="projectName">Name of the project.</param>
        /// <param name="projectManagerId">Name of project Manager</param>
        /// <param name="auditType">Name of Audit type</param>
        /// <param name="dateTime">Datetime</param>
        /// <param name="projectId">ID of the project.</param>
        /// <returns>string with each email ID comma separated.</returns>
        public string RetrieveAuditReviewers(string projectName, string projectManagerId, string auditType, string dateTime, string projectId)
        {
            if (this.CheckPMAndProject(projectId, projectManagerId) == false)
            {
                return "0";
            }

            string query = string.Format(Constants.AuditReviewers, projectManagerId, projectName, auditType, dateTime);
            string data;
            try
            {
                data = this.Select(query);
                return data;
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                data = "0";
            }

            return "0";
        }

        /// <summary>
        /// Approves audit form from link obtained by project manager in email.
        /// </summary>
        /// <param name="projectName">Project name.</param>
        /// <param name="pMName">Project manager name.</param>
        /// <param name="auditType">Type of audit.</param>
        /// <param name="date">Date when the audit was created.</param>
        /// <param name="passed">Has the PM approved it?</param>
        /// <param name="comments">Comments from PM.</param>
        /// <returns>bollean value indication success or failure.</returns>
        public bool ApproveAuditForPM(string projectName, string pMName, string auditType, string date, string passed, string comments)
        {
            string query = string.Format(Constants.PMAuditApproval, passed, comments, pMName, projectName, auditType, date);
            try
            {
                this.InsertOrUpdate(query);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Returns the SQL connection after getting all details.
        /// </summary>
        /// <param name="tenantId">Tenant ID.</param>
        /// <param name="clientId">Client Id.</param>
        /// <param name="clientSecret">Client Secret.</param>
        /// <param name="dbServer">DB server.</param>
        /// <param name="dbName">DB name.</param>
        /// <returns>SQLConnection.</returns>
        private SqlConnection GetSqlConnection(string tenantId, string clientId, string clientSecret, string dbServer, string dbName)
        {
            AuthenticationContext executionAuthenticationContext = new AuthenticationContext(string.Format(CultureInfo.InvariantCulture, this.instance, tenantId));

            ClientCredential executionClientCredential1 = new ClientCredential(clientId, clientSecret);

            AuthenticationResult executionAuthenticationResult = executionAuthenticationContext.AcquireTokenAsync(this.resourceId, executionClientCredential1).Result;

            string executionToken = executionAuthenticationResult.AccessToken;

            // Connection to DB
            SqlConnection connection = new SqlConnection("Data Source=" + dbServer + ";Initial Catalog = " + dbName + "; Connect Timeout=30;");
            connection.AccessToken = executionToken;
            return connection;
        }

        private SqlConnection GetSqlConnection2()
        {
            string connectionString;
            SqlConnection cnn;
            connectionString = @"Data Source=auisqlsr2k17maq.database.windows.net;Initial Catalog=auisqldb2k17maq;User ID=mksa;Password=$ecr3T!0!P@$$word";
            cnn = new SqlConnection(connectionString);
            return cnn;
        }

        /// <summary>
        /// Inserts and Updates data using SQL connection.
        /// </summary>
        /// <param name="query">The query to be inserted or updated.</param>
        /// <returns>Integer value indicating the status of insertio or updation.</returns>
        private int InsertOrUpdate(string query)
        {
            SqlConnection connection = this.GetSqlConnection(this.tenantId, this.clientId, this.clientSecret, this.dbServer, this.dbName);
            connection.Open();
            SqlCommand cmd = new SqlCommand(query, connection);
            int status = cmd.ExecuteNonQuery();
            connection.Close();
            return status;
        }

        /// <summary>
        /// Selects data using SQL connection.
        /// </summary>
        /// <param name="query">The query to be inserted or updated.</param>
        /// <returns>string value of the rretrieved data.</returns>
        private string Select(string query)
        {
            SqlConnection connection = this.GetSqlConnection(this.tenantId, this.clientId, this.clientSecret, this.dbServer, this.dbName);
            connection.Open();
            SqlCommand cmd = new SqlCommand(query, connection);
            SqlDataReader reader = cmd.ExecuteReader();
            string data = string.Empty;
            while (reader.Read())
            {
                data = data + reader.GetValue(0);
            }

            connection.Close();
            return data;
        }

        private string Select2(string query)
        {
            SqlConnection connection = this.GetSqlConnection2();
            connection.Open();
            SqlCommand cmd = new SqlCommand(query, connection);
            SqlDataReader reader = cmd.ExecuteReader();
            string data = string.Empty;
            while (reader.Read())
            {
                data = data + reader.GetValue(0);
            }

            connection.Close();
            return data;
        }
    }
}